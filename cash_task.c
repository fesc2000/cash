/*
Copyright 2002,2020 Felix Schmidt

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

#include <stdio.h>
#include <sys/types.h>
#include <pthread.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/syscall.h>

#include <cash.h>


static pthread_mutex_t task_list_mutex = PTHREAD_MUTEX_INITIALIZER;

#ifdef __APPLE__
typedef uint64_t _pid_t;
#else
typedef pid_t _pid_t;
#endif

/*! task structure
 */
typedef struct task
{
    /*! pthread ID */
    pthread_t ptid;

    /*! cash ID */
    int id;

    /*! LWP ID /  */
    _pid_t lwp;

    /*! whether task is active or not */
    int active;

    /*! first bytes of task name */
    char name[32];

    /*! entry point/arg */
    void *(*entry)(void*);
    void *arg;

    /*! next in list */
    struct task *next;

} cash_task_t;

/*! incremented for each new task */
static int current_taskid = 1;

/*! list of task structures (only added to at the moment) */
static cash_task_t *tasks = NULL;

static _pid_t _gettid()
{
#ifdef __APPLE__
    uint64_t tid64;
    pthread_threadid_np(NULL, &tid64);
    return (_pid_t)tid64;
#else
    pid_t tid = syscall(__NR_gettid);

    return (_pid_t)tid;
#endif
}


/*! list tasks */
int cash_tl (int all)
{
    cash_task_t *t = tasks;
    int first = 1;

    while (t != NULL)
    {
        if (all || t->active)
        {
            if (first)
                printf ("ID     St pthread_id       LWP      Name\n"
                        "------ -- ---------------- -------- ----------\n");
            first = 0;

            printf ("%-6d %2s %-16p %-8ld %s\n",
            t->id, 
            t->active ? "Rn" : "st",
            t->ptid,
            t->lwp,
            t->name);
        }

    t = t->next;
    }
    return 0;
}

/*! get a new task structure (not linked yet) */
static cash_task_t *task_get (char *name)
{
    cash_task_t *t = calloc(1, sizeof(*t));

    if (t == NULL)
    {
        fprintf (stderr, "Failed to allocate task structure\n");
        return NULL;
    }

    if ((name == NULL) || (strlen(name) == 0))
        name = "<anon>";

    pthread_mutex_lock (&task_list_mutex);
    t->id = current_taskid++;
    pthread_mutex_unlock (&task_list_mutex);

    strncpy (t->name, name, sizeof(t->name)-1);

    return t;
}

/*! Task wrapper */
void * cash_task_wrap (void *arg)
{
    cash_task_t *t = (cash_task_t*)arg;

    t->lwp = _gettid();

    /* make sure our task structure is linked, i.e. let
     * parent cash_spawn complete
     */
    pthread_mutex_lock (&task_list_mutex);
    pthread_mutex_unlock (&task_list_mutex);

    t->active = 1;

    t->entry (t->arg);

    t->active = 0;

    return NULL;
}

/*! get pthread ID for active task */
void *cash_task_pid (int id)
{
    cash_task_t *t;

    for (t = tasks; t != NULL; t = t->next)
    {
        if (!t->active || (t->id != id))
            continue;

        return (void*)t->ptid;
    }

    return NULL;
}

/*! get pthread ID for active task */
int cash_task_alive (int id)
{
    return (cash_task_pid (id) != NULL);
}

/*! destroy a task. Returns 0 on success, 1 if the task does not exist 
 * or is finished
 */
int cash_td (int id)
{
    cash_task_t *t;

    for (t = tasks; t != NULL; t = t->next)
    {
        if (!t->active || (t->id != id))
            continue;

        if (pthread_cancel (t->ptid) == 0)
        {
            t->active = 0;

            return 0;
        }
    }

    return -1;
}



/* Spawn new task
 * Return ID on success or 0 on failure
 */
int cash_spawn (cash_spawn_fp entry, void *arg, char *name)
{
    cash_task_t *t;
    
    t = task_get (name);

    if (t == NULL)
        return 0;

    t->entry = entry;
    t->arg = arg;
    t->lwp = 0;

    pthread_mutex_lock (&task_list_mutex);
    if (pthread_create (&t->ptid, NULL, cash_task_wrap, (void*)t))
    {
        free (t);
        return 0;
    }

    t->next = tasks;
    tasks = t;
    pthread_mutex_unlock (&task_list_mutex);

    return t->id;
}

/* Spawn new task. Same API as pthread_create + thread name
 */
int cash_spawn2 (pthread_t *thread, const pthread_attr_t *attr, cash_spawn_fp entry, void *arg, char *name)
{
    cash_task_t *t;
    int rc;

    printf ("Spawning %s\n", name);
    
    t = task_get (name);

    if (t == NULL)
        return -1;

    t->entry = entry;
    t->arg = arg;
    t->lwp = 0;


    pthread_mutex_lock (&task_list_mutex);
    if ((rc = pthread_create (&t->ptid, attr, cash_task_wrap, (void*)t)) != 0)
    {
        free (t);
        return rc;
    }
    *thread = t->ptid;

    t->next = tasks;
    tasks = t;
    pthread_mutex_unlock (&task_list_mutex);

    return rc;
}
/*! announce current thread to cash task list (if it's not known yet)
 * Returns current thread's task ID
 */
int cash_task_self (char *name, int rename)
{
    uint64_t myLwp = _gettid();
    pthread_t me = pthread_self();
    cash_task_t *t;

    for (t = tasks; t != NULL; t = t->next)
    {
        if (t->lwp == myLwp)
        {
            if (rename)
            {
                strncpy (t->name, name, sizeof(t->name)-1);
            }
            return t->id;
        }
    }

    t = task_get (name);
    t->ptid = me;
    t->active = 1;
    t->lwp = myLwp;

    t->next = tasks;
    tasks = t;

    return t->id;
}

/*! Clean up task list (removes entries for non-active tasks)
 */
void cash_task_cleanup (void)
{
    cash_task_t *t, *prev, *next;

    prev = NULL;

    pthread_mutex_lock (&task_list_mutex);

    for (t = tasks; t != NULL; )
    {
        if (!t->active)
        {
            if (!prev)
                tasks = t->next;
            else
                prev->next = t->next;

            next = t->next;

            free (t);

            t = next;
        }
        else
        {
            prev = t;
            t = t->next;
        }
    }

    pthread_mutex_unlock (&task_list_mutex);
}
