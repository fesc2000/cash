#
# Copyright 2002,2020 Felix Schmidt
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#    http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
##############################################################################
#
# These options can be passed to this Makefile:
#
# CROSS_COMPILE
#   Cross compiler prefix
#
# DYNCALL_DIR
#   Location where the dyncall library source has been extracted (see http://dyncall.org).
#   Required for function prototype support.
#   The configure script should have been run on the build host, the resulting configuration
#   should also work when cross-compiling ..
#   Default is dyncall-1.3, extracted from local dyncall-1.3.tar.gz
#
# OBJDIR
#   Existing directory where objectes, libraries and binaries shall be put into
#
# LIBREADLINE
#   Set to 0 to not use libreadline for tty prompt.
#
# LIBDL
#   Set to 0 to not use libdl.
#
# PREBUILT_YACC_SRC
#   If set to 1, the build-process uses pre-built cash.tab.c/h files instead of re-generating
#   them using bison. This might be useful for the case that an up-to-date bison version
#   (3.x) is not available.
#
# Make targets:
#   lib	- builds libcash.a
#   tool - builds cash 
#   all - builds both
#   clean - cleans build directory
#
#
# Extensions
# ----------
# To link your own stuff into the cash binary, the following macros can be defined:
# CASH_EXT_LIST - A list of directories containing sources which are compiled. 
#                 At least one of the files must provide an init function:
#                 void case_ext_init_name();
#                 where name is the name of the CASH_EXT_LIST directory.
#                 This function will be called at init time and can be used
#                 to declare specific prototypes.
# CASH_EXT_OBJS - A list of objects (with absolute directory) which are linked.
# EXT_LIBS      - A list of libraries to be linked 
# EXT_LDFLAGS   - Specific linker flags
# EXT_CFLAGS    - Specific compile flags
# 
# Extensions will not be linked into the shared library.
#

# Settings
#
PWD 				=	$(shell pwd)

# Configuration
#
CC					?= $(CROSS_COMPILE)gcc
LD					?= $(CC)

WFLAGS			?= -Wall -Wno-unknown-pragmas -Wno-incompatible-function-pointer-types -Wformat=0
OPTFLAGS		?= -g

CFLAGS			= $(CASH_CFLAGS)
CFLAGS			+= -I$(PWD)
CFLAGS		 	+= -fPIC
CFLAGS			+= $(WFLAGS)
CFLAGS			+= $(OPTFLAGS)
CFLAGS			+= -DHAVE_MEMPROBE
CFLAGS			+= -DINCLUDE_HELP

LIBS			+= -ldyncall_s
LIBS      += -ldynload_s

ifeq ($(LIBDL),)
LIBDL				= 1
endif

DC_VERS   ?= 1.3



ifeq ($(LIBREADLINE),)
LIBREADLINE	=	1
ifeq ($(shell uname -s),Darwin)
HOMEBREW  ?= $(shell ls -d /opt/*brew)

ifeq ($(HOMEBREW),)
$(error homebrew not found)
endif

READLINE_DIR=$(HOMEBREW)/opt/readline
READLINE_INC=-I$(READLINE_DIR)/include/readline
READLINE_LIB=-L$(READLINE_DIR)/lib

ifeq ($(READLINE_DIR),)
$(error need libreadline in $(HOMEBREW)/Cellar/readline)
endif

endif # Darwin
endif # LIBREADLINE

ifeq ($(BISON),)
ifneq ($(shell uname -s),Darwin)
BISON=bison
else
BISON=$(wildcard $(HOMEBREW)/Cellar/bison/3*/bin/bison)
endif # BISON

ifeq ($(BISON),)
$(error need bison 3.x)
endif
endif # BISON

ifeq ($(OBJDIR),)
	ifdef CROSS_COMPILE
		REL_OBJDIR=build/$(CROSS_COMPILE)cross
	else
		REL_OBJDIR=build/$(shell uname -s)-$(shell uname -m)
	endif

	OBJDIR=$(shell mkdir -p $(REL_OBJDIR) && cd $(REL_OBJDIR) && pwd)
endif

LDFLAGS += -L$(OBJDIR)


ifeq ($(LIBREADLINE),1)
CFLAGS 			+= -DHAVE_READLINE  $(READLINE_INC)
LIBS 			+= $(READLINE_LIB) -lreadline -lhistory 
endif

ifeq ($(LIBDL),1)
CFLAGS			+= -DUSE_LIBELF -DUSE_LIBDL
LIBS				+= -ldl
endif

ifeq ($(DYNCALL_DIR),)
DYNCALL_DIR = dyncall-$(DC_VERS)
endif
DYNCALL_LIB =  $(OBJDIR)/libdyncall_s.a
DYNLOAD_LIB =  $(OBJDIR)/libdynload_s.a
CFLAGS 			+= -DDYNCALL -I$(DYNCALL_DIR) -I$(DYNCALL_DIR)/dyncall -I$(DYNCALL_DIR)/dynload

DYNCALL_CLEAN = make -C $(DYNCALL_DIR) clean

# End of Configuration

# System settings
#
ifneq ($(shell uname -s),Darwin)
LIB_SUFFIX 	:= .so
else
LIB_SUFFIX 	:= .dylib
endif

# Build settings
#
LIBRARY := $(OBJDIR)/cash_wrap$(LIB_SUFFIX)


# Extensions: Absulute directory with sources to compile and link to cash
#
ifneq ($(CASH_EXT_LIST),)
EXT_NAMES   := $(shell basename -a $(CASH_EXT_LIST) | sort -u)
EXT_SRC     := $(shell basename -a $(foreach ext,$(CASH_EXT_LIST),$(wildcard $(ext)/*.c)))
EXT_OBJS    := $(CASH_EXT_OBJS) $(EXT_SRC:%.c=$(OBJDIR)/%.o)
EXT_LIBS    := $(CASH_EXT_LIBS) 
EXT_LDFLAGS := $(CASH_EXT_LDFLAGS)

EXT_INIT    := -DEXT_INIT="${EXT_NAMES:%=cash_ext_init_%();}"
EXT_PROTO   := -DEXT_PROTO="${EXT_NAMES:%=void cash_ext_init_%(void);}"

EXT_CFLAGS  := $(CASH_EXT_CFLAGS) $(EXT_PROTO) $(EXT_INIT)

VPATH       := $(CASH_EXT_LIST)
endif

CFLAGS 			+= $(EXT_CFLAGS)

ifeq ($(LIBREADLINE),builtin)
CFLAGS 			+= -DHAVE_READLINE 
LIBS 				+= -L$(PWD)/readline/shlib -lreadline -lhistory -lncurses

DEPS 				+= readline

READLINE_CLEAN = make -C readline distclean
endif

OBJS=cash.tab.o cash_utils.o cash_net.o cash_task.o cash_help.o btree.o
ifeq ($(LIBDL),1)
OBJS+=cash_libdl.o
ifneq ($(shell uname -s),Darwin)
OBJS+=cash_elf.o
else
OBJS+=cash_macho.o
endif
endif

TOOLOBJS=$(OBJS) cash_main.o cash_tool.o cash_dynload.o
SHOBJS=$(OBJS) cash_main_lib.o cash_wrap.o

BUILD_OBJS=$(TOOLOBJS:%=$(OBJDIR)/%) $(EXT_OBJS)
BUILD_SHOBJS=$(SHOBJS:%=$(OBJDIR)/%)

all: tool shlib

cash_help.c:	cash_help.txt cash_help_cmd.txt
	@rm -f cash_help.c
	@echo 'char *__cash_help_str =' >> cash_help.c
	@cat cash_help.txt | sed -e 's/\\/\\\\/g' -e 's/"/\\"/g' -e 's/%/%%/g' -e 's/\(.*\)/"\1\\n"/' >> cash_help.c
	@echo ";" >> cash_help.c
	@echo 'char *__cash_cmd_help_str =' >> cash_help.c
	@cat cash_help_cmd.txt | sed -e 's/\\/\\\\/g' -e 's/"/\\"/g' -e 's/%/%%/g' -e 's/\(.*\)/"\1\\n"/' >> cash_help.c
	@echo ";" >> cash_help.c

lib:	$(DYNCALL_LIB) $(DYNLOAD_LIB) $(OBJDIR)/libcash.a

shlib:	$(DYNCALL_LIB) $(DYNLOAD_LIB) $(LIBRARY)

tool:	$(DYNCALL_LIB) $(DYNLOAD_LIB) $(DEPS) $(OBJDIR)/cash

clean:
	$(DYNCALL_CLEAN)
	$(READLINE_CLEAN)
	rm -rf build
	rm -f cash.tab.c cash.tab.h
	rm -rf $(BUILD_OBJS) $(OBJDIR)/libcash.a $(OBJDIR)/cash $(OBJDIR)/cash_wrap.so

$(OBJDIR)/libcash.a:	${BUILD_OBJS} 
	${CROSS_COMPILE}ar crus $@ ${BUILD_OBJS}

ifeq ($(PREBUILT_YACC_SRC),1)
cash.tab.c:    generated/cash.tab.c
	cp generated/cash.tab.c .

cash.tab.h:    generated/cash.tab.h
	cp generated/cash.tab.h .

else

cash.tab.c:    cash.y
	$(BISON) -Wall -d cash.y

cash.tab.h:    cash.y
	$(BISON) -Wall -d cash.y

endif

############################################################################
# Main targets
#

# cash tool with extensions
#
$(OBJDIR)/cash:	${BUILD_OBJS} ${EXT_LIBS}
	$(call CLD$(V),$@,${LDFLAGS} ${EXT_LDFLAGS},${BUILD_OBJS},${EXT_LIBS} ${LIBS} -pthread -lm)
	@cp $@ .

# cash_wrap shared library (no extensions)
#
$(LIBRARY):	${BUILD_SHOBJS}
	$(call CLD$(V),$@,-shared ${LDFLAGS} ${EXT_LDFLAGS},${BUILD_SHOBJS},${EXT_LIBS} ${LIBS} -pthread -lm)
	@cp $@ .

############################################################################
# Misc targets
#
# Libdyncall build
#
dyncall-$(DC_VERS).tar.gz:
	wget https://dyncall.org/r$(DC_VERS)/dyncall-$(DC_VERS).tar.gz

dyncall-$(DC_VERS):	dyncall-$(DC_VERS).tar.gz
	@tar xfz dyncall-$(DC_VERS).tar.gz
	(cd dyncall-$(DC_VERS); ./configure)

$(DYNCALL_LIB):	$(DYNCALL_DIR)
	@make -C $(DYNCALL_DIR) clean >/dev/null
	@make -C $(DYNCALL_DIR) CC="$(CC)" CFLAGS=-g TARGET_ARCH= libdyncall
	@mv $(DYNCALL_DIR)/dyncall/libdyncall_s.a $@

$(DYNLOAD_LIB): $(DYNCALL_DIR)
	@make -C $(DYNCALL_DIR) CC="$(CC)"  CFLAGS=-g TARGET_ARCH= libdynload
	@mv $(DYNCALL_DIR)/dynload/libdynload_s.a $@

# Documentation 
#
doc:	cash.html

cash.html:  README.txt cash_help.txt cash_help_cmd.txt 
	rm -f cash.html
	cat $^ | markdown - > $@

# Static readline build if required
#
.PHONY:	readline

readline/config.status:
	@ln -sf readline/support/* .
	@cd readline; CFLAGS=-fPIC LDFLAGS=-fPIC ./configure $(RL_CONFIGURE_FLAGS)

readline:	readline/config.status
	@make -C readline
	@cd readline/shlib; ln -sf libreadline.so.6.2 libreadline.so.6
	@cd readline/shlib; ln -sf libreadline.so.6 libreadline.so
	@cd readline/shlib; ln -sf libhistory.so.6.2 libhistory.so.6
	@cd readline/shlib; ln -sf libhistory.so.6 libhistory.so
	

# Rules
#

# Complile command:
# 1: source
# 2: target
#
CPPOMP1 = $(CC) $(CPPFLAGS) -c $(1) -o $(2) && $(CC) -MM $(CPPFLAGS) $(1) | sed -e 's!.*\.o:!$(2):!' > $(2:%.o=%.d)
CPPOMP  = @printf "CPP %-30s : %s\n" `basename $(2)` `echo $(1)`; $(CPPOMP1)
CCOMP1  = $(CC) $(CFLAGS) -c $(1) -o $(2) && $(CC) -MM $(CFLAGS) $(1) | sed -e 's!.*\.o:!$(2):!' > $(2:%.o=%.d)
CCOMP   = @printf "CC  %-30s : %s\n" `basename $(2)` `echo $(1)`; $(CCOMP1)

# Linker command
# 1: output
# 2: flags
# 3: objects
# 4: libraries
CLD1  = $(CC) -o $(1) -g $(2) $(3) $(4)
CLD   = @printf "LD  %-30s : %s\n" `basename $(1)` "$(3)" | sed -e 's,$(OBJDIR)/,,g'; $(CLD1)

$(OBJDIR)/%.o:  %.c cash.tab.h cash.h
	$(call CCOMP$(V),$<,$@)

$(OBJDIR)/cash_main_lib.o: CFLAGS+=-DCASH_NOEXT

$(OBJDIR)/cash_main_lib.o:  cash_main.c cash.tab.h cash.h
	$(call CCOMP$(V),$<,$@)
