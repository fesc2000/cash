
/*
Copyright 2002,2020 Felix Schmidt

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

#include <stdio.h>
#include <string.h>
#ifndef __APPLE__
#include <link.h>
#include <elf.h>
#endif
#include <stdlib.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/mman.h>
#include <fcntl.h>
#include <unistd.h>
#include <dlfcn.h>

#include "cash.h"

void *cash_default_dl_handle = NULL;

void cash_dl_set_dflfile(char *file)
{
    cash_default_dl_handle = dlopen(file, RTLD_LAZY);
}

void *cash_dl_handle(void *handle)
{
    if (!handle)
    {
        return cash_default_dl_handle;
    }
    return handle;
}


/* ELF symbol table loader. Supports static and dynamic symbol tables.
 */
void*
mapfile(char* path, unsigned long* size)
{
    int f;
    struct stat s;

    if (path == NULL)
    {
        return NULL;
    }

    f = open(path, O_RDONLY);

    if (f == -1)
    {
        return NULL;
    }

    fstat(f, &s);

    if (size)
        *size = s.st_size;
    
    void* retval = mmap(NULL, s.st_size, PROT_READ, MAP_PRIVATE, f, 0); 
    close(f);
    return retval;
}


int cash_load_so (char *file)
{
#ifdef USE_LIBDL
    void *handle = dlopen(file, RTLD_NOW|RTLD_GLOBAL);

    if (handle == NULL)
        return -1;

    return cash_load_symtab_from_file (file, 1, handle);
#else
    return -1;
#endif
}
