/*
Copyright 2002,2020 Felix Schmidt

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
#ifndef _cash_h_
#define _cash_h_

#include <stdint.h>
#include <stdbool.h>
#include <strings.h>
#include <string.h>
#include <pthread.h>
#include <unistd.h>


#ifdef DYNCALL
#include <dyncall.h>
#endif

#define ALLOW_CONSTPTR

#define MAX_ID_LEN  128
#define MAX_PARMS   20
#define MAX_ITERATORS   10
#define MAX_SCOPE        32
#define MAX_CONSTP       32

#define CHAR_MASK   0x7f
#define UCHAR_MASK  0xff
#define SHORT_MASK  0xffff
#define UINT_MASK   0xffffffff
#define ULL_MASK    0xffffffffffffffffULL

#if defined(__LP64__)
#define UL_MASK     0xffffffffffffffffULL
#define PTR_MASK    0xffffffffffffffffULL
#else
#define UL_MASK     0xffffffff
#define PTR_MASK    0xffffffff
#endif

#define FUNC_MASK   PTR_MASK

#define CASH_VERSION_1  2
#define CASH_VERSION_2  4
#define CASH_VERSION_3  1

#define ITER_VALUESET           (1<<0)
#define ITER_TERM_LOCAL         (1<<1)
#define ITER_TERM_GLOBAL        (1<<2)

#define CTX_VERB_PRINTF(...) { if (scan_ctx->verbose && (getpid() == scan_ctx->pid)) printf(__VA_ARGS__); }
#define CTX_PRINTF(...) { if (getpid() == scan_ctx->pid) printf(__VA_ARGS__); }
#define CTX_ERR_PRINTF(...) { if (getpid() == scan_ctx->pid) fprintf(stderr, __VA_ARGS__); }

/* Macro to insert/redeclare a function to the symbol table with correct prototype */
#define PA32        UINT_MASK
#define PA64        PTR_MASK
#define PL_LEN(...)        (sizeof(uint64_t[]){__VA_ARGS__} / sizeof(uint64_t))
#define DECL_PROTO(name,rc,...) \
    cash_fct_add (#name, (intptr_t)name, (FCT_PROTO){rc, {__VA_ARGS__}, PL_LEN(__VA_ARGS__)});
#define DECL_INT(name,mask) cash_symtab_add(#name, T_INT, name, (int*)&name, mask);

#define CASH_SHORT_PREFIX    "_cch_b__"
#define CASH_VERB_PREFIX     "_cch_v__"
#define CASH_PROTO_PREFIX    "_cch_p__"

#ifdef INCLUDE_HELP
#define CASH_CMD_HELP(cmd,brief,verb)       \
    char* _cch_b__ ## cmd = brief; \
    char* _cch_v__ ## cmd = verb;
#else
#define CASH_CMD_HELP(cmd,brief,verb)
#endif



typedef enum
{
    T_ANON,     /* to be created */
    T_ANY,      /* only for looking up a symbol */
    T_INT,
    T_CHARPTR,
    T_SHORTPTR,
    T_INTPTR,
    T_STR,
    T_FUNC,
    T_INT64PTR,
    T_FCTPTR,
    T_STRPTR
} scantype;

typedef struct fctProto
{
    uint64_t rcsize;
    uint64_t argsize[MAX_PARMS];
    int numArgs;
} FCT_PROTO;


typedef struct symtabEntry
{
    struct symtabEntry  *next;

    char                *name;
    uint64_t            *ptr;
    uint64_t             storage;
    scantype             type;
    uint64_t             mask;
    FCT_PROTO           *proto;
    const char          *short_help;
    const char          *long_help;
} SYMTAB_ENTRY;

typedef struct
{
    scantype             type;
    uint64_t             val;
    uint64_t             mask;
    uint64_t             lv;
    int                  num_args;
    uint64_t             args[MAX_PARMS];
    uint64_t             argsize[MAX_PARMS];
#ifdef DYNCALL
    DCCallVM            *callVm;
#endif
    FCT_PROTO           *proto;

    SYMTAB_ENTRY        *sym;
    int                  token;
} VALUE;

/* yylex scan states */
typedef enum
{
    NONE,
    DEC,
    OCT,
    KW,
    HEX,
    BIN,
    IDENT,
    STR,
    OTHER,
    ENV,
    COM
} scan_state_t;

typedef struct
{
    int pos;
    uint64_t val, start, end, increment, abort;
    int complete;

    int isList;
    uint64_t values[MAX_PARMS];

} iterator_t;

typedef struct
{
    int active;
} scope_t;

typedef struct
{
    char         *str;
    int           idx;
    int           slen;
    scan_state_t  state;

    int           last_token_pos;
    int           saved_pos;
    int           need_restore;

    int           num_iterators;
    iterator_t    iterator[MAX_ITERATORS];

    int           isFirstFct, lastToken, pseudoFuncCallOpen;

    uint64_t      start_time;
    int           async;

    char         *anon_sym;
    
    int           verbose;
    
    int           scope_id;
    scope_t       scope[MAX_SCOPE];
    
    int           const_ptr_count;
    uint64_t      const_ptr[MAX_CONSTP];

    int           stdout;
    
    VALUE         result;

    int           iterator_warn;

    pid_t         pid;
} scan_ctx_t;

typedef enum 
{
    ns_mode_line_redir = 0,
    ns_mode_tty = 1,
    ns_mode_line = 2,
    ns_mode_redir = 3 /* no longer supported */
} netserver_mode_t;

typedef struct
{
    int port;
    netserver_mode_t mode;
    int local_only;
    char *af_unix_name;
} cash_netserver_args_t;

typedef void *(cash_spawn_fp) (void*);
typedef unsigned int (cash_fp) (int,...);

typedef unsigned int (cash_fp_pi)  (void*,int,...);
typedef unsigned int (cash_fp_2pi) (void*,void*,int,...);
typedef unsigned int (cash_fp_3pi) (void*,void*,void*,int,...);
typedef unsigned int (cash_fp_4pi) (void*,void*,void*,void*,int,...);
typedef unsigned int (cash_fp_5pi) (void*,void*,void*,void*,void*,int,...);


#define YYSTYPE VALUE
#define YYLEX_PARAM     scan_ctx
#define YYPARSE_PARAM   scan_ctx

extern char *__cash_help_str;
extern char *__cash_cmd_help_str;

extern __thread int do_trace;

extern cash_fp *cash_set_argc;

extern void lssyms (char *filter, int exact);
extern SYMTAB_ENTRY *cash_symtab_add (char *name, scantype type, uint64_t val, int *ptr, uint64_t mask);
SYMTAB_ENTRY * cash_fct_add (char *name, uint64_t val, FCT_PROTO proto);
extern SYMTAB_ENTRY *cash_symtab_find (char *name, scantype stype);
extern char cash_getbyte (void);
extern void symtab_anchor(void);
extern int cash_run_expr (int argc, char *argv[], int verbose, VALUE *result);
extern int cash_run_expr_s (char *line, int verbose, VALUE *result);
extern void cash_expr_initialize (void);
extern char *cash_new_string (char *string, int nchar);
extern void cash_show_val (VALUE *val);
extern void cash_show_val_str (VALUE *val, char*, int len);
extern void cash_sym_delete (char *name);
extern void cash_symtab_stat(void);
extern int cash_main (int argc, char **argv, char *initScript, VALUE *result);
extern int simple_strtoul (char *c, void *p, int base);
extern uint64_t simple_strtoull (char *c, void *p, int base);
extern intptr_t cash_adrs_remap (intptr_t adrs);
extern intptr_t cash_virt_to_io ( intptr_t virt);
extern int cash_mem_probe (char *adrs);
extern int cash_netserver(cash_netserver_args_t *args);
extern int cash_net(int port, netserver_mode_t mode, int allow);
extern int cash_net_local (char *pipedir, char *pipename, netserver_mode_t mode);
extern int cash_spawn (cash_spawn_fp entry, void *arg, char *name);
extern int cash_spawn2 (pthread_t *thread, const pthread_attr_t *attr, cash_spawn_fp entry, void *arg, char *name);
extern uint64_t cash_symval_get (SYMTAB_ENTRY *sym);
extern bool cash_symval_get_safe (SYMTAB_ENTRY *sym, uint64_t* val);
extern void cash_symval_set (SYMTAB_ENTRY *sym, int64_t val);
extern char *cash_prompt(void);
extern int cash_run_file (int fd, VALUE *result);
extern char* cash_getexename(char* buf, size_t size);
extern void cash_do_io_map(void);
extern void cash_init (char *fname, char *libname);
extern void cash_dl_set_dflfile(char *file);
extern void *cash_dl_handle(void *handle);
extern int cash_load_symtab_from_file (char *file, int use_libdl, void *handle);
extern int cash_segv_catch (void);
extern SYMTAB_ENTRY *dl_func_get (char *name, void *handle);
extern int dl_func (char *name, void *handle);
extern int dl_int (char *name, void *handle);
extern void cash_do_iterator (scan_ctx_t *ctx, VALUE *v1, VALUE *v2, VALUE *oval, int flags);
extern uint64_t cash_time64(void);
extern int yylex (VALUE *yylval, scan_ctx_t *ctx);
extern int cash_task_self (char *name, int rename);
extern int cash_md (void *addr, int nbytes, int width);
extern int cash_script (char *fname);
extern void cash_set_help (char *symname, char *helpstr);
extern void cash_net_init (void);
extern SYMTAB_ENTRY * cash_addr_resolve (uintptr_t addr, scantype stype, int exact);
extern void * cash_task_wrap (void *arg);
extern uint64_t *cash_get_constp (scan_ctx_t *ctx, uint64_t val, uint64_t mask);
extern int help(intptr_t addr);
extern int cash_help(intptr_t addr);
extern void* mapfile(char* path, unsigned long* size);
extern char* cash_escape_char(unsigned char u, char *buffer, size_t buflen);
extern char *cash_escape(char *str, char *buffer, size_t buflen, size_t str_len);
extern char* symtab_complete (const char *text, int state);
extern void* cash_dl_adrs(char *name, void *handle);
extern int cash_is_helpstr (char *str);
extern uint64_t cash_bits(uint64_t mask, int msb, int lsb);

#endif
