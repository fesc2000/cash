/*
Copyright 2002,2020 Felix Schmidt

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

/* cash_utils.c - Support routines for C expression parser */

#include <string.h>
#include <stdlib.h>
#include <ctype.h>
#ifdef USE_LIBDL
#include <dlfcn.h>
#endif
#include <stdio.h>


#include "cash.h"
#include "cash.tab.h"

#define BTREE_SYMTAB

#ifdef BTREE_SYMTAB
#include "btree.h"
#endif

#define ISAL(c)      ((c >= 'a' && c <= 'z') || (c >= 'A' && c <= 'Z'))
#define ISNUM(c)     ((c >= '0') && (c <= '9'))
#define ISID(c)      (ISAL(c) || ISNUM(c) || (c == '_'))
#define ISIDSTART(c) (ISAL(c) || (c == '_'))
#define ISSPACE(c)   (c == ' ')
#define ISHEX(c)     (ISNUM(c) || ((c >= 'a') && (c <= 'f')) || ((c >= 'A') && (c <= 'F')))
#define ISBIN(c)     ((c == '0') || (c == '1') || (c == '.'))
#define ISCHAR(c)    ((c >= 0x20) && (c <= 0x7f))
#define ISUL(c)      ((c == 'U') || (c == 'L'))

#define tPrintf if (3 == do_trace) printf

typedef unsigned int (fp) (void);

__thread int do_trace = 0;

extern int segv_silent;

#define MAX_LINE_LENGTH 1024

#ifdef BTREE_SYMTAB
BTREE        symTree = NULL;
#endif

struct strtab_entry
{
    struct strtab_entry *next;
    int len;
    char *str;

} *strtab = NULL;

struct search_arg
{
    /* for lssyms */
    char        *filter;
    int          exactMatch;
    void        *searchResult;

    /* for cash_int */
    uint64_t     value;
    int          all;
    
    int          found;
    
    /* for cash_resolve */
    scantype stype;
    int exact;
    uintptr_t addr;
    uintptr_t minDiff;
    SYMTAB_ENTRY *e;

    /* for symtab_complete */
    int count;   
    int state;
    int len;
    
    /* for symtab_stat */
    int integers;
    int strings;
    int pointers;
    int functions;
};

/*
 * Key word table
 */

static struct kt
{
    char *kt_name;
    int kt_val;
} key_words[] =
{
    /* Keyword              Ident. */
    { "void", KW_VOID},
    { "char", KW_CHAR},
    { "byte", KW_UINT8},
    { "uchar", KW_UINT8},
    { "u8", KW_UINT8},
    { "int8", KW_UINT8},
    { "int8_t", KW_UINT8},
    { "uint8", KW_UINT8},
    { "uint8_t", KW_UINT8},
    { "UINT8", KW_UINT8},
    { "short", KW_UINT16},
    { "u16", KW_UINT16},
    { "int16", KW_UINT16},
    { "int16_t", KW_UINT16},
    { "uint16", KW_UINT16},
    { "uint16_t", KW_UINT16},
    { "UINT16", KW_UINT16},
    { "ushort", KW_UINT16},
    { "int", KW_UINT32},
    { "uint", KW_UINT32},
    { "long", KW_LONG},
    { "ulong", KW_LONG},
    { "u32", KW_UINT32},
    { "int32", KW_UINT32},
    { "int32_t", KW_UINT32},
    { "uint32", KW_UINT32},
    { "uint32_t", KW_UINT32},
    { "UINT32", KW_UINT32},
    { "ullong", KW_UINT64},
    { "u64", KW_UINT64},
    { "int64", KW_UINT64},
    { "int64_t", KW_UINT64},
    { "uint64", KW_UINT64},
    { "uint64_t", KW_UINT64},
    { "UINT64", KW_UINT64},
    { ">>", RSHIFT},
    { "<<", LSHIFT},
    { "&&", AND},
    { "||", OR},
    { "==", EQ},
    { "!=", NE},
    { ">=", GE},
    { "<=", LE},
    { "++", AI},
    { "--", AD},
    { "+=", ADDA},
    { "-=", SUBA},
    { "*=", MULA},
    { "/=", DIVA},
    { "|=", ORA},
    { "&=", ANDA},
    { "^=", XORA},
    { "%=", MODA},
    { "<<=", SHLA},
    { ">>=", SHRA},
    { "sizeof", KW_SIZEOF},
    { "..", DOTDOT},
    { "??", ITER_COND},
    { "[[", ITER_START},
    { "]]", ITER_END},
    { 0, 0}
    };

#define INT_ETIME       1
#define INT_ESEC        2
#define INT_TIME        3
#define INT_SEC         4
#define INT_LTID        5

static struct kt internals[] =
{
    { "time",       INT_TIME }, /* system time in us */
    { "sec",        INT_SEC },  /* system time in sec */
    { "etime",      INT_ETIME },/* us since start of expression */
    { "esec",       INT_ESEC }, /* s since start of expression */
    { "tid",        INT_LTID }, /* last spawned thread */
    {0, 0}
};


/* locals */
static int lastTid;

/* globals */
int cash_autoresolve = 0;

/* prototypes */
unsigned int decode_bin (char *bin);
int yyparse (scan_ctx_t*);

/******************************************************************************
*
* kw_lookup - Look up keyword table
* 
*/
static int
kw_lookup (char *word)
{
    register struct kt *kp;

    for (kp = key_words; kp->kt_name != 0; kp++)
        if (!strcmp (word, kp->kt_name))
            return kp->kt_val;
    return -1;
}

/******************************************************************************
*
* int_lookup - Look up internals table
* 
*/
static int
int_lookup (char *word)
{
    register struct kt *kp;

    for (kp = internals; kp->kt_name != 0; kp++)
        if (!strcmp (word, kp->kt_name))
            return kp->kt_val;

    return -1;
}

/*
 * reset scan position to a specific value
 * Also re-initializes const storage
 */
static void reset_expression (scan_ctx_t *ctx, int scanpos)
{
    ctx->idx = scanpos;
    ctx->const_ptr_count = 0;
    memset (&ctx->const_ptr[0], 0, sizeof(ctx->const_ptr));
}


/******************************************************************************
*
* symtab_anchor 
* 
* This special routine is used by the symbol table loader
* script to calculate the offset between the symbol values as reported by nm
* and the actual address in the executing image.
*/
void
symtab_anchor (void)
{
}


/******************************************************************************
*
* kw_search - Look up keyword table for a non-zero terminated string
* 
*/
int
kw_search (char *str, int *len)
{
    register struct kt *kp;
    int kl;

    for (kp = key_words; kp->kt_name != 0; kp++)
    {
        kl = strlen (kp->kt_name);

        if (!strncmp (str, kp->kt_name, kl))
        {
            *len = kl;
            return kp->kt_val;
        }
    }
    return -1;
}

int num_suffix (char *str, int l, uint64_t *mask)
{
    if (!str)
        return 0;

    if ((l > 2) && !strcmp (&str[l-3], "ULL"))
    {
        *mask = ULL_MASK;
        str[l-3] = 0;
        return 1;
    }

    if (l > 1)
    {
        if (!strcmp (&str[l-2], "UL"))
        {
            *mask = UL_MASK;
            str[l-2] = 0;
            return 1;
        }
        else if (!strcmp (&str[l-2], "LL"))
        {
            *mask = ULL_MASK;
            str[l-2] = 0;
            return 1;
        }
    }

    if ((l > 0) && !strcmp (&str[l-1], "L"))
    {
        *mask = UL_MASK;
        str[l-1] = 0;
        return 1;
    }

    *mask = UINT_MASK;
    return 1;
}

void init_iterators (scan_ctx_t *ctx)
{
    ctx->saved_pos = ctx->last_token_pos;
    ctx->need_restore = 0;

    ctx->start_time = cash_time64();

    tPrintf ("[saving at %d]", ctx->saved_pos);
}

/* Process all iterators in current context from right to left
 * Returns 1 if at least one iterator has been changed,
 *         0 if all are complete
 */
int process_iterators (scan_ctx_t *ctx)
{
    int i, j;
    iterator_t *it;

    /* process all iterators in expression from right to left
     */
    for (i = ctx->num_iterators-1; i >= 0; i--)
    {
        it = &ctx->iterator[i];

        if (it->complete)
            continue;

        it->val += it->increment;

        if (it->val == it->end)
        {
            it->complete = 1;

            continue;
        }
        
        /* reset subordinate (right side) iterators to start value
         */
        for (j = i + 1; j < ctx->num_iterators; j++)
        {
            ctx->iterator[j].val = ctx->iterator[j].start;
            ctx->iterator[j].complete = 0;
        }

        /* restore scan position to start of expression
         */
        reset_expression (ctx, ctx->saved_pos);
            
        return 1;
    }

    /* no iterators in expression or all iterators are complete
     */
    ctx->num_iterators = 0;
    return 0;
}

static iterator_t *find_iterator (scan_ctx_t *ctx)
{
    int i;

    for (i = 0; i < ctx->num_iterators; i++)
    {
        if (ctx->idx == ctx->iterator[i].pos)
        {
            tPrintf ("[using iterator %d at %d]\n", i, ctx->idx);
            return &ctx->iterator[i];
        }
    }

    return NULL;
}

static iterator_t *new_iterator (scan_ctx_t *ctx)
{
    iterator_t *it = NULL;

    if (ctx->num_iterators >= MAX_ITERATORS)
    {
        return NULL;
    }

    tPrintf ("[new iterator %d at %d]\n", ctx->num_iterators, ctx->idx);
    it = &ctx->iterator[ctx->num_iterators];
    it->pos = ctx->idx;

    ctx->num_iterators++;

    return it;
}

void cash_do_iterator (scan_ctx_t *ctx, VALUE *v1, VALUE *v2, VALUE *oval, int flags)
{
    iterator_t *it;

    oval->sym = NULL;
    oval->lv = 0;
    oval->type = v1->type;
    oval->val = v1->val;
    oval->mask = v1->mask;

    /* find iterator at this position */
    it = find_iterator (ctx);

    if (it == NULL)
    {
        it = new_iterator (ctx);

        if (it == NULL)
            return;

        if ((flags & ITER_VALUESET) == 0)
        {
            it->isList = 0;
            it->start = v1->val;
            it->val = v1->val;

            if (v2)
            {
                it->end = v2->val;
                it->increment = (it->val < it->end) ? 1 : -1;
                it->end += it->increment;
            }
            else
            {
                it->end = v1->val;
                it->increment = 1;
            }
            it->complete = 0;
        }
        else
        {
            it->isList = 1;
            it->start = 0;
            it->val = 0;
            it->end = v1->num_args;
            it->increment = 1;
            it->complete = 0;
            
            /* The array normally used for function parameters is taken as
             * value set.
             */
            memcpy ((void*)it->values, (void*)v1->args, sizeof(it->values));
        }
    }

    if (flags & ITER_TERM_LOCAL)
    {
        it->complete = 1;
    }

    if (it->isList == 0)
        oval->val = it->val;
    else
        oval->val = it->values[it->val];
}

/******************************************************************************
*
* yylex - A simple scanner providing symbols to the yacc generated source.
* 
*/
int
yylex (VALUE *yylval, scan_ctx_t *ctx)
{
    char c, c1, c2;
    int start;
    char tmp_str[128];
    int kw, kl;

    start = 0;
    ctx->state = NONE;
    ctx->last_token_pos = ctx->idx;

    /* this is a hack to support writing at least one function call without brackets.
     * When after the first occurence of a function identifer, the next non-space is
     * not a "(", it's inserted, as well as the closing ")" at the end of the 
     * expression.
     * Nested "pseudo function calls" are not possible
     */
    if (ctx->idx == 0)
    {
        tPrintf ("<SOE>");
    }

    tPrintf ("%s", ctx->pseudoFuncCallOpen ? "~":"-");

    while (1)
    {
        if ((ctx->state == NONE) &&
            ((ctx->lastToken == -1) || (ctx->lastToken == ';')))
        {
            /* initialize iterators whenever a new expression starts and 
             */
            if (ctx->num_iterators == 0)
                init_iterators (ctx);
        }

        c = ctx->str[ctx->idx];
        c1 = ctx->str[ctx->idx + 1];
        c2 = ctx->str[ctx->idx + 2];

        if (ctx->idx > ctx->slen)
        {
            tPrintf ("<EOE>");
            if (ctx->pseudoFuncCallOpen)
            {
                ctx->pseudoFuncCallOpen = 0;
                tPrintf("<)>");
                ctx->lastToken = yylval->token = ')';
                return ')';
            }

            if (process_iterators (ctx))
            {
                ctx->lastToken = yylval->token = ';';
                return ';';
            }

            ctx->lastToken = yylval->token = 0;
            return 0;
        }

        if (ctx->state == NONE)
        {
            /* initial state. search for beginning of symbols */

            if ((c == '/') && (c1 == '/'))
            {
                tPrintf ("<COM>");
                ctx->state = COM;
                ctx->idx+= 2;
                continue;
            }

            /* skip initial . or / */
            if ((ctx->idx == 0) && ((c == '/') || (c == '.')))
            {
                ctx->verbose = (c == '.');
                ctx->idx++;
                continue;
            }

            if (c == ' ')
            {
                /* skip spaces */
                ctx->idx++;
                continue;
            }

            /* if the last token was a known function identifier, 
             * and this is the first symbol of a expression, and it is
             * not followed by "(" or "=" (which are the legal characters 
             * allowed by the parser), then insert colon symbols.
             *
             * The same is done for a LIBDL_ID, which is a unknown symbol that
             * could be resolved via libdl. The parser will automatically
             * import this symbol into the symbol table.
             */
            if (ctx->isFirstFct && 
                ((ctx->lastToken == FUNC_ID) ||
                 (ctx->lastToken == LIBDL_ID)) && 
                !ctx->pseudoFuncCallOpen && 
                (c != '(') && (c != '='))
            {
                tPrintf("<(>");
                ctx->lastToken = yylval->token = '(';
                ctx->pseudoFuncCallOpen = 1;
                return '(';
            }

            if (c == '$')
            {
                /* environment/internal var. */
                ctx->state = ENV;
                ctx->idx ++;
                start = ctx->idx;
                continue;
            }

            

            if ((c == '\'') && (c2 == '\''))
            {
                /* character constant */
                ctx->idx += 3;
                yylval->mask = CHAR_MASK;
                yylval->val = c1;
                ctx->lastToken = yylval->token = CHAR;
                return CHAR;
            }

            if ((c == '0') && (c1 == 'x'))
            {
                /* 0x... : hex number */
                ctx->state = HEX;
                start = ctx->idx;
                ctx->idx += 2;
                continue;
            }
            if ((c == '0') && (c1 == 'b'))
            {
                /* 0b: binary number */
                ctx->state = BIN;
                start = ctx->idx + 2;
                ctx->idx += 2;
                continue;
            }
            if ((c == '0') && ISNUM (c1))
            {
                /* 0<num> : octal number */
                ctx->state = OCT;
                start = ctx->idx;
                ctx->idx++;
                continue;
            }

            if (ISNUM (c))
            {
                /* <num> : decimal number */
                ctx->state = DEC;
                start = ctx->idx;
                ctx->idx++;
                continue;
            }

            if ((c == '"') || (c == '`'))
            {
                /* string */
                ctx->state = STR;
                start = ctx->idx;
                ctx->idx++;
                continue;
            }

            if (ISIDSTART (c))
            {
                /* character: beginning of identifier or keyword */
                ctx->state = IDENT;
                start = ctx->idx;
                ctx->idx++;
                continue;
            }

            if (c != ' ')
            {
                /* something else. maybe it's a keyword */
                if ((kw = kw_search (&ctx->str[ctx->idx], &kl)) != -1)
                {
                    strncpy (tmp_str, &ctx->str[ctx->idx], kl);
                    tmp_str[kl] = 0;
                    tPrintf ("<KW: %s>", tmp_str);
                    ctx->idx += kl;
                    ctx->lastToken = yylval->token = kw;
                    return kw;
                }

                /* no keyword. provide character as is */
                /* newlines are treated as empty declaration */
                if ((c == '\n') || (c == ';') || (c < 0x20))
                {
                    tPrintf ("<EOE>");
                    if (ctx->pseudoFuncCallOpen)
                    {
                        ctx->pseudoFuncCallOpen = 0;
                        tPrintf("<)>");
                        return ')';
                    }

                    if (process_iterators (ctx))
                    {
                        ctx->lastToken = yylval->token = ';';
                        return ';';
                    }
                }

                ctx->idx++;
                tPrintf ("<%c>", c);
                ctx->lastToken = yylval->token = c;
                return c;
            }

            ctx->idx++;
            continue;
        }

        /* not initial state */

        if (ctx->state == COM)
        {
            if (c == '\n')
            {
                ctx->state = NONE;
            }
            else
            {
                ctx->idx++;
                continue;
            }
        }

        if (ctx->state == HEX)
        {
            if (ISHEX (c) || ISUL(c))
            {
                ctx->idx++;
                continue;
            }
            kl = ctx->idx - start;

            strncpy (tmp_str, &ctx->str[start], kl);
            tmp_str[kl] = 0;
            if (!num_suffix(tmp_str, kl, &yylval->mask))
                return OTHER;
            tPrintf ("<HEX: %s>", tmp_str);
            yylval->val = simple_strtoull (tmp_str, NULL, 16);
            yylval->sym = NULL;
            if ((yylval->mask == UINT_MASK) && (yylval->val > 0xffffffffULL))
                yylval->mask = ULL_MASK;
            ctx->lastToken = yylval->token = NUMBER;
            return NUMBER;
        }

        if (ctx->state == BIN)
        {
            if (ISBIN (c) || ISUL(c))
            {
                ctx->idx++;
                continue;
            }
            kl = ctx->idx - start;

            strncpy (tmp_str, &ctx->str[start], kl);
            tmp_str[kl] = 0;
            if (!num_suffix(tmp_str, kl, &yylval->mask))
                return OTHER;
            tPrintf ("<BIN: %s>", tmp_str);
            yylval->val = decode_bin (tmp_str);
            yylval->sym = NULL;
            if ((yylval->mask == UINT_MASK) && (yylval->val > 0xffffffffULL))
                yylval->mask = ULL_MASK;
            ctx->lastToken = yylval->token = NUMBER;
            return NUMBER;
        }

        if (ctx->state == OCT)
        {
            if (ISNUM (c) || ISUL(c))
            {
                ctx->idx++;
                continue;
            }
            kl = ctx->idx - start;
            strncpy (tmp_str, &ctx->str[start], kl);
            tmp_str[kl] = 0;
            if (!num_suffix(tmp_str, kl, &yylval->mask))
                return OTHER;
            tPrintf ("<OCT: %s>", tmp_str);
            yylval->val = simple_strtoull (tmp_str, NULL, 8);
            yylval->sym = NULL;
            if ((yylval->mask == UINT_MASK) && (yylval->val > 0xffffffffULL))
                yylval->mask = ULL_MASK;
            ctx->lastToken = yylval->token = NUMBER;
            return NUMBER;
        }

        if (ctx->state == DEC)
        {
            if (ISNUM (c) || ISUL(c))
            {
                ctx->idx++;
                continue;
            }
            kl = ctx->idx - start;
            strncpy (tmp_str, &ctx->str[start], kl);
            tmp_str[kl] = 0;
            if (!num_suffix(tmp_str, kl, &yylval->mask))
                return OTHER;
            tPrintf ("<DEC: %s>", tmp_str);
            yylval->val = simple_strtoull (tmp_str, NULL, 10);
            yylval->sym = NULL;
            if ((yylval->mask == UINT_MASK) && (yylval->val > 0xffffffffULL))
                yylval->mask = ULL_MASK;

            ctx->lastToken = yylval->token = NUMBER;
            return NUMBER;
        }

        if (ctx->state == STR)
        {
            if ((c != '"') && (c != '`')){
                ctx->idx++;
                continue;
            }
            kl = ctx->idx - start - 1;
            yylval->mask = PTR_MASK;
            yylval->val = (intptr_t) cash_new_string (&ctx->str[start + 1], kl);
            yylval->sym = NULL;
            tPrintf ("<STR: %s (%d)>", (char*)(uintptr_t)yylval->val, kl);
            ctx->idx++;
            ctx->lastToken = yylval->token = NUMBER;
            return STRING;
        }

        if (ctx->state == IDENT)
        {
            if (ISID (c))
            {
                ctx->idx++;
                continue;
            }
            kl = ctx->idx - start;
            if (kl >= MAX_ID_LEN)
            {
                printf ("identifier too long\n");
                ctx->lastToken = yylval->token = 0;
                return 0;
            }

            strncpy (tmp_str, &ctx->str[start], kl);
            tmp_str[kl] = 0;

            if ((kw = kw_lookup (tmp_str)) == -1)
            {
                /* not a keyword, look up symbol tavble */

                if (!strcmp (tmp_str, "exit"))
                {
                    fprintf (stderr, "cash: use _cash_syscall_exit for exit system call\n");
                    strcpy (tmp_str, "_cash_exit");
                    kl = strlen (tmp_str);
                }
                else if (!strcmp (tmp_str, "_cash_syscall_exit"))
                {
                    strcpy (tmp_str, "exit");
                    kl = strlen (tmp_str);
                }


                yylval->sym = cash_symtab_find (tmp_str, T_ANY);

                if (yylval->sym == NULL)
                {
                    /* not in symbol table, i.e. it's an undefined symbol.
                     * Allocate the name string here, the parser must
                     * release the string if it doesn't need it!
                     */
                    yylval->mask = PTR_MASK;
                    yylval->val = (intptr_t)malloc(kl+1);
                    strcpy ((char*)(uintptr_t)yylval->val, tmp_str);
                    tPrintf ("<anon ID: %s>", tmp_str);
                    ctx->lastToken = yylval->token = ANON_ID;

#ifdef USE_LIBDL
                    /* this feature allows to automatically resolve unknown 
                     * symbols via libdl (for example stuff from libc ...).
                     * This works, but is often not consistent, so it's 
                     * disabled by default.
                     * For example, a known function will implictly be declared 
                     * as integer if something gets assigned to it by accident, 
                     * and can therefore later on no longer be added as symbol.
                     */
                    if (cash_autoresolve)
                        if (dlsym (cash_dl_handle(NULL), tmp_str))
                            ctx->lastToken = yylval->token = LIBDL_ID;
#endif

                    return yylval->token;
                }

                if (yylval->sym->type == T_FUNC)
                {
                    if (ctx->lastToken == -1)
                        ctx->isFirstFct = 1;
                    else
                        ctx->isFirstFct = 0;

                    /* a function name */
                    tPrintf ("<%d %d FUNC_ID: %s>", ctx->isFirstFct, ctx->lastToken, tmp_str);

                    yylval->mask = yylval->sym->mask;
                    ctx->lastToken = yylval->token = FUNC_ID;
                    return FUNC_ID;
                }

                /* a variable */
                tPrintf ("<VAR_ID: %s>", tmp_str);
                ctx->lastToken = yylval->token = VAR_ID;
                return VAR_ID;

            }
            tPrintf ("<KW: %s>", tmp_str);
            ctx->lastToken = yylval->token = kw;
            return kw;
        }
        
        if (ctx->state == ENV)
        {
            if (ISID (c))
            {
                ctx->idx++;
                continue;
            }

            kl = ctx->idx - start;
            if (kl >= MAX_ID_LEN)
            {
                printf ("env. identifier too long\n");
                ctx->lastToken = yylval->token = 0;
                return 0;
            }

            strncpy (tmp_str, &ctx->str[start], kl);
            tmp_str[kl] = 0;

            switch (int_lookup (tmp_str))
            {
                case INT_TIME:
                    yylval->val = cash_time64();
                    yylval->mask = ULL_MASK;
                    ctx->lastToken = yylval->token = NUMBER;
                    return NUMBER;
                case INT_SEC:
                    yylval->val = cash_time64() / 1000000;
                    yylval->mask = ULL_MASK;
                    ctx->lastToken = yylval->token = NUMBER;
                    return NUMBER;
                case INT_ETIME:
                    yylval->val = cash_time64() - ctx->start_time;
                    yylval->mask = ULL_MASK;
                    ctx->lastToken = yylval->token = NUMBER;
                    return NUMBER;
                case INT_ESEC:
                    yylval->val = (cash_time64() - ctx->start_time) / 1000000;
                    yylval->mask = ULL_MASK;
                    ctx->lastToken = yylval->token = NUMBER;
                    return NUMBER;
                case INT_LTID:
                    yylval->val = (uint64_t)lastTid;
                    yylval->mask = UINT_MASK;
                    ctx->lastToken = yylval->token = NUMBER;
                    return NUMBER;
                default:
                    yylval->mask = PTR_MASK;
                    yylval->val = (intptr_t) getenv (tmp_str);

                    if (yylval->val == 0) yylval->val = (intptr_t)"";

                    tPrintf ("<STR: %s (%d)>", (char*)(uintptr_t)yylval->val, kl);
                    ctx->lastToken = yylval->token = STRING;
                    return STRING;
            }
        }

        /* newlines are treated as empty declaration */
        if ((c == '\n') || (c == ';') || (c < 0x20))
        {
            tPrintf ("<EOE>");
            if (ctx->pseudoFuncCallOpen)
            {
                ctx->pseudoFuncCallOpen = 0;
                tPrintf("<)>");
                return ')';
            }

            if (process_iterators (ctx))
            {
                ctx->lastToken = yylval->token = ';';
                return ';';
            }

            c = ';';
        }

        ctx->idx++;
        ctx->lastToken = yylval->token = c;
        return c;
    }
}

/******************************************************************************
*
* decode_bin - Decode binary character string.
* 
*/
unsigned int
decode_bin (char *bin)
{
    int i;
    int rc = 0;
    int bit;

    bit = 31;
    for (bit = 0, i = strlen (bin) - 1; bit < 32; i--, bit++)
    {
        if (bin[i] == 'b')
            break;

        if (bin[i] == '.')
        {
            if ((bit % 4) == 0)
                bit += 3;
            else
                bit += 4 + (3 - (bit % 4));
            continue;
        }

        if (bin[i] == '1')
            rc |= (1 << bit);
    }

    return rc;
}

/******************************************************************************
*
* scan_init - Initialize scan-line for parsing.
* Applies all escape sequences
* 
*/
scan_ctx_t * scan_init (char *s)
{
    int i, j;
    char c;
    int lasti;
    scan_ctx_t *ctx = calloc (1, sizeof(*ctx));

    if (ctx == NULL)
        return NULL;
    
    ctx->num_iterators = 0;
    ctx->str = s;
    ctx->idx = 0;
    ctx->slen = strlen (s);
    ctx->isFirstFct = 0;
    ctx->lastToken = -1;
    ctx->pseudoFuncCallOpen = 0;
    ctx->async = 0;
    ctx->scope_id = 0;
    ctx->scope[0].active = 1;
    ctx->pid = getpid();
    lasti = 0;

    for (i = j = 0; i <= ctx->slen; i++)
    {
        c = ctx->str[i];

        if (c == '\0')
        {
            ctx->str[j++] = c;
            break;
        }

        if (c == '\\')
        {
            switch (ctx->str[i + 1])
            {
            case '\\':
                c = '\\';
                break;
            case '0':
                c = 0;
                break;
            case 'a':
                c = 7;
                break;
            case 'b':
                c = 8;
                break;
            case 't':
                c = 9;
                break;
            case 'n':
                c = 10;
                break;
            case 'v':
                c = 11;
                break;
            case 'f':
                c = 12;
                break;
            case 'r':
                c = 13;
                break;
            default:
                printf ("illegal escape sequence <%c>\n", ctx->str[i + 1]);
                return 0;
            }
            i++;
        }
        else if (c == '\r')
        {
            continue;
        }
        else if (c != ' ')
        {
            lasti = j;
        }

        ctx->str[j++] = c;
    }

    if (lasti && ctx->str[lasti] == '&')
    {
        ctx->async = 1;
        ctx->str[lasti] = 0;
        j = lasti;
    }

    ctx->slen = j;

    if (j == 0)
    {
        free (ctx);
        return NULL;
    }

    /* this is important to suppport a read-ahead of two */
    strcat (ctx->str, ";  ");
    ctx->slen += 2;

    return ctx;
}

/******************************************************************************
*
* i2s - Convert integer <value> to string of base <base> (default 10). 
* Base 10 values can be signed (<us> = 0) or unsigned (<us> = 1, default).
* 
*/
char *
i2s (uint64_t value, int base, int us)
{
    char tmp_str[50];
    int i, j, k;

    if (base == 0)
        base = 10;

    switch (base)
    {
    case 2:
        strcpy (tmp_str, "0b");
        k = 0;
        for (i = (value > 0xffffffffULL)?64:31; i >= 0; i--)
        {
            if (value & (1 << i))
                k = 1;

            if (k)
                strcat (tmp_str, value & (1 << i) ? "1" : "0");
        }
        if (k == 0)
            strcat (tmp_str, "0");
        break;
    case 8:
        k = 0;

        strcpy (tmp_str, "0");
        for (i = 0; i < 10; i++)
        {
            j = (value >> 27) & 7;
            if (j != 0)
                k = 1;

            if (k)
                sprintf (tmp_str + strlen (tmp_str), "%d", j);

            value = value << 3;
        }
        if (k == 0)
            strcat (tmp_str, "0");
        break;
    case 10:
        if (us)
            sprintf (tmp_str, "%ull", value);
        else
            sprintf (tmp_str, "%dll", value);
        break;
    case 16:
        sprintf (tmp_str, "0x%llx", value);
        break;

    default:
        return NULL;
    }

    return cash_new_string (tmp_str, -1);
}

char* cash_escape_char(unsigned char u, char *buffer, size_t buflen)
{
    if (!buffer)
    {
        buffer = malloc(5);
        buflen = 5;
    }
        
    if (buflen < 2)
        *buffer = '\0';
    else if (isprint(u) && u != '\'' && u != '\"' && u != '\\' && u != '\?')
        sprintf(buffer, "%c", u);
    else if (buflen < 3)
        *buffer = '\0';
    else
    {
        switch (u)
        {
        case '\a':  strcpy(buffer, "\\a"); break;
        case '\b':  strcpy(buffer, "\\b"); break;
        case '\f':  strcpy(buffer, "\\f"); break;
        case '\n':  strcpy(buffer, "\\n"); break;
        case '\r':  strcpy(buffer, "\\r"); break;
        case '\t':  strcpy(buffer, "\\t"); break;
        case '\v':  strcpy(buffer, "\\v"); break;
        case '\\':  strcpy(buffer, "\\\\"); break;
        case '\'':  strcpy(buffer, "\\'"); break;
        case '\"':  strcpy(buffer, "\\\""); break;
        case '\?':  strcpy(buffer, "\\\?"); break;
        default:
            if (buflen < 5)
                *buffer = '\0';
            else
                sprintf(buffer, "\\%03o", u);
            break;
        }
    }

    return buffer;
}

char *cash_escape(char *str, char *buffer, size_t buflen, size_t str_len)
{
    unsigned char u;
    size_t len;
    int allocated = 0;
    size_t i;

    if (!str)
    {
        return NULL;
    }

    if (str_len == 0)
        str_len = strlen(str);

    if (buffer == NULL)
    {
        buflen = str_len * 2;
        buffer = calloc(buflen, 1);
        allocated = 1;
    }

    for (i = 0; i < str_len; i++)
    {
        char tmp[5];
        u =  str[i];

        cash_escape_char(u, tmp, 5);
        if (allocated && len > buflen)
        {
            buflen += (str_len - i) * 5;
            buffer = realloc(buffer, buflen);
        }

        if (((len = strlen(tmp)) == 0) || (len > buflen))
            goto err;

        strcat (buffer, tmp);
        buflen -= len;
    }

    return buffer;

err:
    if (allocated)
        free (buffer);

    return NULL;
}

static void print_sym(SYMTAB_ENTRY *s)
{
    VALUE val;
    bool valid;

    valid = cash_symval_get_safe (s, &val.val);
    val.type = s->type;
    val.mask = s->mask;
    val.sym = s;

    if (s->type == T_INT)
        printf ("%21s@%p | ", s->name, s->ptr);
    else
        printf ("%32s | ", s->name);

    if (valid)
    {
        cash_show_val (&val);
    }
    else
    {
        printf ("[MEMORY ERROR]");
    }

    if (s->short_help)
      printf (" : %s", s->short_help);
}

static int list_sym_cb (const void *arg1, void *usrArg, int spaces)
{
    struct search_arg *search = usrArg;
    SYMTAB_ENTRY **s1 = (SYMTAB_ENTRY**)arg1;
    
    if ((s1 == NULL) || (*s1 == NULL) || (search == NULL))
        return 0;
    
    if ((search->filter != NULL) && 
        (strstr ((*s1)->name, search->filter) == NULL))
        return 0;
    
    if ((search->exactMatch == 1) && 
        search->filter && 
        strcmp (search->filter, (*s1)->name))
        return 0;
    
    if ((search->exactMatch == 2) && 
        (((*s1)->type != T_FUNC) ||
         ((*s1)->proto == NULL)))
        return 0;
        
    if (search->exactMatch == 1)
        return 1;
    
    print_sym(*s1);
    printf ("\n");

    return 0;
}

/******************************************************************************
*
* lssyms - List symbol table with optional filter string
* 
*/
void
lssyms (char *filter, int exactMatch)
{
    struct search_arg search;

    if (!symTree)
        return;
    
    search.filter = filter;
    search.exactMatch = exactMatch;
    
    btree_Walk (symTree, list_sym_cb, (void*)&search);
}

/******************************************************************************
*
* cash_symtab_find - Find symbol table entry of specified type, or T_ANY for all 
* types
*/
SYMTAB_ENTRY *
cash_symtab_find (char *name, scantype stype)
{
    SYMTAB_ENTRY *entry = NULL;
    SYMTAB_ENTRY key;
    SYMTAB_ENTRY *keyp = &key;
    
    if (!symTree)
        return NULL;
    
    key.name = name;
    
    if (!btree_Search (symTree, &keyp, &entry))
    {
        if ((stype == T_ANY) || (stype == entry->type))
            return entry;
    }
    
    return NULL;
}

static int cash_int_cb (const void *arg1, void *usrArg, int spaces)
{
    struct search_arg *search = usrArg;
    SYMTAB_ENTRY **s1 = (SYMTAB_ENTRY**)arg1;
    uintptr_t sval;
    
    if ((s1 == NULL) || (*s1 == NULL) || (search == NULL))
        return 0;

    sval = cash_symval_get (*s1);

    if ((search->value == sval)
        && (search->all || (*s1)->type == T_INT)
        && strstr((*s1)->name, search->filter))
    {
        printf ("[%s] ", (*s1)->name);
        search->found = 1;
    }
    return 0;
}

/******************************************************************************
*
* cash_addr_resolve - List symbols with the same value
*/
uint64_t
cash_int (uint64_t value, char *substring, int all)
{
    struct search_arg search;
    
    search.value = value;
    search.all = all;
    search.filter = substring ? substring : "";
    search.found = 0;

    btree_Walk (symTree, cash_int_cb, (void*)&search);

    if (search.found)
        printf ("\n");

    return value;
}

static int cash_resolve_cb (const void *arg1, void *usrArg, int spaces)
{
    struct search_arg *search = usrArg;
    SYMTAB_ENTRY **s1 = (SYMTAB_ENTRY**)arg1;
    uintptr_t ptr;
    
    if ((s1 == NULL) || (*s1 == NULL) || (search == NULL))
        return 0;

    ptr = cash_symval_get (*s1);

    if ((search->stype == T_ANY) || 
        (search->stype == (*s1)->type))
    {
        if (search->exact)
        {
            if (search->addr == ptr)
            {
                search->e = (*s1);
                return 1;
            }
        }
        else
        {
            if ((search->addr >= ptr) && 
                ((search->addr - ptr) < search->minDiff))
            {
                search->minDiff = search->addr - ptr;
                search->e = *s1;
            }
        }
    }
    
    return 0;
}

/******************************************************************************
*
* cash_addr_resolve - Find nearest symbol with lower or equal address for given
* address/type types
*/
SYMTAB_ENTRY *
cash_addr_resolve (uintptr_t addr, scantype stype, int exact)
{
    struct search_arg search;
    
    if (!symTree)
        return NULL;
    
    search.addr = addr;
    search.stype = stype;
    search.exact = exact;
    search.minDiff = PTR_MASK;
    search.e = NULL;

    btree_Walk (symTree, cash_resolve_cb, (void*)&search);

    return search.e;

}

/******************************************************************************
*
* cash_addr_show - Show name of nearest (lower) function and offset for given 
*                  address
*/
int cash_addr_show (void *addr)
{
    SYMTAB_ENTRY *e = cash_addr_resolve ((uintptr_t)addr, T_FUNC, 0);

    if (!e)
        return -1;

    printf ("%s+%d\n",
        e->name, 
        (int)((uintptr_t)addr - (uintptr_t)cash_symval_get (e)));

    return 0;
}

static int cash_complete_cb (const void *arg1, void *usrArg, int spaces)
{
    struct search_arg *search = usrArg;
    SYMTAB_ENTRY **s1 = (SYMTAB_ENTRY**)arg1;
    
    if ((s1 == NULL) || (*s1 == NULL) || (search == NULL))
        return 0;

    if (!strncmp (search->filter, (*s1)->name, search->len))
    {
        if (search->state == search->count)
        {
            search->count++;
            
            search->e = *s1;
            return 1;
        }

        search->count++;
    }
    return 0;
}

/******************************************************************************
*
* symtab_complete - completion callback suitable for libreadline
*/
char *
symtab_complete (const char *text, int state)
{
    struct search_arg search;
    
    if (!symTree)
        return NULL;
    
    search.count = 0;
    search.state = state;
    search.len = strlen (text);
    search.filter = (char*)text;
    search.e = NULL;
    
    btree_Walk (symTree, cash_complete_cb, (void*)&search);
    
    if (search.e)
    {
        return (char*)strdup (search.e->name);
    }

    return NULL;
}

bool cash_symval_get_safe (SYMTAB_ENTRY *sym, uint64_t* val)
{
    if (!cash_mem_probe((char*)cash_adrs_remap((intptr_t)sym->ptr)))
    {
        return false;
    }

    switch (sym->mask)
    {
        case 0:
            *val = 0;
            break;
        case CHAR_MASK:
        case UCHAR_MASK:
            *val = *(uint8_t*)cash_adrs_remap((intptr_t)sym->ptr);
            break;
        case SHORT_MASK:
            *val = *(uint16_t*)cash_adrs_remap((intptr_t)sym->ptr);
            break;
        case ULL_MASK:
            *val = *(uint64_t*)cash_adrs_remap((intptr_t)sym->ptr);
            break;
        default:
            *val = *(uint32_t*)cash_adrs_remap((intptr_t)sym->ptr);
            break;
    }

    return true;
}

uint64_t cash_symval_get (SYMTAB_ENTRY *sym)
{
    switch (sym->mask)
    {
        case 0:
            return 0;
        case CHAR_MASK:
        case UCHAR_MASK:
            return *(uint8_t*)cash_adrs_remap((intptr_t)sym->ptr);
        case SHORT_MASK:
            return *(uint16_t*)cash_adrs_remap((intptr_t)sym->ptr);
        case ULL_MASK:
            return *(uint64_t*)cash_adrs_remap((intptr_t)sym->ptr);
        default:
            return *(uint32_t*)cash_adrs_remap((intptr_t)sym->ptr);
            break;
    }

    return 0;
}

void cash_symval_set (SYMTAB_ENTRY *sym, int64_t val)
{
    switch (sym->mask)
    {
        case 0:
            return;
        case CHAR_MASK:
        case UCHAR_MASK:
            *(uint8_t*)cash_adrs_remap((intptr_t)sym->ptr) = (uint8_t)val;
            break;
        case SHORT_MASK:
            *(uint16_t*)cash_adrs_remap((intptr_t)sym->ptr) = (uint16_t)val;
            break;
        case ULL_MASK:
            *(uint64_t*)cash_adrs_remap((intptr_t)sym->ptr) = (uint64_t)val;
            break;
        default:
            *(uint32_t*)cash_adrs_remap((intptr_t)sym->ptr) = (uint32_t)val;
            break;
    }
}

/******************************************************************************
*
* cash_symtab_add - Add symbol table entry. Use internal storage for value 
* if <ptr> is NULL, otherwise use external storage (e.g. if importing
* symbol table). In the first case the storage is initialized with <val>.
*/
SYMTAB_ENTRY
    * cash_symtab_add (char *name, scantype type, uint64_t val, int *ptr,
                  uint64_t mask)
{
    SYMTAB_ENTRY *entry;

    if (!symTree)
        return NULL;
    
    if (mask == 0)
    {
        switch (type)
        {
            case T_ANON:
            case T_ANY:
                return NULL;
            case T_INT:
                mask = UINT_MASK;
                break;
            case T_CHARPTR:
            case T_SHORTPTR:
            case T_INTPTR:
            case T_INT64PTR:
            case T_STR:
            case T_FUNC:
                mask = PTR_MASK;
                break;
            default:
                return NULL;
        }
    }

    if (cash_symtab_find (name, T_ANY) != NULL)
    {
        tPrintf ("Symbol <%s> exists\n", name);
        return NULL;
    }

    entry = (SYMTAB_ENTRY *) calloc (1, sizeof (*entry));
    if (entry == NULL)
    {
        printf ("failed to allocate %lx bytes for symbol <%s>\n",
                sizeof (*entry), name);
        return NULL;
    }

    entry->type = type;
    entry->mask = mask;
    entry->name = (char *) malloc (strlen (name) + 1);
    if (entry->name == NULL)
    {
        printf ("failed to allocate 0x%lx bytes for string <%s>\n",
                strlen (name) + 1, name);
        free (entry);
        return NULL;
    }
    strcpy (entry->name, name);

    if (ptr != NULL)
    {
        entry->ptr = (uint64_t*)ptr;
    }
    else
    {
        entry->ptr = &entry->storage;
        cash_symval_set (entry, val);
    }

    if (btree_Insert (symTree, (void*)&entry))
    {
        printf ("failed to insert symbol %s\n", entry->name);
    }

    return entry;
}

SYMTAB_ENTRY
    * cash_fct_add (char *name, uint64_t val, FCT_PROTO proto)
{
    SYMTAB_ENTRY *entry = cash_symtab_add (name, T_FUNC, val, NULL, 0);

    if (!entry)
        return NULL;

    entry->proto = calloc (1, sizeof(FCT_PROTO));

    if (!entry->proto)
        return entry;

    *entry->proto = proto;

    return entry;
}


/******************************************************************************
*
* cash_sym_delete - Delete named symbol from symbol table
*/
void
cash_sym_delete (char *name)
{
    SYMTAB_ENTRY *entry;
    
    if (!symTree)
        return;
    
    entry = cash_symtab_find (name, T_ANY);
    
    if (!entry)
        return;
    
    btree_Delete (symTree, (void*)&entry);
    
    free (entry);
}


intptr_t dl_lookup (char *name)
{
#ifdef USE_LIBDL
    void *adrs;

    dlerror();
    adrs = dlsym (cash_dl_handle(NULL), name);

    if (dlerror() == NULL)
    {
        return (intptr_t)adrs;
    }
#endif
    return 0;
}

SYMTAB_ENTRY *dl_func_get (char *name, void *handle)
{
    SYMTAB_ENTRY *e = NULL;
#ifdef USE_LIBDL
    void *adrs;

    dlerror();
    adrs = dlsym (cash_dl_handle(handle), name);

    if (dlerror() == NULL)
    {
        e = cash_symtab_add (name, T_FUNC, (intptr_t)adrs, NULL, PTR_MASK);
    }
#endif
    return e;
}

int dl_func (char *name, void *handle)
{
    if (dl_func_get (name, handle) == NULL)
        return -1;
    
    return 0;
}

int dl_int (char *name, void *handle)
{
#ifdef USE_LIBDL
    void *adrs;

    dlerror();
    adrs = dlsym (cash_dl_handle(handle), name);

    if (dlerror() == NULL)
    {
        if (cash_symtab_add (name, T_INT, (intptr_t)adrs, (int*)adrs, UINT_MASK))
            return 0;
    }
#endif

    return -1;
}

void* cash_dl_adrs(char *name, void *handle)
{
#ifdef USE_LIBDL
    void *adrs;

    dlerror();
    adrs = dlsym (cash_dl_handle(handle), name);

    if (dlerror() == NULL)
    {
      return *(char**)adrs;
    }
#endif
  return NULL;
}

static int stat_cb (const void *arg1, void *usrArg, int spaces)
{
    struct search_arg *search = usrArg;
    SYMTAB_ENTRY **s1 = (SYMTAB_ENTRY**)arg1;
    
    if ((s1 == NULL) || (*s1 == NULL) || (search == NULL))
        return 0;

    switch ((*s1)->type)
        {
        case T_INT:
            search->integers++;
            break;
        case T_STR:
            search->strings++;
            break;
        case T_INTPTR:
        case T_SHORTPTR:
        case T_CHARPTR:
        case T_STRPTR:
        case T_FCTPTR:
            search->pointers++;
            break;
        case T_FUNC:
            search->functions++;
            break;
        default:
            break;
        }
    
    return 0;
}

void
cash_symtab_stat (void)
{
    struct search_arg search;

    if (!symTree)
        return;
    
    search.functions = 0;
    search.integers = 0;
    search.pointers = 0;
    search.strings = 0;

    btree_Walk (symTree, stat_cb, (void*)&search);

    printf ("%d integers, %d pointers, %d strings, %d functions\n",
            search.integers, search.pointers, search.strings, search.functions);
}

/******************************************************************************
*
* cash_new_string - Allocate and initialize new string of length <nchar> 
* (or strlen(<string>) if nchar is -1.
*/
char *
cash_new_string (char *string, int nchar)
{
    struct strtab_entry *entry;
    int elen;

    if (nchar == -1)
        nchar = strlen (string);

    /* try to find string in string table */
    for (entry = strtab; entry != NULL; entry = entry->next)
    {
        if ((entry->len == nchar) &&
            !strncmp (entry->str, string, nchar))
            return entry->str;
    }

    elen = sizeof (struct strtab_entry) + nchar + 1;

    entry = (struct strtab_entry *) malloc (elen);

    if (entry == NULL)
    {
        printf ("failed to allocate %d bytes for string\n", elen);
        return NULL;
    }

    entry->str = ((char *) entry) + sizeof (struct strtab_entry);
    entry->len = nchar;
    strncpy (entry->str, string, nchar);
    entry->str[nchar] = 0;

    entry->next = strtab;
    strtab = entry;

    return entry->str;
}

static char *mask_to_name (uint64_t mask)
{
    switch (mask)
    {
        case CHAR_MASK:
        case UCHAR_MASK:
            return ("char");
        case SHORT_MASK:
            return ("short");
        case ULL_MASK:
            return ("int64");
        default:
            return ("int");
    }
}

void
cash_show_val_str (VALUE * val, char *str, int slen)
{
    int size;
    int sval;
    int64_t lval;

    strcpy (str, "");

    switch (val->mask)
    {
    case CHAR_MASK:
        size = 7;
        break;
    case UCHAR_MASK:
        size = 8;
        break;
    case SHORT_MASK:
        size = 16;
        break;
    case ULL_MASK:
        size = 64;
        break;
    default:
        size = 32;
        break;
    }

    switch (val->type)
    {
    case T_INT:
        if (size == 7)
        {
            if ((val->val >= 0x20) && (val->val < 0x7f))
                str += sprintf (str, "Character: %d = 0x%02x = '%c'",
                        (char)val->val, (char)val->val, (char)val->val);
            else
                str += sprintf (str, "Character: %d = 0x%02x", (int)val->val, (int)val->val);
        }
        else if (size <= 32)
        {
            sval = (int)val->val & (1 << (size - 1)) ?
                val->val | (~val->mask) : val->val;

            str += sprintf (str, "Int%d: %d = %u = 0x%08x",
                    size, (int)sval, (int)val->val, (int)val->val);
        }
        else
        {
            lval = val->val & (1ULL << (size - 1)) ?
                val->val | (~val->mask) : val->val;

            str += sprintf (str, "Int%d: %lld = %llu = 0x%016llx",
                    size, lval, val->val, val->val);

        }
        break;
    case T_STR:
#ifdef HAVE_MEMPROBE
        str += sprintf (str, "String: %p : <%s>",
                (char*)((uintptr_t)val->val), cash_mem_probe ((char*)((uintptr_t)val->val)) ? (char*)cash_adrs_remap((uintptr_t)val->val) : "[MEMORY ERROR]");
#else
        str += sprintf (str, "String: %p", (char*)((uintptr_t)val->val));
#endif
        break;
    case T_CHARPTR:
        str += sprintf (str, "CharPtr: %p", (void*)((uintptr_t)val->val));
        break;
    case T_SHORTPTR:
        str += sprintf (str, "ShortPtr: %p", (void*)((uintptr_t)val->val));
        break;
    case T_INTPTR:
        str += sprintf (str, "IntPtr: %p", (void*)((uintptr_t)val->val));
        break;
    case T_INT64PTR:
        str += sprintf (str, "Int64Ptr: %p", (void*)((uintptr_t)val->val));
        break;
    case T_FCTPTR:
        str += sprintf (str, "FctPtr: %p", (void*)((uintptr_t)val->val));
        break;
    case T_STRPTR:
        str += sprintf (str, "StrPtr: %p", (void*)((uintptr_t)val->val));
        break;
    case T_FUNC:
        str += sprintf (str, "Function: %p", (void*)((uintptr_t)val->val));

        if (val->sym && val->sym->proto)
        {
            int i;

            str += sprintf (str, "(");
            for (i = 0; i < val->sym->proto->numArgs; i++)
            {
                str += sprintf ( str, "%s%s", mask_to_name (val->sym->proto->argsize[i]),
                        i < val->sym->proto->numArgs-1 ? ", " : "");
            }

            str += sprintf (str, "):%s", mask_to_name (val->sym->proto->rcsize));
        }
        break;
    default:
        str += sprintf (str, "?[%d]: %lld = %ulld = %llx, mask 0x%llx",
                val->type,
                val->val, val->val, val->val, val->mask);
        break;
    }

#if 0
    if (val->lv != 0)
    {
        printf (" [lv=%x]", val->lv);
    }

    if (val->sym != NULL)
        printf (" [%s@0x%08x]", val->sym->name, val->sym->ptr);
#endif

    str += sprintf (str, "\n");
}

void
cash_show_val (VALUE * val)
{
    int size;
    int sval;
    int64_t lval;

    switch (val->mask)
    {
    case CHAR_MASK:
        size = 7;
        break;
    case UCHAR_MASK:
        size = 8;
        break;
    case SHORT_MASK:
        size = 16;
        break;
    case ULL_MASK:
        size = 64;
        break;
    default:
        size = 32;
        break;
    }

    switch (val->type)
    {
    case T_INT:
        if (size == 7)
        {
            if ((val->val >= 0x20) && (val->val < 0x7f))
                printf ("Character: %d = 0x%02x = '%c'",
                        (char)val->val, (char)val->val, (char)val->val);
            else
                printf ("Character: %d = 0x%02x", (int)val->val, (int)val->val);
        }
        else if (size <= 32)
        {
            sval = (int)val->val & (1 << (size - 1)) ?
                val->val | (~val->mask) : val->val;

            printf ("Int%d: %d = %u = 0x%08x",
                    size, (int)sval, (int)val->val, (int)val->val);
        }
        else
        {
            lval = val->val & (1ULL << (size - 1)) ?
                val->val | (~val->mask) : val->val;

            printf ("Int%d: %lld = %llu = 0x%016llx",
                    size, lval, val->val, val->val);

        }
        break;
    case T_STR:
#ifdef HAVE_MEMPROBE
        printf ("String: %p : <%s>",
                (char*)((uintptr_t)val->val),
                cash_mem_probe ((char*)((uintptr_t)val->val)) ? (char*)cash_adrs_remap(val->val) : "[MEMORY ERROR]");
#else
        printf ("String: %p", (char*)((uintptr_t)val->val));
#endif
        break;
    case T_CHARPTR:
        printf ("CharPtr: %p", (void*)((uintptr_t)val->val));
        break;
    case T_SHORTPTR:
        printf ("ShortPtr: %p", (void*)((uintptr_t)val->val));
        break;
    case T_INTPTR:
        printf ("IntPtr: %p", (void*)((uintptr_t)val->val));
        break;
    case T_INT64PTR:
        printf ("Int64Ptr: %p", (void*)((uintptr_t)val->val));
        break;
    case T_FCTPTR:
        printf ("FctPtr: %p", (void*)((uintptr_t)val->val));
        break;
    case T_STRPTR:
        printf ("StrPtr: %p", (void*)((uintptr_t)val->val));
        break;
    case T_FUNC:
        printf ("Function: %p", (void*)((uintptr_t)val->val));

        if (val->sym && val->sym->proto)
        {
            int i;

            printf ("(");
            for (i = 0; i < val->sym->proto->numArgs; i++)
            {
                printf ( "%s%s", mask_to_name (val->sym->proto->argsize[i]),
                        i < val->sym->proto->numArgs-1 ? ", " : "");
            }

            printf ("):%s", mask_to_name (val->sym->proto->rcsize));
        }
        break;
    default:
        printf ("NIL");
        break;
    }

#if 0
    if (val->lv != 0)
    {
        printf (" [lv=%x]", val->lv);
    }

    if (val->sym != NULL)
        printf (" [%s@0x%08x]", val->sym->name, val->sym->ptr);
#endif
}

void *yp_async (void *arg)
{
        scan_ctx_t *ctx = arg;

        yyparse (ctx);

        free (ctx->str);
        free (ctx);

        return NULL;
}

/******************************************************************************
*
* cash_run_expr - Execute C expression
*
* The argc vector is concatenated into a single string. 
* The expression value is always printed if <verbose> is 2, is only printed
* if the expression is not an assignment if <verbose> is 1, and never printed
* if <verbose> is 0.
* The <verbose> parameter is ignored if the first character is a '/' (like 
* verbose=0) or '.' (like verbose=1).
*
* Returns 0 if the expression executed successfully and evaluates to non-0,
* Returns 1 for an expression error or an expression value of 0
*/
int
cash_run_expr (int argc, char *argv[], int verbose, VALUE *result)
{
    int len = 0;
    int i;
    int rc = 0;
    char *line = malloc(MAX_LINE_LENGTH*2 + 1);
    scan_ctx_t *ctx;

    if (line == NULL)
        return 1;

    for (i = 0; i < argc; i++)
    {
        len += strlen (argv[i]);

        if (len >= MAX_LINE_LENGTH)
        {
            printf ("expression too long (max %d bytes)\n", MAX_LINE_LENGTH);
            free (line);
            return 1;
        }
    }

    strcpy (line, "");
    for (i = 0; i < argc; i++)
    {
        if (i)
            strcat (line, " ");
        strcat (line, argv[i]);
    }

    if ((ctx = scan_init (line)) == NULL)
    {
        free (line);
        return 1;
    }

    ctx->verbose = verbose;

    if (line[0] == '.')
    {
        ctx->verbose = 1;
    }
    if (line[0] == '/')
    {
        ctx->verbose = 0;
    }


    if (ctx->async)
    {
        lastTid  = cash_spawn (yp_async, (void*)ctx, line);

        rc = 1;
    }
    else
    {
        if (yyparse (ctx))
        {
            printf ("<%s>\n %*s\n", line, ctx->idx-1, "^");
            rc = 1;
        }

        if (result)
        {
            *result = ctx->result;
        }

        free (line);
        free (ctx);
    }

    return rc;
}



int
cash_run_expr_s (char *line, int verbose, VALUE *result)
{
    char *argv[1];

    argv[0] = line;

    return cash_run_expr (1, argv, verbose, result);
}

uint32_t cash_version(void)
{
    return ((CASH_VERSION_1 << 16) | (CASH_VERSION_2 << 8) | CASH_VERSION_3);
}

int cash_version_show(void)
{
    printf ("cash version %d.%d.%d (%s %s)\n", 
        CASH_VERSION_1, CASH_VERSION_2, CASH_VERSION_3,
        __DATE__, __TIME__);
    
    return 0;
}

/*! allocate and initialize a pointer-to-constant storage
 * Returns NULL if the number of const-pointers in ctx is
 * exhausted, otherwise pointer to constant.
 */
uint64_t *cash_get_constp (scan_ctx_t *ctx, uint64_t val, uint64_t mask)
{
    void *ptr;
    
    if (ctx->const_ptr_count >= MAX_CONSTP)
        return NULL;

    ptr = &ctx->const_ptr[ctx->const_ptr_count++];

    /* initialize storage, big endian architectures need 
     * data width taken care of
     */
    switch (mask)
    {
        case CHAR_MASK:
        case UCHAR_MASK:
            *(uint8_t*)ptr = val;
            break;
        case SHORT_MASK:
            *(uint16_t*)ptr = val; 
            break;
        case ULL_MASK:
            *(uint64_t*)ptr = val; 
            break;
        default:
            *(uint32_t*)ptr = val;
            break;
    }

    return ptr;
}

int cash_is_helpstr (char *str)
{
    if (!strncmp(str, CASH_SHORT_PREFIX, sizeof(CASH_SHORT_PREFIX)-1) ||
        !strncmp(str, CASH_SHORT_PREFIX, sizeof(CASH_SHORT_PREFIX)-1))
    {
      return 1;
    }
    return 0;
}
static void cash_sym_update_help (SYMTAB_ENTRY *sym, void *dl_handle)
{
#ifdef INCLUDE_HELP
    char *tmp = alloca(strlen(sym->name) + 10);;
    char *str;

    if (sym->short_help || sym->long_help)
        return;

    sprintf (tmp, "%s%s", CASH_SHORT_PREFIX, sym->name);
    if (NULL != (str = cash_dl_adrs(tmp, dl_handle)))
    {
      sym->short_help = str;
    }
    else
    {
      sym->long_help = NULL;
      return;
    }

    sprintf (tmp, "%s%s", CASH_VERB_PREFIX, sym->name);

    str = cash_dl_adrs(tmp, dl_handle);
    sym->long_help = str;
#endif
}

/* try to find function name in help text, and
 * make a copy until the first empty line
 */
char *find_help(char *name)
{
    char *name_s = alloca(strlen(name + 5));
    char *base;
    sprintf (name_s, "%s (", name);

    base = __cash_help_str;
    char *hp = strstr(base, name_s);

    if (!hp)
    {
        base = __cash_cmd_help_str;
        hp = strstr(base, name_s);
    }

    if (!hp)
        return "";

    while (hp > base && isprint(*(hp-1)))
        hp--;

    char *hpe = hp;
    char *line_end = hpe;
    int empty_line = 1;
    while (1)
    {
        if (*hpe == '\n' || *hpe == 0)
        {
            if (empty_line)
                break;

            empty_line = 1;
            line_end = hpe;

            if (*hpe == 0)
                break;
        }
        else if (isprint(*hpe))
        {
            empty_line = 0;
        }
        hpe++;
    }

    size_t nchars = line_end - hp;

    if (nchars > 0)
    {
        char *help_str = malloc(nchars+1);
        memcpy(help_str, hp, nchars);
        help_str[nchars] = 0;

        return help_str;
    }
    return "";
}




#ifdef INCLUDE_HELP
int help (intptr_t addr)
#else
int cash_help (intptr_t addr)
#endif
{
    SYMTAB_ENTRY *toolHelp;
    fp  *helpFct;
    char *s = (char*)addr;
    SYMTAB_ENTRY *sym;
    
    if (addr == 0)
    {
        cash_version_show();
        printf (__cash_help_str);
        return 0;
    }

    sym = cash_addr_resolve (addr, T_FUNC, 1);
    if (!sym)
          sym = cash_symtab_find (s, T_ANY);

    if (sym)
    {
        cash_sym_update_help (sym, NULL);

        print_sym (sym);
        printf ("\n");

        if (NULL == sym->long_help)
        {
          sym->long_help = find_help(sym->name);
        }
        
        if (sym->long_help && strlen(sym->long_help) > 0)
            printf ("%s\n", sym->long_help);
        
        return 0;
    }
    
    if (!strcmp (s, "cmd"))
    {
        printf (__cash_cmd_help_str);
        return 0;
    }
    
    if (!strcmp (s, "tool"))
    {
        toolHelp = cash_symtab_find ("toolHelp", T_FUNC);

        if (toolHelp)
        {
            helpFct = (fp*)((uintptr_t)cash_symval_get (toolHelp));
            helpFct ();
        }
        else
        {
            printf ("No tool specific help\n");
        }
        return 0;
    }

    return 0;
}



#ifndef isprint
#define isprint(c) ((c >= 'a' && c <= 'z') || (c >= 'A' && c <= 'Z') || (c >= '0' && c <= '9'))
#endif

#ifndef MIN
#define MIN(a,b) (((a) < (b)) ? (a) : (b))
#endif


int cash_md (void *addr, int nbytes, int width)
{
    static void *saved_addr = 0;
    static int saved_width = 1;
    static int saved_nbytes = 0x100;
    static int saved_offset = 0;
    int start_offset;
    int off;
    char buffer[16];
    uint64_t *bp_ll = (uint64_t*)buffer;
    uint32_t *bp_i = (uint32_t*)buffer;
    uint16_t *bp_s = (uint16_t*)buffer;
    uint8_t *bp_c = (uint8_t*)buffer;
    int lbytes;
    int i;
    uint64_t caddr;
    int isLL = ((uintptr_t)addr > 0xffffffffULL);
    char *fmt;

    if (addr == NULL)
    {
        if (saved_addr == NULL)
            return -1;

        addr = saved_addr;
        start_offset = saved_offset;
    }
    else
    {
        saved_addr = addr;
        start_offset = 0;
    }
    
    if (width == 0)
        width = saved_width;

    if ((width != 1) && (width != 2) && (width != 4) && (width != 8))
        return -1;

    if (nbytes == 0)
        nbytes = saved_nbytes;

    saved_offset = start_offset + nbytes;
    
    saved_width = width;
    saved_nbytes = nbytes;
    
    if (isLL)
        printf ("0x%llx :\n", (uintptr_t)addr);
    else
        printf ("0x%x :\n", (uint32_t)(uintptr_t)addr);

    if (nbytes <= 0x100)
        fmt = "+0x%02x: ";
    else if (nbytes <= 10000)
        fmt = "+0x%04x: ";
    else
        fmt = "+0x%08x: ";

    for (off = 0; off < nbytes; off += 16)
    {
        lbytes = MIN(16, nbytes-off);
        caddr = (uint64_t)(uintptr_t)addr + (uint64_t)off + (uint64_t)start_offset;

        printf (fmt, (uint32_t)off + (uint32_t)start_offset);

        for (i = 0; i < 16/width; i++)
        {
            if (i < lbytes)
            {
                switch (width)
                {
                    case 8:
                        bp_ll[i] = *(uint64_t*)cash_adrs_remap(caddr + i*8);
                        printf ("%016llx ", bp_ll[i]);
                        break;
                    case 4:
                        bp_i[i] = *(uint32_t*)cash_adrs_remap(caddr + i*4);
                        printf ("%08x ", bp_i[i]);
                        break;
                    case 2:
                        bp_s[i] = *(uint16_t*)cash_adrs_remap(caddr + i*2);
                        printf ("%04x ", bp_s[i]);
                        break;
                    case 1:
                        bp_c[i] = *(uint8_t*)cash_adrs_remap(caddr + i*1);
                        printf ("%02x ", bp_c[i]);
                        break;
                }
            }
            else
            {
                printf ("%*c ", width*2, ' ');
            }
        }
        
        for (i = 0; i < lbytes; i++)
            printf ("%c", isprint(bp_c[i]) ? bp_c[i] : '.');
        
        printf ("\n");
    }
    return 0;
}

static int btree_compare (const void *arg1, const void *arg2)
{
    int rc;
    
    SYMTAB_ENTRY        **s1 = (SYMTAB_ENTRY**)arg1;
    SYMTAB_ENTRY        **s2 = (SYMTAB_ENTRY**)arg2;
    
    if (s1 == NULL || s2 == NULL)
        return 1;
    
    if (*s1 == *s2)
    {
        return 0;
    }
    
    rc = strcmp ((*s1)->name, (*s2)->name);
    
    return rc;
}

void
cash_expr_initialize (void)
{
    strtab = NULL;
    symTree = btree_Create (sizeof(SYMTAB_ENTRY*), btree_compare);

    DECL_PROTO (lssyms,    PA32, PA64);
    DECL_PROTO (symtab_anchor,    PA32);
    DECL_PROTO (i2s,       PA64, PA64);
    DECL_PROTO (lssyms,    PA32, PA64);
    DECL_PROTO (cash_escape_char, PA64, PA32, PA64, PA64);
    DECL_PROTO (cash_escape, PA64, PA64, PA64, PA64, PA64);
    DECL_PROTO (cash_bits, PA64, PA64, PA32, PA32);

#ifdef USE_LIBDL
    DECL_PROTO (dlopen,    PA64, PA64, PA32);
    DECL_PROTO (dlsym,     PA64, PA64, PA64);
    DECL_PROTO (dl_lookup, PA32, PA64);
    DECL_PROTO (dl_func,   PA32, PA64);
    DECL_PROTO (dl_int,    PA32, PA64);
#endif

    cash_fct_add ("cash_symtab_add", (intptr_t)cash_symtab_add, 
        (FCT_PROTO){PTR_MASK,  {}, 0});
    cash_symtab_add ("do_trace", T_INT, 0, &do_trace, 0);
    cash_symtab_add ("NULL", T_INT, 0, 0, PTR_MASK);
}

uint64_t cash_bits(uint64_t val, int lsb, int msb)
{
  int fbit = 0;
  char *fmt = "";
  int pop = 0;

  if (lsb > msb || lsb < 0 || msb < 0 || msb > 64 || lsb > 63)
    return val;

  if (lsb == 0 && msb == 0)
  {
    lsb = 0;
    msb = 63;
  }
  else
  {
    val = val >> lsb;
    uint64_t mask2 = (1ULL << (msb - lsb));
    if ((msb-lsb) > 0)
      mask2 |= mask2-1;

    val = val & mask2;
  }

  for (int i = msb-lsb; i >= 0; i--)
  {
    if (val & (1ULL<<i))
    {
      pop++;
      if (!fbit)
      {
        fbit = i;
        if (fbit >=32)
          fmt = "%2d :%2d : 0x%016llx\n";
        else if (fbit >= 16)
          fmt = "%2d :%2d : 0x%08llx\n";
        else if (fbit >= 8)
          fmt = "%2d :%2d : 0x%04llx\n";
        else
          fmt = "%2d :%2d : 0x%02llx\n";
      }

      printf(fmt, i+lsb, i, 1ULL<<i);
    }
  }
  return val;
}
