/*
Copyright 2002,2020 Felix Schmidt

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

#define _GNU_SOURCE

#include <sys/types.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/select.h>
#include <sys/stat.h>
#include <netinet/in.h>
#include <stdio.h>
#include <netdb.h>
#include <sys/time.h>
#include <signal.h>
#include <unistd.h>
#include <pthread.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#ifndef __APPLE__
#include <linux/un.h>
#endif
#include <stdlib.h>
#include <string.h>
#include <libgen.h>

#include "cash.h"

static pthread_mutex_t tty_list_mutex = PTHREAD_MUTEX_INITIALIZER;

typedef struct cash_tty
{
    int stdin, stdout, stderr;
    int socket;

    struct cash_tty *prev;
} cash_tty_t;

static cash_tty_t main_tty;
static cash_tty_t *current_tty;

/* prepare/check directory of AF_UNIX sockets
 */
int cash_sock_prepdir (char *sockFile)
{
    struct stat st;
    char *dir = dirname (strdup (sockFile));

    if (-1 == stat (dir, &st))
    {
        /* create the directory if it does not exist
         */
        if (-1 == mkdir (dir, 0700))
        {
            perror (dir);
            return 0;
        }
    }

    /* check directory permissions. Access to 
     * a named sockets is restricted by it's parent directory!
     */
    if (-1 == stat (dir, &st))
    {
        perror (dir);
        return 0;
    }

    if (!S_ISDIR(st.st_mode))
    {
        fprintf (stderr, "Not a directory: %s\n", dir);
        return 0;
    }
    if ((st.st_mode & (S_IRWXG|S_IRWXO)) != 0)
    {
        fprintf (stderr, "Wrong permssions: %s\n", dir);
        return 0;
    }
    
    return 1;
}

void cash_net_init(void)
{
    main_tty.stdin = dup(STDIN_FILENO);
    main_tty.stdout = dup(STDOUT_FILENO);
    main_tty.stderr = dup(STDERR_FILENO);
    main_tty.socket = -1;
    main_tty.prev = NULL;

    current_tty = &main_tty;
}

static cash_tty_t *cash_set_tty (int fd)
{
    cash_tty_t *new_tty = calloc (1, sizeof(cash_tty_t));

    if (new_tty == NULL)
        return NULL;

    pthread_mutex_lock (&tty_list_mutex);

    new_tty->prev = current_tty;

    new_tty->stdin = new_tty->stdout = new_tty->stderr = fd;
    new_tty->socket = fd;

    fflush (stdin);
    fflush (stdout);
    fflush (stderr);

    dup2 (fd, STDIN_FILENO);
    dup2 (fd, STDOUT_FILENO);
    dup2 (fd, STDERR_FILENO);

    if (isatty(fd))
    {
        setlinebuf (stdin);
        setlinebuf (stdout);
        setlinebuf (stderr);
    }
    else
    {
        setbuf (stdin, NULL);
        setbuf (stdout, NULL);
        setbuf (stderr, NULL);
    }
    
    current_tty = new_tty;

    pthread_mutex_unlock (&tty_list_mutex);

    return new_tty;
}

static void cash_release_tty (cash_tty_t *tty)
{
    cash_tty_t *t;

    pthread_mutex_lock (&tty_list_mutex);

    for (t = current_tty; t != NULL; t = t->prev)
    {
        if (t == tty)
            break;

        if (t->prev == tty)
        {
            t->prev = tty->prev;
            t = tty;
            break;
        }
    }

    if ((t != NULL) && (t->prev != NULL))
    {
        if (tty == current_tty)
        {
            fflush (stdin);
            fflush (stdout);
            fflush (stderr);

            dup2 (tty->prev->stdin, STDIN_FILENO);
            dup2 (tty->prev->stdout, STDOUT_FILENO);
            dup2 (tty->prev->stderr, STDERR_FILENO);

            if (isatty(STDIN_FILENO))
            {
                setlinebuf (stdin);
                setlinebuf (stdout);
                setlinebuf (stderr);
            }
            else
            {
                setbuf (stdin, NULL);
                setbuf (stdout, NULL);
                setbuf (stderr, NULL);
            }

            current_tty = tty->prev;
        }
    }

    pthread_mutex_unlock (&tty_list_mutex);

    free (tty);
}

static int inet_sock (cash_netserver_args_t *args, char *mode)
{
    struct sockaddr_in  sin;
    int f = socket (AF_INET, SOCK_STREAM, 0);
 
    if (f < 0)
    {
        perror ("socket");
        return -1;
    }
 
    bzero (&sin, sizeof(sin));
    sin.sin_family      = AF_INET;
    sin.sin_port        = htons (args->port);
    if (args->local_only)
        sin.sin_addr.s_addr = inet_addr("127.0.0.1");
 
    while (bind (f, (struct sockaddr *) &sin, sizeof (sin)) < 0)
    {
        sleep (5);
    }
 
    printf ("TCP server at port %d (%s)\n", args->port, mode);
    
    return f;
} 
 
#ifndef __APPLE__
static int unix_sock (cash_netserver_args_t *args, char *mode)
{
    struct sockaddr_un  sun;
    int f = socket (PF_UNIX, SOCK_STREAM, 0);
 
    if (f < 0)
    {
        perror ("socket");
        return -1;
    }
 
    unlink (args->af_unix_name);
    if (!cash_sock_prepdir (args->af_unix_name))
        return -1;
 
    bzero (&sun, sizeof(sun));
    sun.sun_family      = AF_UNIX;
    snprintf(sun.sun_path, UNIX_PATH_MAX, args->af_unix_name);
 
    while (bind (f, (struct sockaddr *) &sun, sizeof (sun)) < 0)
    {
        sleep (5);
    }
 
    if (getenv ("cash_verbose"))
        printf ("Server at socket %s (%s)\n", args->af_unix_name, mode);
    
    return f;
} 
#endif
 
int cash_netserver(cash_netserver_args_t *args)
{
    int f, s;
    char buffer[10000];
    int i;
    int rc;
    char *mode;
    struct sockaddr_in  from_i;
#ifndef __APPLE__
    struct sockaddr_un  from_u;
#endif
    struct sockaddr *from;
    socklen_t from_len;
    cash_tty_t *tty;

    if (!args)
        return 1;

    switch (args->mode)
    {
        case ns_mode_tty:
            mode = "tty redirected";
            break;
        
        case ns_mode_line_redir:
            mode = "stdin/out redirected while parsing";
            break;
        
        case ns_mode_line:
            mode = "stdin/out NOT redirected";
            break;

        case ns_mode_redir:
#if 0
            /* no longer supported
             */
            mode = "redirect";
            break;
#endif

        default:
            return 1;
    }


#ifndef __APPLE__
    if (args->af_unix_name)
    {
        f = unix_sock (args, mode);
        from = (struct sockaddr*)&from_u;
        from_len = sizeof(from_u);
    }
    else
#endif
    {
        f = inet_sock (args, mode);
        from = (struct sockaddr*)&from_i;
        from_len = sizeof(from_i);
    }
    
    if (f == -1)
        return 0;

    bzero (&from_i, sizeof(from_i));
#ifndef __APPLE__
    bzero (&from_u, sizeof(from_u));
#endif

    while (1)
    {
        if (listen (f, 1) < 0)
        {
            perror ("listen\n");
            return 0;
        }

        if ((s = accept (f, from, &from_len))  == -1)
        {
            perror ("accept");
            continue;
        }

#if 0
        /* no longer supported 
         */
        if (args->mode == ns_mode_redir)
        {
            dup2 (s, STDIN_FILENO);
            dup2 (s, STDOUT_FILENO);
            dup2 (s, STDERR_FILENO);

            return 0;
        }
#endif

        if (args->mode == ns_mode_tty)
        {
            printf ("Connection on port %d, redirecting console to socket ..\n", args->port );

            tty = cash_set_tty (s);

            if (tty == NULL)
            {
                close (s);
                continue;
            }

            cash_run_file (s, NULL);

            cash_release_tty (tty);

            close (s);
            continue;
        }

        strcpy (buffer, "You are connected to ");
        cash_getexename (buffer + strlen(buffer), sizeof(buffer));
        strcat (buffer, " (");
        strcat (buffer, mode);
        strcat (buffer, ")\n");

        write (s, buffer, strlen(buffer));

        while (1)
        {
            write (s, cash_prompt(), strlen(cash_prompt()));

            for (i = 0; i < sizeof(buffer);)
            {
                rc = read (s, &buffer[i], 1);

                if (rc <= 0)
                {
                    i = -1;
                    break;
                }

                if ((buffer[i] == '\n') || (buffer[i] == '\0'))
                {
                    buffer[i] = 0;
                    break;
                }

                i += rc;
            }

            if (i == -1)
            {
                close (s);
                break;
            }
            else
            {
                VALUE result;

                if (args->mode == ns_mode_line_redir)
                {
                    tty = cash_set_tty (s);
                }

                if (cash_segv_catch())
                    cash_run_expr_s (buffer, 1, &result);

                if (args->mode == ns_mode_line_redir)
                {
                    cash_release_tty (tty);
                }
                else
                {
                    char str[1024];

                    cash_show_val_str (&result, str, sizeof(str));
                    write (s, str, strlen(str));
                }
            }
        }
    }

    return 1;
}

/* start TCP network server at port/mode
 */
int cash_net (int port, netserver_mode_t mode, int public)
{
    cash_netserver_args_t args;
    
    bzero ((void*)&args, sizeof(args));
 
    args.port = port;
    args.mode = mode;
    args.local_only = !public;
 
    return cash_netserver (&args);
}
 
/* Start server listening on AF_UNIX socket at pipedir/pipename
 * pipedir is created if it does not exist, otherwise it must be a directory
 * with permission 0700.
 */
int cash_net_local (char *pipedir, char *pipename, netserver_mode_t mode)
{
    cash_netserver_args_t args;
    struct stat st;
    
    if ((pipedir == NULL) || (pipename == NULL))
        return -1;
 
    bzero ((void*)&args, sizeof(args));
 
    args.mode = mode;
    args.af_unix_name = malloc (strlen(pipedir)+strlen(pipename)+2);
    sprintf (args.af_unix_name, "%s/%s", pipedir, pipename);
    
    if (-1 == stat (pipedir, &st))
    {
        /* create the directory if it does not exist
         */
        mkdir (pipedir, 0700);
    }
 
    /* check directory permissions in case it already exists. Access to 
     * the named socket is restricted by it's parent directory!
     */
    if (-1 == stat (pipedir, &st))
    {
        perror ("stat");
        return -1;
    }
 
    if (!S_ISDIR(st.st_mode))
    {
        fprintf (stderr, "Not a directory: %s\n", pipedir);
        return 0;
    }
    if ((st.st_mode & (S_IRWXG|S_IRWXO)) != 0)
    {
        fprintf (stderr, "Wrong permssions: %s\n", pipedir);
        return 0;
    }
 
    return cash_netserver (&args);
}
 
