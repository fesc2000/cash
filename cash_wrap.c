/*
Copyright 2002,2020 Felix Schmidt

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

/* This wrapper code makes the cash library usable as debugger. 
 * It will wrap around the glibc (or uClibc) initialization function and
 * start a cash netserver before invoking the actual main() function of the
 * application.
 *
 * Specify LD_PRELOAD=cash_wrap.so before calling the program to be
 * instrumented, and set CASH_LIB to path of cash_wrap.so
 *
 * The wrapper will create a AF_UNIX socket file in /tmp/cash/<pid>.
 *
 * Environmant variables: 
 *  LD_PRELOAD
 *    Expected to contain cash_wrap.so
 *  CASH_ORG_LD_PRELOAD
 *    Original LD_PRELOAD without cash_wrap.so
 *  CASH_LIB
 *        Path to cash_wrap.so fir symbol load
 *  cash_verbose
 *        Can be defined for some additional output
 *  cash_wrap_autoexec
 *        If defined, application will be started automatically. 
 *        If not, a shell is started and application needs to be
 *        started manually with "cash_app_start"
 */

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/stat.h>
#define __USE_GNU
#include <dlfcn.h>

#include "cash.h"

#ifndef REDIR_PTHREAD_CREATE
# if defined(__UCLIBC__)
#  define REDIR_PTHREAD_CREATE 0
# else
#  define REDIR_PTHREAD_CREATE 1
# endif
#endif

#if !defined(__UCLIBC__)
#define CRT_MAIN      __libc_start_main
#define CRT_MAIN_NAME "__libc_start_main"
#else
#define CRT_MAIN      __uClibc_main
#define CRT_MAIN_NAME "__uClibc_main"
#endif

#ifndef __APPLE__
  #define PRELOAD_VAR "LD_PRELOAD"
#else
  #define PRELOAD_VAR "DYLD_INSERT_LIBRARIES"
#endif

/* the applications main function
 */
static int    (*appMain)(int, char **, char**) = NULL;
static int    appArgc;
static char** appArgv;
static char** appArgv2;
static char   cashPrompt[1024] = "CASH> ";
static int    cash_verbose = 0;
static char   sockdir[100] = "";
static char   sockname[100] = "";

#if REDIR_PTHREAD_CREATE == 1
int (*org_pthread_create)(pthread_t *thread, const pthread_attr_t *attr,
                          void *(*start_routine) (void *), void *arg) = NULL;
#endif

intptr_t cash_adrs_remap (intptr_t adrs)
{
    return adrs;
}

intptr_t cash_virt_to_io ( intptr_t virt)
{
    return virt;
}

char *cash_prompt(void)
{
    return cashPrompt;
}

void cash_do_io_map (void)
{
}

#if REDIR_PTHREAD_CREATE == 1

/* intercept call to pthread_create so that we can handle threads
 * using our own task functions
 */
int pthread_create(pthread_t *thread, const pthread_attr_t *attr,
                          void *(*start_routine) (void *), void *arg)
{
    char *name = NULL;
    SYMTAB_ENTRY *se;
    int rc;

    if (org_pthread_create==NULL)
    {
        printf ("### pthread_create called but no !org_pthread_create!\n");
        return 0;
    }

    /* cash_spawn..() will always use the cash_task_wrap function as entry
     */
    if ((void*)start_routine == (void*)cash_task_wrap)
    {
        return org_pthread_create (thread, attr, start_routine, arg);
    }

    /* find address in symbol table to name the thread
     */
    se = cash_addr_resolve ((uintptr_t)start_routine, T_ANY, 1);

    if (se == NULL)
        name = "<anon>";
    else
        name = se->name;

    /* spawn thread */
    if ((rc = cash_spawn2 (thread, attr, start_routine, arg, name)) == 0)
    {
        printf ("### New thread: %s@%p\n", name, start_routine);
    }
    return rc;
}
#endif

/* start the main application's main function with original
 * arguments
 */
int cash_app_start(void)
{
    int rc;
    
    if (cash_verbose)
        printf ("calling main\n");

    rc = appMain (appArgc, appArgv, appArgv2);

    if (cash_verbose)
        printf ("done, returns %d\n", rc);

    return rc;
}

static void cleanup_socket(void)
{
    struct stat st;

    if (stat (sockname, &st) == 0)
        unlink (sockname);

    if (stat (sockdir, &st) == 0)
        rmdir (sockdir);
}

static void *cash_netserver_thread (void *arg)
{
    cash_netserver (arg);

    return NULL;
}

int newmain (int argc, char **argv, char **argv2 )
{
    int rc;
    cash_netserver_args_t args;
    char *lib = getenv ("CASH_LIB");
    char *prompt, *s;
    int run_netserver = 0;

    /* make sure to not invoke the wrapper for further programs ..
     */
    s = getenv("CASH_ORG_LD_PRELOAD");
    if (s)
    {
        setenv (PRELOAD_VAR, s, 1);
    }
    else
    {
        unsetenv (PRELOAD_VAR);
    }

    cash_verbose = (getenv ("cash_verbose") != NULL);

    /* configure command socket.
     * Default is AF_UNIX, $cash_socket_local_port opens local AF_INET port */
    args.mode = ns_mode_line_redir;
    s = getenv("cash_socket_local_port");
    if (s)
    {
      args.local_only = 1;
      args.port = atoi(s);
      if (args.port)
      {
          run_netserver = 1;

          if (cash_verbose)
              printf ("Netserver at local port %d\n", args.port);
      }
    }
#ifndef __APPLE__
    else
    {
      args.mode = ns_mode_line_redir;
      args.af_unix_name = sockname;

      snprintf (sockdir, sizeof(sockdir), "/tmp/cash_%d", getpid());
      snprintf (sockname, sizeof(sockname), "%s/0", sockdir);

      run_netserver = 1;

      if (cash_verbose)
          printf ("Netserver at %s\n", sockname);
    }
#endif

    if (cash_verbose)
        printf ("Initializing cash ..\n");

    cash_init (argv[0], NULL);

    if (lib)
        cash_load_symtab_from_file (lib, 1, NULL);

    if (run_netserver)
        cash_spawn (cash_netserver_thread, &args, "netserver");

    atexit (cleanup_socket);

    /* define the prompt, consisting of executable name and pid
     */
    for (prompt = s = argv[0]; *s != 0; s++)
    {
        if (*s == '/')
            prompt = s+1;
    }
    snprintf (cashPrompt, sizeof(cashPrompt), "%s[%d]> ", prompt, getpid());

    appArgc = argc;
    appArgv = argv;
    appArgv2 = argv2;

    if (getenv ("cash_wrap_autoexec"))
    {
        rc = cash_app_start ();
    }
    else
    {
        printf ("main() execution inhibited. Enter the following command to spawn application:\n"
                "cash_app_start&\n");
        rc = cash_run_file (STDIN_FILENO, NULL);
    }

    exit (rc);
}

#ifdef __APPLE__
int _main (int argc, char **argv)
{
  printf ("calling %p\n", appMain);
  return newmain(argc, argv, NULL);
}

void __attribute((constructor))init()
{
    appMain = dlsym(dlopen("./cash", RTLD_LAZY), "main");
    if (NULL == appMain) {
        fprintf(stderr, "failed to resolve main(): %s\n", dlerror());
    }
    printf ("%p %p\n", appMain, _main);
}


#else

int CRT_MAIN (int(*main)(int, char **, char **), int argc, char *ubp_av,
             __typeof (main) init, void(*fini)(void), void(*rtld_fini)(void), void *stack_end)
{
    int (*org)(int(*main)(int, char **, char ** ), int argc, char *ubp_av,
                           __typeof (main) init, void(*fini)(void), void(*rtld_fini)(void), void *stack_end);
    
    org = dlsym(RTLD_NEXT, CRT_MAIN_NAME);
#if REDIR_PTHREAD_CREATE == 1
    org_pthread_create = dlsym(RTLD_NEXT, "pthread_create");
#endif

    if (NULL == org) {
        fprintf(stderr, "failed to resolve main(): %s\n", dlerror());
        exit (1);
    }

    appMain = main;

    return org (newmain, argc, ubp_av, init, fini, rtld_fini, stack_end);
}
#endif
