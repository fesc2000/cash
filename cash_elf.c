
/*
Copyright 2002,2020 Felix Schmidt

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

#include <stdio.h>
#include <string.h>
#include <link.h>
#include <elf.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/mman.h>
#include <fcntl.h>
#include <unistd.h>
#include <dlfcn.h>

#include "cash.h"

static void import_syms (char *strtab, 
    ElfW(Sym) *sym, 
    unsigned int symcount, 
    int use_libdl, 
    char *file,
    void *handle)
{
    int i;
    char name[1024];
    char *tmp;
    int fcount = 0, icount = 0;

    for(i = 0; i < symcount; i++, sym++)
    {
        strcpy (name, &strtab[sym->st_name]);
        tmp = strstr (name, "@@");
        if (tmp)
            *tmp = 0;

        if (!cash_is_helpstr(name))
        {
            switch (ELF32_ST_TYPE(sym->st_info))
            {
                case STT_FUNC:
                    if (sym->st_value && !use_libdl)
                    {
                        cash_symtab_add (name, T_FUNC, (int)sym->st_value, NULL, PTR_MASK);
                    }
                    else
                    {
                        dl_func (name, handle);
                    }
                    fcount++;
                    break;

                case STT_OBJECT:
                    if (sym->st_value && !use_libdl)
                    {
                        cash_symtab_add (name, T_INT, (int)sym->st_value, (int*)sym->st_value, UINT_MASK);
                    }
                    else
                    {
                        dl_int (name, handle);
                    }
                    icount++;
                    break;
            }
        }
    }
    
    if (getenv ("cash_verbose"))
        printf (" Loaded %d STT_FUNC and %d STT_OBJECT types from %s\n", fcount, icount, file);
}

/* Search static and dynamic symbol tables in a file, try to resolve them and add
 * them to the cash symbol table if successful.
 * STT_FUNC symbols are trated as functions, STT_OBJECT as integers
 *
 */
int
cash_load_symtab_from_file (char *file, int use_libdl, void *handle)
{
    unsigned long sz;
    int i;
    ElfW(Ehdr)* exheader;
    ElfW(Shdr)* header;
    char* strtab[10];
    unsigned int strtab_id[10];
    unsigned int n_strtabs = 0;
    int k;
    void *base = NULL;

    base = mapfile (file, &sz);

    if (!base)
    {
        fprintf (stderr, "Failed to read symbols from %s\n", file);
        return -1;
    }

    exheader = base;
#ifdef DEBUG
    printf ("loading symbol table from %s [%p]\n", file, exheader);
#endif

    /* search all string table sections and remember their index.
     */
    header = (ElfW(Shdr)*)((char*)exheader + exheader->e_shoff);
    for(i = 0; i < exheader->e_shnum; i++)
    {
        if(header->sh_type == SHT_STRTAB)
        {
            strtab[n_strtabs] = (char*)((char*)exheader + header->sh_offset);
            strtab_id[n_strtabs] = i;
            n_strtabs++;
            
            if (n_strtabs >= 10)
                break;
        }
        header++;
    }

    if (n_strtabs == 0)
        goto out;

    /* look for all symbol table sections
     */
    header = (ElfW(Shdr)*)((char*)exheader + exheader->e_shoff);
    for(i = 0; i < exheader->e_shnum; i++)
    {
        if ((header->sh_type == SHT_SYMTAB) || (header->sh_type == SHT_DYNSYM))
        {
            /* check if section refers to a string section. If yes, resolve
             * and import all symbols.
             */
            for (k = 0; k < n_strtabs; k++)
            {
                if (header->sh_link == strtab_id[k])
                {
                    import_syms (strtab[k], 
                        (ElfW(Sym)*)((char*)exheader + header->sh_offset),
                        header->sh_size / sizeof(ElfW(Sym)), 
                        use_libdl, 
                        file,
                        handle);
                }
            }
        }
        header++;
    }

out:
    if (base)
        munmap (base, sz);

    return 0;
} 
