/*
Copyright 2002,2020 Felix Schmidt

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

#include <stdio.h>
#include <cash.h>


intptr_t cash_adrs_remap (intptr_t adrs)
{
        return adrs;
}

intptr_t cash_virt_to_io ( intptr_t virt)
{
    return virt;
}


char *cash_prompt(void)
{
        return "CASH> ";
}

void cash_do_io_map (void)
{
}


int main (int argc, char **argv)
{
    VALUE   result;
    int rc;
    
    rc = cash_main (argc, argv, NULL, &result);

    printf ("Result: ");
    cash_show_val (&result);

    return rc;
}
