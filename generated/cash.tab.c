/* A Bison parser, made by GNU Bison 3.0.4.  */

/* Bison implementation for Yacc-like parsers in C

   Copyright (C) 1984, 1989-1990, 2000-2015 Free Software Foundation, Inc.

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.  */

/* As a special exception, you may create a larger work that contains
   part or all of the Bison parser skeleton and distribute that work
   under terms of your choice, so long as that work isn't itself a
   parser generator using the skeleton or a modified version thereof
   as a parser skeleton.  Alternatively, if you modify or redistribute
   the parser skeleton itself, you may (at your option) remove this
   special exception, which will cause the skeleton and the resulting
   Bison output files to be licensed under the GNU General Public
   License without this special exception.

   This special exception was added by the Free Software Foundation in
   version 2.2 of Bison.  */

/* C LALR(1) parser skeleton written by Richard Stallman, by
   simplifying the original so-called "semantic" parser.  */

/* All symbols defined below should begin with yy or YY, to avoid
   infringing on user name space.  This should be done even for local
   variables, as they might otherwise be expanded by user macros.
   There are some unavoidable exceptions within include files to
   define necessary library symbols; they are noted "INFRINGES ON
   USER NAME SPACE" below.  */

/* Identify Bison output.  */
#define YYBISON 1

/* Bison version.  */
#define YYBISON_VERSION "3.0.4"

/* Skeleton name.  */
#define YYSKELETON_NAME "yacc.c"

/* Pure parsers.  */
#define YYPURE 1

/* Push parsers.  */
#define YYPUSH 0

/* Pull parsers.  */
#define YYPULL 1




/* Copy the first part of user declarations.  */
#line 1 "cash.y" /* yacc.c:339  */

#include <stdlib.h>
#include <stdio.h>
#include "cash.h"
#ifdef USE_LIBDL
#include <dlfcn.h>
#endif

void yyerror( scan_ctx_t *scan_ctx, const char *fmt, ... );
void cash_show_val (VALUE *val);

static int assign_op (scan_ctx_t*, VALUE *vl, VALUE *vr, int op);
static SYMTAB_ENTRY *new_sym (char *name, scantype type, VALUE *val, uint64_t mask);
static SYMTAB_ENTRY *new_libdl_sym (char *name, scantype type, VALUE *val, uint64_t mask);
static void funcCall (scan_ctx_t*, cash_fp *func, SYMTAB_ENTRY *sym, VALUE *args, VALUE *res);
static void init_proto_args (VALUE *pa);
static void add_pa_type (VALUE *pa, VALUE *t);
static void get_sym (VALUE *val, SYMTAB_ENTRY *sym);
static int pushScope (scan_ctx_t*, int activate);
static int popScope (scan_ctx_t*);

#define SCOPE_ACTIVE       (scan_ctx->scope[scan_ctx->scope_id].active)
#define CHECK_SCOPE        if (!SCOPE_ACTIVE) break;
#define SCOPE_START(cond)  if (pushScope(scan_ctx, cond)) YYABORT;
#define SCOPE_END          if (popScope(scan_ctx)) YYABORT;

cash_fp *cash_set_argc = NULL;



#line 97 "cash.tab.c" /* yacc.c:339  */

# ifndef YY_NULLPTR
#  if defined __cplusplus && 201103L <= __cplusplus
#   define YY_NULLPTR nullptr
#  else
#   define YY_NULLPTR 0
#  endif
# endif

/* Enabling verbose error messages.  */
#ifdef YYERROR_VERBOSE
# undef YYERROR_VERBOSE
# define YYERROR_VERBOSE 1
#else
# define YYERROR_VERBOSE 1
#endif

/* In a future release of Bison, this section will be replaced
   by #include "cash.tab.h".  */
#ifndef YY_YY_CASH_TAB_H_INCLUDED
# define YY_YY_CASH_TAB_H_INCLUDED
/* Debug traces.  */
#ifndef YYDEBUG
# define YYDEBUG 0
#endif
#if YYDEBUG
extern int yydebug;
#endif

/* Token type.  */
#ifndef YYTOKENTYPE
# define YYTOKENTYPE
  enum yytokentype
  {
    STRING = 258,
    NUMBER = 259,
    CHAR = 260,
    ANON_ID = 261,
    LIBDL_ID = 262,
    FUNC_ID = 263,
    VAR_ID = 264,
    AND = 265,
    RSHIFT = 266,
    LSHIFT = 267,
    KW_VOID = 268,
    KW_CHAR = 269,
    KW_UINT8 = 270,
    KW_UINT16 = 271,
    KW_UINT32 = 272,
    KW_UINT64 = 273,
    KW_LONG = 274,
    MULA = 275,
    DIVA = 276,
    MODA = 277,
    ADDA = 278,
    SUBA = 279,
    SHLA = 280,
    SHRA = 281,
    ANDA = 282,
    ORA = 283,
    XORA = 284,
    OR = 285,
    GE = 286,
    LE = 287,
    EQ = 288,
    NE = 289,
    AI = 290,
    AD = 291,
    KW_SIZEOF = 292,
    DOTDOT = 293,
    ITER_COND = 294,
    ITER_START = 295,
    ITER_END = 296,
    UNARY = 297
  };
#endif

/* Value type.  */
#if ! defined YYSTYPE && ! defined YYSTYPE_IS_DECLARED
typedef int YYSTYPE;
# define YYSTYPE_IS_TRIVIAL 1
# define YYSTYPE_IS_DECLARED 1
#endif



int yyparse (scan_ctx_t *scan_ctx);

#endif /* !YY_YY_CASH_TAB_H_INCLUDED  */

/* Copy the second part of user declarations.  */

#line 190 "cash.tab.c" /* yacc.c:358  */

#ifdef short
# undef short
#endif

#ifdef YYTYPE_UINT8
typedef YYTYPE_UINT8 yytype_uint8;
#else
typedef unsigned char yytype_uint8;
#endif

#ifdef YYTYPE_INT8
typedef YYTYPE_INT8 yytype_int8;
#else
typedef signed char yytype_int8;
#endif

#ifdef YYTYPE_UINT16
typedef YYTYPE_UINT16 yytype_uint16;
#else
typedef unsigned short int yytype_uint16;
#endif

#ifdef YYTYPE_INT16
typedef YYTYPE_INT16 yytype_int16;
#else
typedef short int yytype_int16;
#endif

#ifndef YYSIZE_T
# ifdef __SIZE_TYPE__
#  define YYSIZE_T __SIZE_TYPE__
# elif defined size_t
#  define YYSIZE_T size_t
# elif ! defined YYSIZE_T
#  include <stddef.h> /* INFRINGES ON USER NAME SPACE */
#  define YYSIZE_T size_t
# else
#  define YYSIZE_T unsigned int
# endif
#endif

#define YYSIZE_MAXIMUM ((YYSIZE_T) -1)

#ifndef YY_
# if defined YYENABLE_NLS && YYENABLE_NLS
#  if ENABLE_NLS
#   include <libintl.h> /* INFRINGES ON USER NAME SPACE */
#   define YY_(Msgid) dgettext ("bison-runtime", Msgid)
#  endif
# endif
# ifndef YY_
#  define YY_(Msgid) Msgid
# endif
#endif

#ifndef YY_ATTRIBUTE
# if (defined __GNUC__                                               \
      && (2 < __GNUC__ || (__GNUC__ == 2 && 96 <= __GNUC_MINOR__)))  \
     || defined __SUNPRO_C && 0x5110 <= __SUNPRO_C
#  define YY_ATTRIBUTE(Spec) __attribute__(Spec)
# else
#  define YY_ATTRIBUTE(Spec) /* empty */
# endif
#endif

#ifndef YY_ATTRIBUTE_PURE
# define YY_ATTRIBUTE_PURE   YY_ATTRIBUTE ((__pure__))
#endif

#ifndef YY_ATTRIBUTE_UNUSED
# define YY_ATTRIBUTE_UNUSED YY_ATTRIBUTE ((__unused__))
#endif

#if !defined _Noreturn \
     && (!defined __STDC_VERSION__ || __STDC_VERSION__ < 201112)
# if defined _MSC_VER && 1200 <= _MSC_VER
#  define _Noreturn __declspec (noreturn)
# else
#  define _Noreturn YY_ATTRIBUTE ((__noreturn__))
# endif
#endif

/* Suppress unused-variable warnings by "using" E.  */
#if ! defined lint || defined __GNUC__
# define YYUSE(E) ((void) (E))
#else
# define YYUSE(E) /* empty */
#endif

#if defined __GNUC__ && 407 <= __GNUC__ * 100 + __GNUC_MINOR__
/* Suppress an incorrect diagnostic about yylval being uninitialized.  */
# define YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN \
    _Pragma ("GCC diagnostic push") \
    _Pragma ("GCC diagnostic ignored \"-Wuninitialized\"")\
    _Pragma ("GCC diagnostic ignored \"-Wmaybe-uninitialized\"")
# define YY_IGNORE_MAYBE_UNINITIALIZED_END \
    _Pragma ("GCC diagnostic pop")
#else
# define YY_INITIAL_VALUE(Value) Value
#endif
#ifndef YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN
# define YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN
# define YY_IGNORE_MAYBE_UNINITIALIZED_END
#endif
#ifndef YY_INITIAL_VALUE
# define YY_INITIAL_VALUE(Value) /* Nothing. */
#endif


#if ! defined yyoverflow || YYERROR_VERBOSE

/* The parser invokes alloca or malloc; define the necessary symbols.  */

# ifdef YYSTACK_USE_ALLOCA
#  if YYSTACK_USE_ALLOCA
#   ifdef __GNUC__
#    define YYSTACK_ALLOC __builtin_alloca
#   elif defined __BUILTIN_VA_ARG_INCR
#    include <alloca.h> /* INFRINGES ON USER NAME SPACE */
#   elif defined _AIX
#    define YYSTACK_ALLOC __alloca
#   elif defined _MSC_VER
#    include <malloc.h> /* INFRINGES ON USER NAME SPACE */
#    define alloca _alloca
#   else
#    define YYSTACK_ALLOC alloca
#    if ! defined _ALLOCA_H && ! defined EXIT_SUCCESS
#     include <stdlib.h> /* INFRINGES ON USER NAME SPACE */
      /* Use EXIT_SUCCESS as a witness for stdlib.h.  */
#     ifndef EXIT_SUCCESS
#      define EXIT_SUCCESS 0
#     endif
#    endif
#   endif
#  endif
# endif

# ifdef YYSTACK_ALLOC
   /* Pacify GCC's 'empty if-body' warning.  */
#  define YYSTACK_FREE(Ptr) do { /* empty */; } while (0)
#  ifndef YYSTACK_ALLOC_MAXIMUM
    /* The OS might guarantee only one guard page at the bottom of the stack,
       and a page size can be as small as 4096 bytes.  So we cannot safely
       invoke alloca (N) if N exceeds 4096.  Use a slightly smaller number
       to allow for a few compiler-allocated temporary stack slots.  */
#   define YYSTACK_ALLOC_MAXIMUM 4032 /* reasonable circa 2006 */
#  endif
# else
#  define YYSTACK_ALLOC YYMALLOC
#  define YYSTACK_FREE YYFREE
#  ifndef YYSTACK_ALLOC_MAXIMUM
#   define YYSTACK_ALLOC_MAXIMUM YYSIZE_MAXIMUM
#  endif
#  if (defined __cplusplus && ! defined EXIT_SUCCESS \
       && ! ((defined YYMALLOC || defined malloc) \
             && (defined YYFREE || defined free)))
#   include <stdlib.h> /* INFRINGES ON USER NAME SPACE */
#   ifndef EXIT_SUCCESS
#    define EXIT_SUCCESS 0
#   endif
#  endif
#  ifndef YYMALLOC
#   define YYMALLOC malloc
#   if ! defined malloc && ! defined EXIT_SUCCESS
void *malloc (YYSIZE_T); /* INFRINGES ON USER NAME SPACE */
#   endif
#  endif
#  ifndef YYFREE
#   define YYFREE free
#   if ! defined free && ! defined EXIT_SUCCESS
void free (void *); /* INFRINGES ON USER NAME SPACE */
#   endif
#  endif
# endif
#endif /* ! defined yyoverflow || YYERROR_VERBOSE */


#if (! defined yyoverflow \
     && (! defined __cplusplus \
         || (defined YYSTYPE_IS_TRIVIAL && YYSTYPE_IS_TRIVIAL)))

/* A type that is properly aligned for any stack member.  */
union yyalloc
{
  yytype_int16 yyss_alloc;
  YYSTYPE yyvs_alloc;
};

/* The size of the maximum gap between one aligned stack and the next.  */
# define YYSTACK_GAP_MAXIMUM (sizeof (union yyalloc) - 1)

/* The size of an array large to enough to hold all stacks, each with
   N elements.  */
# define YYSTACK_BYTES(N) \
     ((N) * (sizeof (yytype_int16) + sizeof (YYSTYPE)) \
      + YYSTACK_GAP_MAXIMUM)

# define YYCOPY_NEEDED 1

/* Relocate STACK from its old location to the new one.  The
   local variables YYSIZE and YYSTACKSIZE give the old and new number of
   elements in the stack, and YYPTR gives the new location of the
   stack.  Advance YYPTR to a properly aligned location for the next
   stack.  */
# define YYSTACK_RELOCATE(Stack_alloc, Stack)                           \
    do                                                                  \
      {                                                                 \
        YYSIZE_T yynewbytes;                                            \
        YYCOPY (&yyptr->Stack_alloc, Stack, yysize);                    \
        Stack = &yyptr->Stack_alloc;                                    \
        yynewbytes = yystacksize * sizeof (*Stack) + YYSTACK_GAP_MAXIMUM; \
        yyptr += yynewbytes / sizeof (*yyptr);                          \
      }                                                                 \
    while (0)

#endif

#if defined YYCOPY_NEEDED && YYCOPY_NEEDED
/* Copy COUNT objects from SRC to DST.  The source and destination do
   not overlap.  */
# ifndef YYCOPY
#  if defined __GNUC__ && 1 < __GNUC__
#   define YYCOPY(Dst, Src, Count) \
      __builtin_memcpy (Dst, Src, (Count) * sizeof (*(Src)))
#  else
#   define YYCOPY(Dst, Src, Count)              \
      do                                        \
        {                                       \
          YYSIZE_T yyi;                         \
          for (yyi = 0; yyi < (Count); yyi++)   \
            (Dst)[yyi] = (Src)[yyi];            \
        }                                       \
      while (0)
#  endif
# endif
#endif /* !YYCOPY_NEEDED */

/* YYFINAL -- State number of the termination state.  */
#define YYFINAL  59
/* YYLAST -- Last index in YYTABLE.  */
#define YYLAST   699

/* YYNTOKENS -- Number of terminals.  */
#define YYNTOKENS  64
/* YYNNTS -- Number of nonterminals.  */
#define YYNNTS  17
/* YYNRULES -- Number of rules.  */
#define YYNRULES  106
/* YYNSTATES -- Number of states.  */
#define YYNSTATES  190

/* YYTRANSLATE[YYX] -- Symbol number corresponding to YYX as returned
   by yylex, with out-of-bounds checking.  */
#define YYUNDEFTOK  2
#define YYMAXUTOK   297

#define YYTRANSLATE(YYX)                                                \
  ((unsigned int) (YYX) <= YYMAXUTOK ? yytranslate[YYX] : YYUNDEFTOK)

/* YYTRANSLATE[TOKEN-NUM] -- Symbol number corresponding to TOKEN-NUM
   as returned by yylex, without out-of-bounds checking.  */
static const yytype_uint8 yytranslate[] =
{
       0,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,    62,     2,     2,     2,    54,    47,     2,
      55,    59,    52,    50,    58,    51,     2,    53,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,    43,    57,
      49,    44,    48,    42,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,    60,     2,    61,    46,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,    45,     2,    63,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     1,     2,     3,     4,
       5,     6,     7,     8,     9,    10,    11,    12,    13,    14,
      15,    16,    17,    18,    19,    20,    21,    22,    23,    24,
      25,    26,    27,    28,    29,    30,    31,    32,    33,    34,
      35,    36,    37,    38,    39,    40,    41,    56
};

#if YYDEBUG
  /* YYRLINE[YYN] -- Source line where rule number YYN was defined.  */
static const yytype_uint16 yyrline[] =
{
       0,   101,   101,   102,   106,   114,   118,   119,   123,   124,
     147,   148,   152,   156,   181,   188,   194,   200,   205,   214,
     258,   318,   354,   367,   391,   396,   401,   406,   411,   416,
     421,   426,   432,   437,   442,   447,   452,   457,   462,   467,
     472,   477,   483,   488,   494,   499,   505,   510,   515,   520,
     525,   530,   535,   540,   546,   551,   557,   557,   556,   568,
     573,   578,   583,   588,   606,   612,   618,   623,   630,   635,
     636,   640,   648,   669,   685,   691,   695,   703,   719,   724,
     728,   734,   742,   752,   762,   784,   797,   822,   839,   859,
     860,   861,   862,   863,   864,   865,   866,   867,   868,   869,
     870,   871,   872,   879,   915,   943,   971
};
#endif

#if YYDEBUG || YYERROR_VERBOSE || 1
/* YYTNAME[SYMBOL-NUM] -- String name of the symbol SYMBOL-NUM.
   First, the terminals, then, starting at YYNTOKENS, nonterminals.  */
static const char *const yytname[] =
{
  "$end", "error", "$undefined", "STRING", "NUMBER", "CHAR", "ANON_ID",
  "LIBDL_ID", "FUNC_ID", "VAR_ID", "AND", "RSHIFT", "LSHIFT", "KW_VOID",
  "KW_CHAR", "KW_UINT8", "KW_UINT16", "KW_UINT32", "KW_UINT64", "KW_LONG",
  "MULA", "DIVA", "MODA", "ADDA", "SUBA", "SHLA", "SHRA", "ANDA", "ORA",
  "XORA", "OR", "GE", "LE", "EQ", "NE", "AI", "AD", "KW_SIZEOF", "DOTDOT",
  "ITER_COND", "ITER_START", "ITER_END", "'?'", "':'", "'='", "'|'", "'^'",
  "'&'", "'>'", "'<'", "'+'", "'-'", "'*'", "'/'", "'%'", "'('", "UNARY",
  "';'", "','", "')'", "'['", "']'", "'!'", "'~'", "$accept", "Statements",
  "Statement", "ExprList", "Expr", "ExpOrType", "SimpleExpr", "$@1", "$@2",
  "IteratorList", "Parameters", "ParameterList", "ProtoArgs",
  "ProtoArgList", "Declaration", "Typedecl", "Assignment", YY_NULLPTR
};
#endif

# ifdef YYPRINT
/* YYTOKNUM[NUM] -- (External) token number corresponding to the
   (internal) symbol number NUM (which must be that of a token).  */
static const yytype_uint16 yytoknum[] =
{
       0,   256,   257,   258,   259,   260,   261,   262,   263,   264,
     265,   266,   267,   268,   269,   270,   271,   272,   273,   274,
     275,   276,   277,   278,   279,   280,   281,   282,   283,   284,
     285,   286,   287,   288,   289,   290,   291,   292,   293,   294,
     295,   296,    63,    58,    61,   124,    94,    38,    62,    60,
      43,    45,    42,    47,    37,    40,   297,    59,    44,    41,
      91,    93,    33,   126
};
# endif

#define YYPACT_NINF -143

#define yypact_value_is_default(Yystate) \
  (!!((Yystate) == (-143)))

#define YYTABLE_NINF -1

#define yytable_value_is_error(Yytable_value) \
  0

  /* YYPACT[STATE-NUM] -- Index in YYTABLE of the portion describing
     STATE-NUM.  */
static const yytype_int16 yypact[] =
{
     198,  -143,  -143,  -143,   -40,   -38,   -48,  -143,   -23,    -8,
      -7,    -5,    -4,    -3,  -143,   198,   198,   -32,   198,   198,
     198,   198,   198,   198,   198,   198,     1,  -143,    22,   290,
    -143,  -143,   173,  -143,   198,   198,   198,   198,  -143,  -143,
    -143,  -143,  -143,  -143,    -9,    -9,   198,   290,   -29,    -2,
      -9,    -9,    -9,    -9,   290,   -16,    46,    -9,    -9,  -143,
     198,   198,   198,   198,   198,   198,   198,   198,   198,   198,
     198,   198,   198,   198,   198,   198,   198,   198,   198,   198,
    -143,  -143,  -143,   198,   198,   198,   198,   198,   198,   198,
     198,   198,   198,   198,   198,   198,    26,    44,    45,  -143,
    -143,    41,   336,   336,   290,    43,    52,    54,   290,    57,
     173,   198,   198,  -143,   198,   198,  -143,   198,  -143,   290,
     412,   639,    71,   336,   336,   336,   336,   336,   336,   336,
     336,   336,   336,   382,   581,   607,   529,   555,   198,   336,
     442,   472,   502,   628,   628,   -22,   -22,    73,    73,    73,
      59,   244,   151,   151,   151,    60,  -143,   198,  -143,  -143,
      62,    66,    -1,   290,   290,    -9,   290,  -143,  -143,    61,
      53,    75,    70,    78,  -143,   290,  -143,   137,  -143,    87,
    -143,   151,  -143,  -143,  -143,    91,   198,    75,  -143,   336
};

  /* YYDEFACT[STATE-NUM] -- Default reduction number in state STATE-NUM.
     Performed when YYTABLE does not specify something else to do.  Zero
     means the default is an error.  */
static const yytype_uint8 yydefact[] =
{
       5,    22,    14,    15,   106,     0,     9,    23,    92,    89,
      90,    91,    93,    94,    95,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     2,     4,     6,
       8,    69,     0,    70,     0,     0,    74,    74,    96,    97,
      98,    99,   100,   101,    44,    45,     0,    71,     8,     0,
      20,    17,    16,    19,     0,     8,     0,    59,    60,     1,
       5,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
      42,    43,    56,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,    74,     0,    82,    83,     0,    84,
     102,     0,   104,   105,    76,     0,    75,     0,    10,     0,
      11,     0,     0,    68,     0,     0,    12,     0,     3,     7,
      46,    54,    55,    34,    35,    39,    32,    33,    40,    41,
      37,    36,    38,    47,    52,    53,    48,    49,     0,   103,
      28,    30,    29,    50,    51,    26,    27,    24,    25,    31,
       0,     0,    78,    78,    78,     0,    63,     0,    62,    13,
       8,     8,     0,    73,    72,    18,    57,    61,    21,     0,
      79,    80,     0,     0,    85,    77,    66,     0,    67,     0,
      87,     0,    88,    86,    64,     8,     0,    81,    65,    58
};

  /* YYPGOTO[NTERM-NUM].  */
static const yytype_int16 yypgoto[] =
{
    -143,  -143,    79,  -143,     0,  -143,   -15,  -143,  -143,    35,
     -35,  -143,  -142,  -143,  -143,   -18,  -143
};

  /* YYDEFGOTO[NTERM-NUM].  */
static const yytype_int16 yydefgoto[] =
{
      -1,    26,    27,    28,    54,   109,    30,   138,   179,    49,
     105,   106,   169,   170,    31,    32,    33
};

  /* YYTABLE[YYPACT[STATE-NUM]] -- What to do in state STATE-NUM.  If
     positive, shift that token.  If negative, reduce the rule whose
     number is the opposite.  If YYTABLE_NINF, syntax error.  */
static const yytype_uint8 yytable[] =
{
      29,    59,   107,    48,    34,    56,    35,    37,    55,   111,
     112,   172,   173,    80,    81,    44,    45,    36,    47,    50,
      51,    52,    53,    46,    57,    58,    80,    81,   110,    38,
      91,    92,    93,    94,   102,   103,   104,   104,    95,   113,
     178,   114,   114,   116,    39,    40,   108,    41,    42,    43,
     155,    95,    96,    97,    98,    99,   115,   115,    60,   150,
      29,   119,   120,   121,   122,   123,   124,   125,   126,   127,
     128,   129,   130,   131,   132,   133,   134,   135,   136,   137,
      61,   152,    63,   139,   140,   141,   142,   143,   144,   145,
     146,   147,   148,   149,   104,   151,   160,   161,   100,   153,
     154,   101,   156,   176,   177,   117,    80,    81,    80,    81,
     157,   181,    47,   158,   163,   164,   159,   165,   167,   174,
     180,    89,    90,    91,    92,    93,    94,   100,    94,   182,
     186,    95,   188,    95,   171,   171,   171,   183,   166,   118,
       1,     2,     3,     4,     5,     6,     7,   162,     0,     0,
       8,     9,    10,    11,    12,    13,    14,   175,     0,     0,
       0,     0,   185,   187,     8,     9,    10,    11,    12,    13,
      14,     0,    15,    16,    17,     0,     0,    18,   184,    96,
      97,    98,    99,     0,    19,     0,   189,    20,    21,    22,
       0,     0,    23,     0,     0,     0,     0,     0,     0,    24,
      25,     1,     2,     3,     4,     5,     6,     7,     0,     0,
       0,     8,     9,    10,    11,    12,    13,    14,     0,     0,
       0,     0,     0,     0,     0,   100,     0,     0,   101,     0,
       0,     0,     0,    15,    16,    17,     0,     0,    18,     0,
       0,     0,     0,     0,     0,    19,     0,     0,    20,    21,
      22,     0,     0,    23,    62,    63,    64,     0,     0,     0,
      24,    25,     0,     0,    65,    66,    67,    68,    69,    70,
      71,    72,    73,    74,    75,    76,    77,    78,    79,    80,
      81,     0,     0,     0,     0,     0,    82,     0,    83,    84,
      85,    86,    87,    88,    89,    90,    91,    92,    93,    94,
      62,    63,    64,     0,    95,   168,     0,     0,     0,     0,
      65,    66,    67,    68,    69,    70,    71,    72,    73,    74,
      75,    76,    77,    78,    79,    80,    81,     0,     0,     0,
       0,     0,    82,     0,    83,    84,    85,    86,    87,    88,
      89,    90,    91,    92,    93,    94,    62,    63,    64,     0,
      95,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,    75,    76,    77,    78,
      79,    80,    81,     0,     0,     0,     0,     0,    82,     0,
      83,    84,    85,    86,    87,    88,    89,    90,    91,    92,
      93,    94,    62,    63,    64,     0,    95,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,    76,    77,    78,    79,    80,    81,     0,
       0,     0,     0,    63,    64,     0,     0,    84,    85,    86,
      87,    88,    89,    90,    91,    92,    93,    94,     0,     0,
       0,     0,    95,    76,    77,    78,    79,    80,    81,     0,
       0,     0,     0,    63,    64,     0,     0,    84,    85,    86,
      87,    88,    89,    90,    91,    92,    93,    94,     0,     0,
       0,     0,    95,    76,    77,    78,    79,    80,    81,     0,
       0,     0,     0,    63,    64,     0,     0,     0,    85,    86,
      87,    88,    89,    90,    91,    92,    93,    94,     0,     0,
       0,     0,    95,    76,    77,    78,    79,    80,    81,     0,
       0,     0,     0,    63,    64,     0,     0,     0,     0,    86,
      87,    88,    89,    90,    91,    92,    93,    94,     0,     0,
       0,     0,    95,    76,    77,    78,    79,    80,    81,     0,
      63,    64,     0,     0,     0,     0,     0,     0,     0,     0,
      87,    88,    89,    90,    91,    92,    93,    94,     0,     0,
      76,    77,    95,    79,    80,    81,    63,    64,     0,     0,
       0,     0,     0,     0,     0,     0,     0,    87,    88,    89,
      90,    91,    92,    93,    94,     0,    76,    77,     0,    95,
      80,    81,    63,    64,     0,     0,     0,     0,     0,     0,
       0,     0,     0,    87,    88,    89,    90,    91,    92,    93,
      94,     0,     0,    77,     0,    95,    80,    81,    63,    64,
       0,     0,     0,     0,     0,     0,     0,     0,     0,    87,
      88,    89,    90,    91,    92,    93,    94,     0,     0,    63,
      64,    95,    80,    81,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,    87,    88,    89,    90,    91,
      92,    93,    94,    80,    81,     0,     0,    95,     0,     0,
       0,     0,     0,     0,    80,    81,     0,     0,    89,    90,
      91,    92,    93,    94,     0,     0,     0,     0,    95,    89,
      90,    91,    92,    93,    94,     0,     0,     0,     0,    95
};

static const yytype_int16 yycheck[] =
{
       0,     0,    37,    18,    44,    23,    44,    55,    23,    38,
      39,   153,   154,    35,    36,    15,    16,    55,    18,    19,
      20,    21,    22,    55,    24,    25,    35,    36,    46,    52,
      52,    53,    54,    55,    34,    35,    36,    37,    60,    41,
      41,    43,    43,    59,    52,    52,    46,    52,    52,    52,
       9,    60,     6,     7,     8,     9,    58,    58,    57,    94,
      60,    61,    62,    63,    64,    65,    66,    67,    68,    69,
      70,    71,    72,    73,    74,    75,    76,    77,    78,    79,
      58,    55,    11,    83,    84,    85,    86,    87,    88,    89,
      90,    91,    92,    93,    94,    95,   111,   112,    52,    55,
      55,    55,    59,    41,    38,    59,    35,    36,    35,    36,
      58,    58,   112,    59,   114,   115,    59,   117,    59,    59,
      59,    50,    51,    52,    53,    54,    55,    52,    55,    59,
      43,    60,    41,    60,   152,   153,   154,    59,   138,    60,
       3,     4,     5,     6,     7,     8,     9,   112,    -1,    -1,
      13,    14,    15,    16,    17,    18,    19,   157,    -1,    -1,
      -1,    -1,   177,   181,    13,    14,    15,    16,    17,    18,
      19,    -1,    35,    36,    37,    -1,    -1,    40,    41,     6,
       7,     8,     9,    -1,    47,    -1,   186,    50,    51,    52,
      -1,    -1,    55,    -1,    -1,    -1,    -1,    -1,    -1,    62,
      63,     3,     4,     5,     6,     7,     8,     9,    -1,    -1,
      -1,    13,    14,    15,    16,    17,    18,    19,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    52,    -1,    -1,    55,    -1,
      -1,    -1,    -1,    35,    36,    37,    -1,    -1,    40,    -1,
      -1,    -1,    -1,    -1,    -1,    47,    -1,    -1,    50,    51,
      52,    -1,    -1,    55,    10,    11,    12,    -1,    -1,    -1,
      62,    63,    -1,    -1,    20,    21,    22,    23,    24,    25,
      26,    27,    28,    29,    30,    31,    32,    33,    34,    35,
      36,    -1,    -1,    -1,    -1,    -1,    42,    -1,    44,    45,
      46,    47,    48,    49,    50,    51,    52,    53,    54,    55,
      10,    11,    12,    -1,    60,    61,    -1,    -1,    -1,    -1,
      20,    21,    22,    23,    24,    25,    26,    27,    28,    29,
      30,    31,    32,    33,    34,    35,    36,    -1,    -1,    -1,
      -1,    -1,    42,    -1,    44,    45,    46,    47,    48,    49,
      50,    51,    52,    53,    54,    55,    10,    11,    12,    -1,
      60,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    30,    31,    32,    33,
      34,    35,    36,    -1,    -1,    -1,    -1,    -1,    42,    -1,
      44,    45,    46,    47,    48,    49,    50,    51,    52,    53,
      54,    55,    10,    11,    12,    -1,    60,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    31,    32,    33,    34,    35,    36,    -1,
      -1,    -1,    -1,    11,    12,    -1,    -1,    45,    46,    47,
      48,    49,    50,    51,    52,    53,    54,    55,    -1,    -1,
      -1,    -1,    60,    31,    32,    33,    34,    35,    36,    -1,
      -1,    -1,    -1,    11,    12,    -1,    -1,    45,    46,    47,
      48,    49,    50,    51,    52,    53,    54,    55,    -1,    -1,
      -1,    -1,    60,    31,    32,    33,    34,    35,    36,    -1,
      -1,    -1,    -1,    11,    12,    -1,    -1,    -1,    46,    47,
      48,    49,    50,    51,    52,    53,    54,    55,    -1,    -1,
      -1,    -1,    60,    31,    32,    33,    34,    35,    36,    -1,
      -1,    -1,    -1,    11,    12,    -1,    -1,    -1,    -1,    47,
      48,    49,    50,    51,    52,    53,    54,    55,    -1,    -1,
      -1,    -1,    60,    31,    32,    33,    34,    35,    36,    -1,
      11,    12,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      48,    49,    50,    51,    52,    53,    54,    55,    -1,    -1,
      31,    32,    60,    34,    35,    36,    11,    12,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    48,    49,    50,
      51,    52,    53,    54,    55,    -1,    31,    32,    -1,    60,
      35,    36,    11,    12,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    48,    49,    50,    51,    52,    53,    54,
      55,    -1,    -1,    32,    -1,    60,    35,    36,    11,    12,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    48,
      49,    50,    51,    52,    53,    54,    55,    -1,    -1,    11,
      12,    60,    35,    36,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    48,    49,    50,    51,    52,
      53,    54,    55,    35,    36,    -1,    -1,    60,    -1,    -1,
      -1,    -1,    -1,    -1,    35,    36,    -1,    -1,    50,    51,
      52,    53,    54,    55,    -1,    -1,    -1,    -1,    60,    50,
      51,    52,    53,    54,    55,    -1,    -1,    -1,    -1,    60
};

  /* YYSTOS[STATE-NUM] -- The (internal number of the) accessing
     symbol of state STATE-NUM.  */
static const yytype_uint8 yystos[] =
{
       0,     3,     4,     5,     6,     7,     8,     9,    13,    14,
      15,    16,    17,    18,    19,    35,    36,    37,    40,    47,
      50,    51,    52,    55,    62,    63,    65,    66,    67,    68,
      70,    78,    79,    80,    44,    44,    55,    55,    52,    52,
      52,    52,    52,    52,    68,    68,    55,    68,    70,    73,
      68,    68,    68,    68,    68,    70,    79,    68,    68,     0,
      57,    58,    10,    11,    12,    20,    21,    22,    23,    24,
      25,    26,    27,    28,    29,    30,    31,    32,    33,    34,
      35,    36,    42,    44,    45,    46,    47,    48,    49,    50,
      51,    52,    53,    54,    55,    60,     6,     7,     8,     9,
      52,    55,    68,    68,    68,    74,    75,    74,    68,    69,
      79,    38,    39,    41,    43,    58,    59,    59,    66,    68,
      68,    68,    68,    68,    68,    68,    68,    68,    68,    68,
      68,    68,    68,    68,    68,    68,    68,    68,    71,    68,
      68,    68,    68,    68,    68,    68,    68,    68,    68,    68,
      74,    68,    55,    55,    55,     9,    59,    58,    59,    59,
      70,    70,    73,    68,    68,    68,    68,    59,    61,    76,
      77,    79,    76,    76,    59,    68,    41,    38,    41,    72,
      59,    58,    59,    59,    41,    70,    43,    79,    41,    68
};

  /* YYR1[YYN] -- Symbol number of symbol that rule YYN derives.  */
static const yytype_uint8 yyr1[] =
{
       0,    64,    65,    65,    66,    66,    67,    67,    68,    68,
      69,    69,    70,    70,    70,    70,    70,    70,    70,    70,
      70,    70,    70,    70,    70,    70,    70,    70,    70,    70,
      70,    70,    70,    70,    70,    70,    70,    70,    70,    70,
      70,    70,    70,    70,    70,    70,    70,    70,    70,    70,
      70,    70,    70,    70,    70,    70,    71,    72,    70,    70,
      70,    70,    70,    70,    70,    70,    70,    70,    70,    70,
      70,    73,    73,    73,    74,    74,    75,    75,    76,    76,
      77,    77,    78,    78,    78,    78,    78,    78,    78,    79,
      79,    79,    79,    79,    79,    79,    79,    79,    79,    79,
      79,    79,    79,    80,    80,    80,    80
};

  /* YYR2[YYN] -- Number of symbols on the right hand side of rule YYN.  */
static const yytype_uint8 yyr2[] =
{
       0,     2,     1,     3,     1,     0,     1,     3,     1,     1,
       1,     1,     3,     4,     1,     1,     2,     2,     4,     2,
       2,     4,     1,     1,     3,     3,     3,     3,     3,     3,
       3,     3,     3,     3,     3,     3,     3,     3,     3,     3,
       3,     3,     2,     2,     2,     2,     3,     3,     3,     3,
       3,     3,     3,     3,     3,     3,     0,     0,     7,     2,
       2,     4,     4,     4,     6,     7,     5,     5,     3,     1,
       1,     1,     3,     3,     0,     1,     1,     3,     0,     1,
       1,     3,     2,     2,     2,     4,     5,     5,     5,     1,
       1,     1,     1,     1,     1,     1,     2,     2,     2,     2,
       2,     2,     2,     3,     3,     3,     1
};


#define yyerrok         (yyerrstatus = 0)
#define yyclearin       (yychar = YYEMPTY)
#define YYEMPTY         (-2)
#define YYEOF           0

#define YYACCEPT        goto yyacceptlab
#define YYABORT         goto yyabortlab
#define YYERROR         goto yyerrorlab


#define YYRECOVERING()  (!!yyerrstatus)

#define YYBACKUP(Token, Value)                                  \
do                                                              \
  if (yychar == YYEMPTY)                                        \
    {                                                           \
      yychar = (Token);                                         \
      yylval = (Value);                                         \
      YYPOPSTACK (yylen);                                       \
      yystate = *yyssp;                                         \
      goto yybackup;                                            \
    }                                                           \
  else                                                          \
    {                                                           \
      yyerror (scan_ctx, YY_("syntax error: cannot back up")); \
      YYERROR;                                                  \
    }                                                           \
while (0)

/* Error token number */
#define YYTERROR        1
#define YYERRCODE       256



/* Enable debugging if requested.  */
#if YYDEBUG

# ifndef YYFPRINTF
#  include <stdio.h> /* INFRINGES ON USER NAME SPACE */
#  define YYFPRINTF fprintf
# endif

# define YYDPRINTF(Args)                        \
do {                                            \
  if (yydebug)                                  \
    YYFPRINTF Args;                             \
} while (0)

/* This macro is provided for backward compatibility. */
#ifndef YY_LOCATION_PRINT
# define YY_LOCATION_PRINT(File, Loc) ((void) 0)
#endif


# define YY_SYMBOL_PRINT(Title, Type, Value, Location)                    \
do {                                                                      \
  if (yydebug)                                                            \
    {                                                                     \
      YYFPRINTF (stderr, "%s ", Title);                                   \
      yy_symbol_print (stderr,                                            \
                  Type, Value, scan_ctx); \
      YYFPRINTF (stderr, "\n");                                           \
    }                                                                     \
} while (0)


/*----------------------------------------.
| Print this symbol's value on YYOUTPUT.  |
`----------------------------------------*/

static void
yy_symbol_value_print (FILE *yyoutput, int yytype, YYSTYPE const * const yyvaluep, scan_ctx_t *scan_ctx)
{
  FILE *yyo = yyoutput;
  YYUSE (yyo);
  YYUSE (scan_ctx);
  if (!yyvaluep)
    return;
# ifdef YYPRINT
  if (yytype < YYNTOKENS)
    YYPRINT (yyoutput, yytoknum[yytype], *yyvaluep);
# endif
  YYUSE (yytype);
}


/*--------------------------------.
| Print this symbol on YYOUTPUT.  |
`--------------------------------*/

static void
yy_symbol_print (FILE *yyoutput, int yytype, YYSTYPE const * const yyvaluep, scan_ctx_t *scan_ctx)
{
  YYFPRINTF (yyoutput, "%s %s (",
             yytype < YYNTOKENS ? "token" : "nterm", yytname[yytype]);

  yy_symbol_value_print (yyoutput, yytype, yyvaluep, scan_ctx);
  YYFPRINTF (yyoutput, ")");
}

/*------------------------------------------------------------------.
| yy_stack_print -- Print the state stack from its BOTTOM up to its |
| TOP (included).                                                   |
`------------------------------------------------------------------*/

static void
yy_stack_print (yytype_int16 *yybottom, yytype_int16 *yytop)
{
  YYFPRINTF (stderr, "Stack now");
  for (; yybottom <= yytop; yybottom++)
    {
      int yybot = *yybottom;
      YYFPRINTF (stderr, " %d", yybot);
    }
  YYFPRINTF (stderr, "\n");
}

# define YY_STACK_PRINT(Bottom, Top)                            \
do {                                                            \
  if (yydebug)                                                  \
    yy_stack_print ((Bottom), (Top));                           \
} while (0)


/*------------------------------------------------.
| Report that the YYRULE is going to be reduced.  |
`------------------------------------------------*/

static void
yy_reduce_print (yytype_int16 *yyssp, YYSTYPE *yyvsp, int yyrule, scan_ctx_t *scan_ctx)
{
  unsigned long int yylno = yyrline[yyrule];
  int yynrhs = yyr2[yyrule];
  int yyi;
  YYFPRINTF (stderr, "Reducing stack by rule %d (line %lu):\n",
             yyrule - 1, yylno);
  /* The symbols being reduced.  */
  for (yyi = 0; yyi < yynrhs; yyi++)
    {
      YYFPRINTF (stderr, "   $%d = ", yyi + 1);
      yy_symbol_print (stderr,
                       yystos[yyssp[yyi + 1 - yynrhs]],
                       &(yyvsp[(yyi + 1) - (yynrhs)])
                                              , scan_ctx);
      YYFPRINTF (stderr, "\n");
    }
}

# define YY_REDUCE_PRINT(Rule)          \
do {                                    \
  if (yydebug)                          \
    yy_reduce_print (yyssp, yyvsp, Rule, scan_ctx); \
} while (0)

/* Nonzero means print parse trace.  It is left uninitialized so that
   multiple parsers can coexist.  */
int yydebug;
#else /* !YYDEBUG */
# define YYDPRINTF(Args)
# define YY_SYMBOL_PRINT(Title, Type, Value, Location)
# define YY_STACK_PRINT(Bottom, Top)
# define YY_REDUCE_PRINT(Rule)
#endif /* !YYDEBUG */


/* YYINITDEPTH -- initial size of the parser's stacks.  */
#ifndef YYINITDEPTH
# define YYINITDEPTH 200
#endif

/* YYMAXDEPTH -- maximum size the stacks can grow to (effective only
   if the built-in stack extension method is used).

   Do not make this value too large; the results are undefined if
   YYSTACK_ALLOC_MAXIMUM < YYSTACK_BYTES (YYMAXDEPTH)
   evaluated with infinite-precision integer arithmetic.  */

#ifndef YYMAXDEPTH
# define YYMAXDEPTH 10000
#endif


#if YYERROR_VERBOSE

# ifndef yystrlen
#  if defined __GLIBC__ && defined _STRING_H
#   define yystrlen strlen
#  else
/* Return the length of YYSTR.  */
static YYSIZE_T
yystrlen (const char *yystr)
{
  YYSIZE_T yylen;
  for (yylen = 0; yystr[yylen]; yylen++)
    continue;
  return yylen;
}
#  endif
# endif

# ifndef yystpcpy
#  if defined __GLIBC__ && defined _STRING_H && defined _GNU_SOURCE
#   define yystpcpy stpcpy
#  else
/* Copy YYSRC to YYDEST, returning the address of the terminating '\0' in
   YYDEST.  */
static char *
yystpcpy (char *yydest, const char *yysrc)
{
  char *yyd = yydest;
  const char *yys = yysrc;

  while ((*yyd++ = *yys++) != '\0')
    continue;

  return yyd - 1;
}
#  endif
# endif

# ifndef yytnamerr
/* Copy to YYRES the contents of YYSTR after stripping away unnecessary
   quotes and backslashes, so that it's suitable for yyerror.  The
   heuristic is that double-quoting is unnecessary unless the string
   contains an apostrophe, a comma, or backslash (other than
   backslash-backslash).  YYSTR is taken from yytname.  If YYRES is
   null, do not copy; instead, return the length of what the result
   would have been.  */
static YYSIZE_T
yytnamerr (char *yyres, const char *yystr)
{
  if (*yystr == '"')
    {
      YYSIZE_T yyn = 0;
      char const *yyp = yystr;

      for (;;)
        switch (*++yyp)
          {
          case '\'':
          case ',':
            goto do_not_strip_quotes;

          case '\\':
            if (*++yyp != '\\')
              goto do_not_strip_quotes;
            /* Fall through.  */
          default:
            if (yyres)
              yyres[yyn] = *yyp;
            yyn++;
            break;

          case '"':
            if (yyres)
              yyres[yyn] = '\0';
            return yyn;
          }
    do_not_strip_quotes: ;
    }

  if (! yyres)
    return yystrlen (yystr);

  return yystpcpy (yyres, yystr) - yyres;
}
# endif

/* Copy into *YYMSG, which is of size *YYMSG_ALLOC, an error message
   about the unexpected token YYTOKEN for the state stack whose top is
   YYSSP.

   Return 0 if *YYMSG was successfully written.  Return 1 if *YYMSG is
   not large enough to hold the message.  In that case, also set
   *YYMSG_ALLOC to the required number of bytes.  Return 2 if the
   required number of bytes is too large to store.  */
static int
yysyntax_error (YYSIZE_T *yymsg_alloc, char **yymsg,
                yytype_int16 *yyssp, int yytoken)
{
  YYSIZE_T yysize0 = yytnamerr (YY_NULLPTR, yytname[yytoken]);
  YYSIZE_T yysize = yysize0;
  enum { YYERROR_VERBOSE_ARGS_MAXIMUM = 5 };
  /* Internationalized format string. */
  const char *yyformat = YY_NULLPTR;
  /* Arguments of yyformat. */
  char const *yyarg[YYERROR_VERBOSE_ARGS_MAXIMUM];
  /* Number of reported tokens (one for the "unexpected", one per
     "expected"). */
  int yycount = 0;

  /* There are many possibilities here to consider:
     - If this state is a consistent state with a default action, then
       the only way this function was invoked is if the default action
       is an error action.  In that case, don't check for expected
       tokens because there are none.
     - The only way there can be no lookahead present (in yychar) is if
       this state is a consistent state with a default action.  Thus,
       detecting the absence of a lookahead is sufficient to determine
       that there is no unexpected or expected token to report.  In that
       case, just report a simple "syntax error".
     - Don't assume there isn't a lookahead just because this state is a
       consistent state with a default action.  There might have been a
       previous inconsistent state, consistent state with a non-default
       action, or user semantic action that manipulated yychar.
     - Of course, the expected token list depends on states to have
       correct lookahead information, and it depends on the parser not
       to perform extra reductions after fetching a lookahead from the
       scanner and before detecting a syntax error.  Thus, state merging
       (from LALR or IELR) and default reductions corrupt the expected
       token list.  However, the list is correct for canonical LR with
       one exception: it will still contain any token that will not be
       accepted due to an error action in a later state.
  */
  if (yytoken != YYEMPTY)
    {
      int yyn = yypact[*yyssp];
      yyarg[yycount++] = yytname[yytoken];
      if (!yypact_value_is_default (yyn))
        {
          /* Start YYX at -YYN if negative to avoid negative indexes in
             YYCHECK.  In other words, skip the first -YYN actions for
             this state because they are default actions.  */
          int yyxbegin = yyn < 0 ? -yyn : 0;
          /* Stay within bounds of both yycheck and yytname.  */
          int yychecklim = YYLAST - yyn + 1;
          int yyxend = yychecklim < YYNTOKENS ? yychecklim : YYNTOKENS;
          int yyx;

          for (yyx = yyxbegin; yyx < yyxend; ++yyx)
            if (yycheck[yyx + yyn] == yyx && yyx != YYTERROR
                && !yytable_value_is_error (yytable[yyx + yyn]))
              {
                if (yycount == YYERROR_VERBOSE_ARGS_MAXIMUM)
                  {
                    yycount = 1;
                    yysize = yysize0;
                    break;
                  }
                yyarg[yycount++] = yytname[yyx];
                {
                  YYSIZE_T yysize1 = yysize + yytnamerr (YY_NULLPTR, yytname[yyx]);
                  if (! (yysize <= yysize1
                         && yysize1 <= YYSTACK_ALLOC_MAXIMUM))
                    return 2;
                  yysize = yysize1;
                }
              }
        }
    }

  switch (yycount)
    {
# define YYCASE_(N, S)                      \
      case N:                               \
        yyformat = S;                       \
      break
      YYCASE_(0, YY_("syntax error"));
      YYCASE_(1, YY_("syntax error, unexpected %s"));
      YYCASE_(2, YY_("syntax error, unexpected %s, expecting %s"));
      YYCASE_(3, YY_("syntax error, unexpected %s, expecting %s or %s"));
      YYCASE_(4, YY_("syntax error, unexpected %s, expecting %s or %s or %s"));
      YYCASE_(5, YY_("syntax error, unexpected %s, expecting %s or %s or %s or %s"));
# undef YYCASE_
    }

  {
    YYSIZE_T yysize1 = yysize + yystrlen (yyformat);
    if (! (yysize <= yysize1 && yysize1 <= YYSTACK_ALLOC_MAXIMUM))
      return 2;
    yysize = yysize1;
  }

  if (*yymsg_alloc < yysize)
    {
      *yymsg_alloc = 2 * yysize;
      if (! (yysize <= *yymsg_alloc
             && *yymsg_alloc <= YYSTACK_ALLOC_MAXIMUM))
        *yymsg_alloc = YYSTACK_ALLOC_MAXIMUM;
      return 1;
    }

  /* Avoid sprintf, as that infringes on the user's name space.
     Don't have undefined behavior even if the translation
     produced a string with the wrong number of "%s"s.  */
  {
    char *yyp = *yymsg;
    int yyi = 0;
    while ((*yyp = *yyformat) != '\0')
      if (*yyp == '%' && yyformat[1] == 's' && yyi < yycount)
        {
          yyp += yytnamerr (yyp, yyarg[yyi++]);
          yyformat += 2;
        }
      else
        {
          yyp++;
          yyformat++;
        }
  }
  return 0;
}
#endif /* YYERROR_VERBOSE */

/*-----------------------------------------------.
| Release the memory associated to this symbol.  |
`-----------------------------------------------*/

static void
yydestruct (const char *yymsg, int yytype, YYSTYPE *yyvaluep, scan_ctx_t *scan_ctx)
{
  YYUSE (yyvaluep);
  YYUSE (scan_ctx);
  if (!yymsg)
    yymsg = "Deleting";
  YY_SYMBOL_PRINT (yymsg, yytype, yyvaluep, yylocationp);

  YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN
  YYUSE (yytype);
  YY_IGNORE_MAYBE_UNINITIALIZED_END
}




/*----------.
| yyparse.  |
`----------*/

int
yyparse (scan_ctx_t *scan_ctx)
{
/* The lookahead symbol.  */
int yychar;


/* The semantic value of the lookahead symbol.  */
/* Default value used for initialization, for pacifying older GCCs
   or non-GCC compilers.  */
YY_INITIAL_VALUE (static YYSTYPE yyval_default;)
YYSTYPE yylval YY_INITIAL_VALUE (= yyval_default);

    /* Number of syntax errors so far.  */
    int yynerrs;

    int yystate;
    /* Number of tokens to shift before error messages enabled.  */
    int yyerrstatus;

    /* The stacks and their tools:
       'yyss': related to states.
       'yyvs': related to semantic values.

       Refer to the stacks through separate pointers, to allow yyoverflow
       to reallocate them elsewhere.  */

    /* The state stack.  */
    yytype_int16 yyssa[YYINITDEPTH];
    yytype_int16 *yyss;
    yytype_int16 *yyssp;

    /* The semantic value stack.  */
    YYSTYPE yyvsa[YYINITDEPTH];
    YYSTYPE *yyvs;
    YYSTYPE *yyvsp;

    YYSIZE_T yystacksize;

  int yyn;
  int yyresult;
  /* Lookahead token as an internal (translated) token number.  */
  int yytoken = 0;
  /* The variables used to return semantic value and location from the
     action routines.  */
  YYSTYPE yyval;

#if YYERROR_VERBOSE
  /* Buffer for error messages, and its allocated size.  */
  char yymsgbuf[128];
  char *yymsg = yymsgbuf;
  YYSIZE_T yymsg_alloc = sizeof yymsgbuf;
#endif

#define YYPOPSTACK(N)   (yyvsp -= (N), yyssp -= (N))

  /* The number of symbols on the RHS of the reduced rule.
     Keep to zero when no symbol should be popped.  */
  int yylen = 0;

  yyssp = yyss = yyssa;
  yyvsp = yyvs = yyvsa;
  yystacksize = YYINITDEPTH;

  YYDPRINTF ((stderr, "Starting parse\n"));

  yystate = 0;
  yyerrstatus = 0;
  yynerrs = 0;
  yychar = YYEMPTY; /* Cause a token to be read.  */
  goto yysetstate;

/*------------------------------------------------------------.
| yynewstate -- Push a new state, which is found in yystate.  |
`------------------------------------------------------------*/
 yynewstate:
  /* In all cases, when you get here, the value and location stacks
     have just been pushed.  So pushing a state here evens the stacks.  */
  yyssp++;

 yysetstate:
  *yyssp = yystate;

  if (yyss + yystacksize - 1 <= yyssp)
    {
      /* Get the current used size of the three stacks, in elements.  */
      YYSIZE_T yysize = yyssp - yyss + 1;

#ifdef yyoverflow
      {
        /* Give user a chance to reallocate the stack.  Use copies of
           these so that the &'s don't force the real ones into
           memory.  */
        YYSTYPE *yyvs1 = yyvs;
        yytype_int16 *yyss1 = yyss;

        /* Each stack pointer address is followed by the size of the
           data in use in that stack, in bytes.  This used to be a
           conditional around just the two extra args, but that might
           be undefined if yyoverflow is a macro.  */
        yyoverflow (YY_("memory exhausted"),
                    &yyss1, yysize * sizeof (*yyssp),
                    &yyvs1, yysize * sizeof (*yyvsp),
                    &yystacksize);

        yyss = yyss1;
        yyvs = yyvs1;
      }
#else /* no yyoverflow */
# ifndef YYSTACK_RELOCATE
      goto yyexhaustedlab;
# else
      /* Extend the stack our own way.  */
      if (YYMAXDEPTH <= yystacksize)
        goto yyexhaustedlab;
      yystacksize *= 2;
      if (YYMAXDEPTH < yystacksize)
        yystacksize = YYMAXDEPTH;

      {
        yytype_int16 *yyss1 = yyss;
        union yyalloc *yyptr =
          (union yyalloc *) YYSTACK_ALLOC (YYSTACK_BYTES (yystacksize));
        if (! yyptr)
          goto yyexhaustedlab;
        YYSTACK_RELOCATE (yyss_alloc, yyss);
        YYSTACK_RELOCATE (yyvs_alloc, yyvs);
#  undef YYSTACK_RELOCATE
        if (yyss1 != yyssa)
          YYSTACK_FREE (yyss1);
      }
# endif
#endif /* no yyoverflow */

      yyssp = yyss + yysize - 1;
      yyvsp = yyvs + yysize - 1;

      YYDPRINTF ((stderr, "Stack size increased to %lu\n",
                  (unsigned long int) yystacksize));

      if (yyss + yystacksize - 1 <= yyssp)
        YYABORT;
    }

  YYDPRINTF ((stderr, "Entering state %d\n", yystate));

  if (yystate == YYFINAL)
    YYACCEPT;

  goto yybackup;

/*-----------.
| yybackup.  |
`-----------*/
yybackup:

  /* Do appropriate processing given the current state.  Read a
     lookahead token if we need one and don't already have one.  */

  /* First try to decide what to do without reference to lookahead token.  */
  yyn = yypact[yystate];
  if (yypact_value_is_default (yyn))
    goto yydefault;

  /* Not known => get a lookahead token if don't already have one.  */

  /* YYCHAR is either YYEMPTY or YYEOF or a valid lookahead symbol.  */
  if (yychar == YYEMPTY)
    {
      YYDPRINTF ((stderr, "Reading a token: "));
      yychar = yylex (&yylval, scan_ctx);
    }

  if (yychar <= YYEOF)
    {
      yychar = yytoken = YYEOF;
      YYDPRINTF ((stderr, "Now at end of input.\n"));
    }
  else
    {
      yytoken = YYTRANSLATE (yychar);
      YY_SYMBOL_PRINT ("Next token is", yytoken, &yylval, &yylloc);
    }

  /* If the proper action on seeing token YYTOKEN is to reduce or to
     detect an error, take that action.  */
  yyn += yytoken;
  if (yyn < 0 || YYLAST < yyn || yycheck[yyn] != yytoken)
    goto yydefault;
  yyn = yytable[yyn];
  if (yyn <= 0)
    {
      if (yytable_value_is_error (yyn))
        goto yyerrlab;
      yyn = -yyn;
      goto yyreduce;
    }

  /* Count tokens shifted since error; after three, turn off error
     status.  */
  if (yyerrstatus)
    yyerrstatus--;

  /* Shift the lookahead token.  */
  YY_SYMBOL_PRINT ("Shifting", yytoken, &yylval, &yylloc);

  /* Discard the shifted token.  */
  yychar = YYEMPTY;

  yystate = yyn;
  YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN
  *++yyvsp = yylval;
  YY_IGNORE_MAYBE_UNINITIALIZED_END

  goto yynewstate;


/*-----------------------------------------------------------.
| yydefault -- do the default action for the current state.  |
`-----------------------------------------------------------*/
yydefault:
  yyn = yydefact[yystate];
  if (yyn == 0)
    goto yyerrlab;
  goto yyreduce;


/*-----------------------------.
| yyreduce -- Do a reduction.  |
`-----------------------------*/
yyreduce:
  /* yyn is the number of a rule to reduce with.  */
  yylen = yyr2[yyn];

  /* If YYLEN is nonzero, implement the default value of the action:
     '$$ = $1'.

     Otherwise, the following line sets YYVAL to garbage.
     This behavior is undocumented and Bison
     users should not rely upon it.  Assigning to YYVAL
     unconditionally makes the parser a bit smaller, and it avoids a
     GCC warning that YYVAL may be used uninitialized.  */
  yyval = yyvsp[1-yylen];


  YY_REDUCE_PRINT (yyn);
  switch (yyn)
    {
        case 4:
#line 107 "cash.y" /* yacc.c:1646  */
    {
            if (scan_ctx->verbose && scan_ctx->pid == getpid())
            {
                printf ("Result: "); cash_show_val (&(yyvsp[0])); printf("\n");
            }
            scan_ctx->result = (yyvsp[0]); 
        }
#line 1510 "cash.tab.c" /* yacc.c:1646  */
    break;

  case 9:
#line 125 "cash.y" /* yacc.c:1646  */
    {
                CHECK_SCOPE; 

                (yyval).type = T_FUNC;
                (yyval).val = cash_symval_get ((yyvsp[0]).sym);
                (yyval).mask = PTR_MASK;
                (yyval).sym = (yyvsp[0]).sym;

                if ((yyval).val != 0)
                {
                    /* initialized function can not be redeclared */
                    (yyval).lv = 0;
                }
                else
                {
                    /* function prototype can be assigned to a name */
                    (yyval).lv = (uint64_t)((intptr_t)(yyval).sym->ptr);
                }
            }
#line 1534 "cash.tab.c" /* yacc.c:1646  */
    break;

  case 12:
#line 153 "cash.y" /* yacc.c:1646  */
    {
        (yyval) = (yyvsp[-1]);
      }
#line 1542 "cash.tab.c" /* yacc.c:1646  */
    break;

  case 13:
#line 157 "cash.y" /* yacc.c:1646  */
    {
                CHECK_SCOPE; 

                bzero ((char*)&(yyval), sizeof((yyval)));
                (yyval).type = T_INT;
                (yyval).mask = UINT_MASK;

                switch ((yyvsp[-1]).mask)
                {
                    case CHAR_MASK:
                    case UCHAR_MASK:
                        (yyval).val = 1; 
                        break;
                    case SHORT_MASK:
                        (yyval).val = 2; 
                        break;
                    case UINT_MASK:
                        (yyval).val = 4; 
                        break;
                    case ULL_MASK:
                        (yyval).val = 8; 
                        break;
                }
            }
#line 1571 "cash.tab.c" /* yacc.c:1646  */
    break;

  case 14:
#line 182 "cash.y" /* yacc.c:1646  */
    {
                (yyval).type = T_INT;
                (yyval).lv = 0;
                (yyval).val = (yyvsp[0]).val; 
                (yyval).mask = (yyvsp[0]).mask;
            }
#line 1582 "cash.tab.c" /* yacc.c:1646  */
    break;

  case 15:
#line 189 "cash.y" /* yacc.c:1646  */
    {
                (yyval).type = T_INT; (yyval).lv = 0;
                (yyval).val = (yyvsp[0]).val; 
                (yyval).mask = (yyvsp[0]).mask;
            }
#line 1592 "cash.tab.c" /* yacc.c:1646  */
    break;

  case 16:
#line 195 "cash.y" /* yacc.c:1646  */
    {
                CHECK_SCOPE; 
                (yyval) = (yyvsp[0]); (yyval).val = (-(yyval).val) & (yyval).mask;
                (yyval).sym = NULL; (yyval).lv = 0; 
            }
#line 1602 "cash.tab.c" /* yacc.c:1646  */
    break;

  case 17:
#line 201 "cash.y" /* yacc.c:1646  */
    {
                CHECK_SCOPE; 
                (yyval) = (yyvsp[0]); 
            }
#line 1611 "cash.tab.c" /* yacc.c:1646  */
    break;

  case 18:
#line 206 "cash.y" /* yacc.c:1646  */
    {
                CHECK_SCOPE; 
                (yyval) = (yyvsp[0]); 
                (yyval).mask = (yyvsp[-2]).mask; 
                (yyval).val &= (yyval).mask;
                (yyval).type = (yyvsp[-2]).type;
                (yyval).lv = 0;
            }
#line 1624 "cash.tab.c" /* yacc.c:1646  */
    break;

  case 19:
#line 215 "cash.y" /* yacc.c:1646  */
    {
                CHECK_SCOPE; 
                (yyval).type = T_INT;
                (yyval).sym = NULL;
                (yyval).lv = (yyvsp[0]).val;

                switch ((yyvsp[0]).type) {
                    case T_STR:
                            (yyval).mask = CHAR_MASK;
                            (yyval).val = *(unsigned char*)cash_adrs_remap((yyvsp[0]).val);
                            break;
                    case T_STRPTR:
                            (yyval).type = T_STR;
                            (yyval).mask = PTR_MASK;
                            (yyval).val = *(unsigned char*)cash_adrs_remap((yyvsp[0]).val);
                            break;
                    case T_CHARPTR:
                            (yyval).mask = UCHAR_MASK;
                            (yyval).val = *(unsigned char*)cash_adrs_remap((yyvsp[0]).val);
                            break;
                    case T_SHORTPTR:
                            (yyval).mask = SHORT_MASK;
                            (yyval).val = *(unsigned short*)cash_adrs_remap((yyvsp[0]).val);
                            break;
                    case T_INT:
                    case T_INTPTR:
                            (yyval).mask = UINT_MASK;
                            (yyval).val = *(unsigned int*)cash_adrs_remap((yyvsp[0]).val);
                            break;
                    case T_INT64PTR:
                            (yyval).mask = ULL_MASK;
                            (yyval).val = *(uint64_t*)cash_adrs_remap((yyvsp[0]).val);
                            break;
                    case T_FCTPTR:
                            (yyval).type = T_FUNC;
                            (yyval).mask = FUNC_MASK;
                            (yyval).val = *(long*)cash_adrs_remap((yyvsp[0]).val);
                            break;
                    default:
                            CTX_PRINTF ("can not dereference non-pointer type");
                            YYABORT;
                }
            }
#line 1672 "cash.tab.c" /* yacc.c:1646  */
    break;

  case 20:
#line 259 "cash.y" /* yacc.c:1646  */
    { 
                CHECK_SCOPE; 
                scantype t;
                uint64_t v;

                if ((yyvsp[0]).sym == NULL)
                {
#ifndef ALLOW_CONSTPTR
                    CTX_PRINTF ("can not determine address of constant\n");
                    YYABORT;
#else
                    /* for dereferencing a constant, need to add a temporary
                     * storage
                     */
                    v = (intptr_t)cash_get_constp (scan_ctx, (yyvsp[0]).val, (yyvsp[0]).mask);
                    
                    if (v == 0)
                    {
                        CTX_PRINTF ("too many constant pointers (max %d)\n", MAX_CONSTP);
                        YYABORT;
                    }
                    
                    t = (yyvsp[0]).type;
#endif
                }
                else
                {
                    t = (yyvsp[0]).type;
                    v = (intptr_t)(yyvsp[0]).sym->ptr;
                }

                switch (t)
                {
                    case T_FUNC:
                        (yyval).type = T_FCTPTR;
                        break;
                    case T_STR:
                        (yyval).type = T_STRPTR;
                        break;
                    default:
                        switch ((yyvsp[0]).mask)
                        {
                            case CHAR_MASK:
                                (yyval).type = T_STR; break;
                            case UCHAR_MASK:
                                (yyval).type = T_CHARPTR; break;
                            case SHORT_MASK:
                                (yyval).type = T_SHORTPTR; break;
                            case ULL_MASK:
                                (yyval).type = T_INT64PTR; break;
                            default:
                                (yyval).type = T_INTPTR; break;
                        }
                }
                (yyval).mask = PTR_MASK;
                (yyval).sym = NULL;
                (yyval).val = v;
                (yyval).lv = 0;
            }
#line 1736 "cash.tab.c" /* yacc.c:1646  */
    break;

  case 21:
#line 319 "cash.y" /* yacc.c:1646  */
    {
                CHECK_SCOPE; 
                (yyval).type = T_INT;
                (yyval).sym = NULL;
                switch ((yyvsp[-3]).type) {
                    case T_STR:
                        (yyval).mask = CHAR_MASK;
                        (yyval).lv = (yyvsp[-3]).val + (yyvsp[-1]).val * sizeof(char);
                        (yyval).val = *(unsigned char*)cash_adrs_remap((yyval).lv);
                        break;
                    case T_CHARPTR:
                        (yyval).mask = UCHAR_MASK;
                        (yyval).lv = (yyvsp[-3]).val + (yyvsp[-1]).val * sizeof(char);
                        (yyval).val = *(unsigned char*)cash_adrs_remap((yyval).lv);
                        break;
                    case T_SHORTPTR:
                        (yyval).mask = SHORT_MASK;
                        (yyval).lv = (yyvsp[-3]).val + (yyvsp[-1]).val * sizeof(short);
                        (yyval).val = *(unsigned short*)cash_adrs_remap((yyval).lv);
                        break;
                    case T_INTPTR:
                        (yyval).mask = UINT_MASK;
                        (yyval).lv = (yyvsp[-3]).val + (yyvsp[-1]).val * sizeof(int);
                        (yyval).val = *(unsigned int*)cash_adrs_remap((yyval).lv);
                        break;
                    case T_INT64PTR:
                        (yyval).mask = ULL_MASK;
                        (yyval).lv = (yyvsp[-3]).val + (yyvsp[-1]).val * sizeof(uint64_t);
                        (yyval).val = *(uint64_t*)cash_adrs_remap((yyval).lv);
                        break;
                    default:
                        CTX_PRINTF ("can not dereference non-pointer type");
                        YYABORT;
                }
            }
#line 1776 "cash.tab.c" /* yacc.c:1646  */
    break;

  case 22:
#line 355 "cash.y" /* yacc.c:1646  */
    {
                CHECK_SCOPE; 
                (yyval).val = (yyvsp[0]).val;

                if ((yyval).val == 0)
                    YYABORT;

                (yyval).type = T_STR;
                (yyval).mask = PTR_MASK;
                (yyval).sym = NULL;
                (yyval).lv = 0;
            }
#line 1793 "cash.tab.c" /* yacc.c:1646  */
    break;

  case 23:
#line 368 "cash.y" /* yacc.c:1646  */
    {
                CHECK_SCOPE; 
                switch ((yyvsp[0]).sym->mask)
                {
                    case CHAR_MASK:
                    case UCHAR_MASK:
                        (yyval).val =  (uint64_t)(*(char*)cash_adrs_remap((intptr_t)(yyvsp[0]).sym->ptr));
                        break;
                    case SHORT_MASK:
                        (yyval).val =  (uint64_t)(*(short*)cash_adrs_remap((intptr_t)(yyvsp[0]).sym->ptr));
                        break;
                    case ULL_MASK:
                        (yyval).val =  (uint64_t)(*(uint64_t*)cash_adrs_remap((intptr_t)(yyvsp[0]).sym->ptr));
                        break;
                    default:
                        (yyval).val =  (uint64_t)(*(int*)cash_adrs_remap((intptr_t)(yyvsp[0]).sym->ptr));
                        break;
                }
                (yyval).type = (yyvsp[0]).sym->type;
                (yyval).mask = (yyvsp[0]).sym->mask;
                (yyval).sym = (yyvsp[0]).sym;
                (yyval).lv = (uint64_t)((uintptr_t)(yyvsp[0]).sym->ptr);
            }
#line 1821 "cash.tab.c" /* yacc.c:1646  */
    break;

  case 24:
#line 392 "cash.y" /* yacc.c:1646  */
    { 
                CHECK_SCOPE; 
                (yyval).val = ((yyvsp[-2]).val * (yyvsp[0]).val) & (yyvsp[-2]).mask; (yyval).mask = (yyvsp[-2]).mask; (yyval).sym = 0; (yyval).lv = 0; 
            }
#line 1830 "cash.tab.c" /* yacc.c:1646  */
    break;

  case 25:
#line 397 "cash.y" /* yacc.c:1646  */
    { 
                CHECK_SCOPE; 
                (yyval).val = ((yyvsp[-2]).val / (yyvsp[0]).val) & (yyvsp[-2]).mask; (yyval).mask = (yyvsp[-2]).mask; (yyval).sym = 0; (yyval).lv = 0; 
            }
#line 1839 "cash.tab.c" /* yacc.c:1646  */
    break;

  case 26:
#line 402 "cash.y" /* yacc.c:1646  */
    { 
                CHECK_SCOPE; 
                (yyval).val = ((yyvsp[-2]).val + (yyvsp[0]).val) & (yyvsp[-2]).mask; (yyval).mask = (yyvsp[-2]).mask; (yyval).sym = 0; (yyval).lv = 0; 
            }
#line 1848 "cash.tab.c" /* yacc.c:1646  */
    break;

  case 27:
#line 407 "cash.y" /* yacc.c:1646  */
    { 
                CHECK_SCOPE; 
                (yyval).val = ((yyvsp[-2]).val - (yyvsp[0]).val) & (yyvsp[-2]).mask; (yyval).mask = (yyvsp[-2]).mask; (yyval).sym = 0; (yyval).lv = 0; 
            }
#line 1857 "cash.tab.c" /* yacc.c:1646  */
    break;

  case 28:
#line 412 "cash.y" /* yacc.c:1646  */
    { 
                CHECK_SCOPE; 
                (yyval).val = ((yyvsp[-2]).val | (yyvsp[0]).val) & (yyvsp[-2]).mask; (yyval).mask = (yyvsp[-2]).mask; (yyval).sym = 0; (yyval).lv = 0; 
            }
#line 1866 "cash.tab.c" /* yacc.c:1646  */
    break;

  case 29:
#line 417 "cash.y" /* yacc.c:1646  */
    {
                CHECK_SCOPE; 
                (yyval).val = ((yyvsp[-2]).val & (yyvsp[0]).val) & (yyvsp[-2]).mask; (yyval).mask = (yyvsp[-2]).mask; (yyval).sym = 0; (yyval).lv = 0; 
            }
#line 1875 "cash.tab.c" /* yacc.c:1646  */
    break;

  case 30:
#line 422 "cash.y" /* yacc.c:1646  */
    {
                CHECK_SCOPE; 
                (yyval).val = ((yyvsp[-2]).val ^ (yyvsp[0]).val) & (yyvsp[-2]).mask; (yyval).mask = (yyvsp[-2]).mask; (yyval).sym = 0; (yyval).lv = 0; 
            }
#line 1884 "cash.tab.c" /* yacc.c:1646  */
    break;

  case 31:
#line 427 "cash.y" /* yacc.c:1646  */
    {
                CHECK_SCOPE; 
                (yyval).val = ((yyvsp[-2]).val % (yyvsp[0]).val) & (yyvsp[-2]).mask; (yyval).mask = (yyvsp[-2]).mask; (yyval).sym = 0; (yyval).lv = 0; 
            }
#line 1893 "cash.tab.c" /* yacc.c:1646  */
    break;

  case 32:
#line 433 "cash.y" /* yacc.c:1646  */
    {
                CHECK_SCOPE; 
                if (!assign_op (scan_ctx, &(yyvsp[-2]), &(yyvsp[0]), ADDA)) YYABORT; (yyval) = (yyvsp[-2]); 
            }
#line 1902 "cash.tab.c" /* yacc.c:1646  */
    break;

  case 33:
#line 438 "cash.y" /* yacc.c:1646  */
    {
                CHECK_SCOPE; 
                if (!assign_op (scan_ctx, &(yyvsp[-2]), &(yyvsp[0]), SUBA)) YYABORT; (yyval) = (yyvsp[-2]); 
            }
#line 1911 "cash.tab.c" /* yacc.c:1646  */
    break;

  case 34:
#line 443 "cash.y" /* yacc.c:1646  */
    {
                CHECK_SCOPE; 
                if (!assign_op (scan_ctx, &(yyvsp[-2]), &(yyvsp[0]), MULA)) YYABORT; (yyval) = (yyvsp[-2]); 
            }
#line 1920 "cash.tab.c" /* yacc.c:1646  */
    break;

  case 35:
#line 448 "cash.y" /* yacc.c:1646  */
    {
                CHECK_SCOPE; 
                if (!assign_op (scan_ctx, &(yyvsp[-2]), &(yyvsp[0]), DIVA)) YYABORT; (yyval) = (yyvsp[-2]); 
            }
#line 1929 "cash.tab.c" /* yacc.c:1646  */
    break;

  case 36:
#line 453 "cash.y" /* yacc.c:1646  */
    {
                CHECK_SCOPE; 
                if (!assign_op (scan_ctx, &(yyvsp[-2]), &(yyvsp[0]), ORA)) YYABORT; (yyval) = (yyvsp[-2]); 
            }
#line 1938 "cash.tab.c" /* yacc.c:1646  */
    break;

  case 37:
#line 458 "cash.y" /* yacc.c:1646  */
    {
                CHECK_SCOPE; 
                if (!assign_op (scan_ctx, &(yyvsp[-2]), &(yyvsp[0]), ANDA)) YYABORT; (yyval) = (yyvsp[-2]); 
            }
#line 1947 "cash.tab.c" /* yacc.c:1646  */
    break;

  case 38:
#line 463 "cash.y" /* yacc.c:1646  */
    {
                CHECK_SCOPE; 
                if (!assign_op (scan_ctx, &(yyvsp[-2]), &(yyvsp[0]), XORA)) YYABORT; (yyval) = (yyvsp[-2]); 
            }
#line 1956 "cash.tab.c" /* yacc.c:1646  */
    break;

  case 39:
#line 468 "cash.y" /* yacc.c:1646  */
    {
                CHECK_SCOPE; 
                if (!assign_op (scan_ctx, &(yyvsp[-2]), &(yyvsp[0]), MODA)) YYABORT; (yyval) = (yyvsp[-2]); 
            }
#line 1965 "cash.tab.c" /* yacc.c:1646  */
    break;

  case 40:
#line 473 "cash.y" /* yacc.c:1646  */
    {
                CHECK_SCOPE; 
                if (!assign_op (scan_ctx, &(yyvsp[-2]), &(yyvsp[0]), SHLA)) YYABORT; (yyval) = (yyvsp[-2]); 
            }
#line 1974 "cash.tab.c" /* yacc.c:1646  */
    break;

  case 41:
#line 478 "cash.y" /* yacc.c:1646  */
    {
                CHECK_SCOPE; 
                if (!assign_op (scan_ctx, &(yyvsp[-2]), &(yyvsp[0]), SHRA)) YYABORT; (yyval) = (yyvsp[-2]); 
            }
#line 1983 "cash.tab.c" /* yacc.c:1646  */
    break;

  case 42:
#line 484 "cash.y" /* yacc.c:1646  */
    {
                CHECK_SCOPE; 
                (yyval)=(yyvsp[-1]); if (!assign_op (scan_ctx, &(yyvsp[-1]), NULL, AI)) YYABORT; 
            }
#line 1992 "cash.tab.c" /* yacc.c:1646  */
    break;

  case 43:
#line 489 "cash.y" /* yacc.c:1646  */
    {
                CHECK_SCOPE; 
                (yyval)=(yyvsp[-1]); if (!assign_op (scan_ctx, &(yyvsp[-1]), NULL, AD)) YYABORT; 
            }
#line 2001 "cash.tab.c" /* yacc.c:1646  */
    break;

  case 44:
#line 495 "cash.y" /* yacc.c:1646  */
    {
                CHECK_SCOPE; 
                if (!assign_op (scan_ctx, &(yyvsp[0]), NULL, AI)) YYABORT; (yyval) = (yyvsp[0]); 
            }
#line 2010 "cash.tab.c" /* yacc.c:1646  */
    break;

  case 45:
#line 500 "cash.y" /* yacc.c:1646  */
    {
                CHECK_SCOPE; 
                if (!assign_op (scan_ctx, &(yyvsp[0]), NULL, AD)) YYABORT; (yyval) = (yyvsp[0]); 
            }
#line 2019 "cash.tab.c" /* yacc.c:1646  */
    break;

  case 46:
#line 506 "cash.y" /* yacc.c:1646  */
    {
                CHECK_SCOPE; 
                (yyval).val = ((yyvsp[-2]).val && (yyvsp[0]).val); (yyval).mask = (yyvsp[-2]).mask; (yyval).type = T_INT; (yyval).sym = 0; (yyval).lv = 0; 
            }
#line 2028 "cash.tab.c" /* yacc.c:1646  */
    break;

  case 47:
#line 511 "cash.y" /* yacc.c:1646  */
    {
                CHECK_SCOPE; 
                (yyval).val = ((yyvsp[-2]).val || (yyvsp[0]).val); (yyval).mask = (yyvsp[-2]).mask; (yyval).type = T_INT; (yyval).sym = 0; (yyval).lv = 0; 
            }
#line 2037 "cash.tab.c" /* yacc.c:1646  */
    break;

  case 48:
#line 516 "cash.y" /* yacc.c:1646  */
    {
                CHECK_SCOPE; 
                (yyval).val = ((yyvsp[-2]).val == (yyvsp[0]).val); (yyval).mask = (yyvsp[-2]).mask; (yyval).type = T_INT; (yyval).sym = 0; (yyval).lv = 0; 
            }
#line 2046 "cash.tab.c" /* yacc.c:1646  */
    break;

  case 49:
#line 521 "cash.y" /* yacc.c:1646  */
    {
                CHECK_SCOPE; 
                (yyval).val = ((yyvsp[-2]).val != (yyvsp[0]).val); (yyval).mask = (yyvsp[-2]).mask; (yyval).type = T_INT; (yyval).sym = 0; (yyval).lv = 0; 
            }
#line 2055 "cash.tab.c" /* yacc.c:1646  */
    break;

  case 50:
#line 526 "cash.y" /* yacc.c:1646  */
    {
                CHECK_SCOPE; 
                (yyval).val = ((yyvsp[-2]).val > (yyvsp[0]).val); (yyval).mask = (yyvsp[-2]).mask; (yyval).type = T_INT; (yyval).sym = 0; (yyval).lv = 0; 
            }
#line 2064 "cash.tab.c" /* yacc.c:1646  */
    break;

  case 51:
#line 531 "cash.y" /* yacc.c:1646  */
    {
                CHECK_SCOPE; 
                (yyval).val = ((yyvsp[-2]).val < (yyvsp[0]).val); (yyval).mask = (yyvsp[-2]).mask; (yyval).type = T_INT; (yyval).sym = 0; (yyval).lv = 0; 
            }
#line 2073 "cash.tab.c" /* yacc.c:1646  */
    break;

  case 52:
#line 536 "cash.y" /* yacc.c:1646  */
    {
                CHECK_SCOPE; 
                (yyval).val = ((yyvsp[-2]).val >= (yyvsp[0]).val); (yyval).mask = (yyvsp[-2]).mask; (yyval).type = T_INT; (yyval).sym = 0; (yyval).lv = 0; 
            }
#line 2082 "cash.tab.c" /* yacc.c:1646  */
    break;

  case 53:
#line 541 "cash.y" /* yacc.c:1646  */
    {
                CHECK_SCOPE; 
                (yyval).val = ((yyvsp[-2]).val <= (yyvsp[0]).val); (yyval).mask = (yyvsp[-2]).mask; (yyval).type = T_INT; (yyval).sym = 0; (yyval).lv = 0; 
            }
#line 2091 "cash.tab.c" /* yacc.c:1646  */
    break;

  case 54:
#line 547 "cash.y" /* yacc.c:1646  */
    {
                CHECK_SCOPE; 
                (yyval).val = ((yyvsp[-2]).val >> (yyvsp[0]).val) & (yyvsp[-2]).mask; (yyval).mask = (yyvsp[-2]).mask; (yyval).sym = 0; (yyval).lv = 0; 
            }
#line 2100 "cash.tab.c" /* yacc.c:1646  */
    break;

  case 55:
#line 552 "cash.y" /* yacc.c:1646  */
    {
                CHECK_SCOPE; 
                (yyval).val = ((yyvsp[-2]).val << (yyvsp[0]).val) & (yyvsp[-2]).mask; (yyval).mask = (yyvsp[-2]).mask; (yyval).sym = 0; (yyval).lv = 0;
            }
#line 2109 "cash.tab.c" /* yacc.c:1646  */
    break;

  case 56:
#line 557 "cash.y" /* yacc.c:1646  */
    {SCOPE_START((yyvsp[-1]).val != 0)}
#line 2115 "cash.tab.c" /* yacc.c:1646  */
    break;

  case 57:
#line 557 "cash.y" /* yacc.c:1646  */
    {SCOPE_END; SCOPE_START((yyvsp[-3]).val == 0);}
#line 2121 "cash.tab.c" /* yacc.c:1646  */
    break;

  case 58:
#line 559 "cash.y" /* yacc.c:1646  */
    {
                SCOPE_END; 
                CHECK_SCOPE; 
                (yyval).val = (yyvsp[-6]).val ? (yyvsp[-3]).val : (yyvsp[0]).val;
                (yyval).mask = (yyvsp[-6]).val ? (yyvsp[-3]).mask : (yyvsp[0]).mask;
                (yyval).val &= (yyval).mask; 
                (yyval).sym = 0;
                (yyval).lv = 0; 
            }
#line 2135 "cash.tab.c" /* yacc.c:1646  */
    break;

  case 59:
#line 569 "cash.y" /* yacc.c:1646  */
    {
                CHECK_SCOPE; 
                (yyval) = (yyvsp[0]); (yyval).val = (!(yyval).val) & (yyval).mask; (yyval).sym = 0; (yyval).lv = 0; 
            }
#line 2144 "cash.tab.c" /* yacc.c:1646  */
    break;

  case 60:
#line 574 "cash.y" /* yacc.c:1646  */
    {
                CHECK_SCOPE; 
                (yyval) = (yyvsp[0]); (yyval).val = (~(yyval).val) & (yyval).mask; (yyval).sym = 0; (yyval).lv = 0; 
            }
#line 2153 "cash.tab.c" /* yacc.c:1646  */
    break;

  case 61:
#line 579 "cash.y" /* yacc.c:1646  */
    { 
                CHECK_SCOPE; 
                funcCall (scan_ctx, (cash_fp*)((uintptr_t)(yyvsp[-3]).val), NULL, &(yyvsp[-1]), &(yyval));
            }
#line 2162 "cash.tab.c" /* yacc.c:1646  */
    break;

  case 62:
#line 584 "cash.y" /* yacc.c:1646  */
    { 
                CHECK_SCOPE;
                funcCall (scan_ctx, (cash_fp*)((uintptr_t)cash_symval_get ((yyvsp[-3]).sym)), (yyvsp[-3]).sym, &(yyvsp[-1]), &(yyval));
            }
#line 2171 "cash.tab.c" /* yacc.c:1646  */
    break;

  case 63:
#line 589 "cash.y" /* yacc.c:1646  */
    {
                CHECK_SCOPE;
                SYMTAB_ENTRY        *e;
                
                e = dl_func_get ((char*)(uintptr_t)(yyvsp[-3]).val, NULL);
 
                if (e == NULL)
                {
                    CTX_PRINTF("undefined symbol : %s\n", (char*)(uintptr_t)(yyvsp[-3]).val);
                    free ((char*)(uintptr_t)(yyvsp[-3]).val);
                    YYABORT;
                }
                else
                {
                    funcCall (scan_ctx, (cash_fp*)((uintptr_t)cash_symval_get (e)), e, &(yyvsp[-1]), &(yyval));
                }
            }
#line 2193 "cash.tab.c" /* yacc.c:1646  */
    break;

  case 64:
#line 607 "cash.y" /* yacc.c:1646  */
    {
                CHECK_SCOPE; 
                int flag = (yyvsp[-4]).val ? 0 : ITER_TERM_LOCAL;
                cash_do_iterator ((scan_ctx_t*)scan_ctx, &(yyvsp[-2]), NULL, &(yyval), flag);
            }
#line 2203 "cash.tab.c" /* yacc.c:1646  */
    break;

  case 65:
#line 613 "cash.y" /* yacc.c:1646  */
    {
                CHECK_SCOPE; 
                int flag = (yyvsp[-5]).val ? 0 : ITER_TERM_LOCAL;
                cash_do_iterator ((scan_ctx_t*)scan_ctx, &(yyvsp[-3]), &(yyvsp[-1]), &(yyval), flag);
            }
#line 2213 "cash.tab.c" /* yacc.c:1646  */
    break;

  case 66:
#line 619 "cash.y" /* yacc.c:1646  */
    {
                CHECK_SCOPE; 
                cash_do_iterator ((scan_ctx_t*)scan_ctx, &(yyvsp[-3]), &(yyvsp[-1]), &(yyval), 0);
            }
#line 2222 "cash.tab.c" /* yacc.c:1646  */
    break;

  case 67:
#line 624 "cash.y" /* yacc.c:1646  */
    {
                CHECK_SCOPE; 
                int flag = (yyvsp[-3]).val ? 0 : ITER_TERM_LOCAL;
                flag |= ITER_VALUESET;
                cash_do_iterator ((scan_ctx_t*)scan_ctx, &(yyvsp[-1]), NULL, &(yyval), flag );
            }
#line 2233 "cash.tab.c" /* yacc.c:1646  */
    break;

  case 68:
#line 631 "cash.y" /* yacc.c:1646  */
    {
                CHECK_SCOPE; 
                cash_do_iterator ((scan_ctx_t*)scan_ctx, &(yyvsp[-1]), NULL, &(yyval), ITER_VALUESET );
            }
#line 2242 "cash.tab.c" /* yacc.c:1646  */
    break;

  case 71:
#line 641 "cash.y" /* yacc.c:1646  */
    {
                CHECK_SCOPE; 
                (yyval).num_args = 1; 
                memset ((void*)&(yyval).args, 0, sizeof ((yyval).args));
                (yyval).args[0] = (yyvsp[0]).val; 
                (yyval).argsize[0] = (yyvsp[0]).mask; 
            }
#line 2254 "cash.tab.c" /* yacc.c:1646  */
    break;

  case 72:
#line 649 "cash.y" /* yacc.c:1646  */
    {
                static int warn = 0;
                CHECK_SCOPE; 
                
                if (!warn && scan_ctx->iterator_warn)
                {
                    CTX_PRINTF ("cash: use of comma for iterator is deprecated, use colon instead!\n");
                    warn = 1;
                }
                
                if ((yyval).num_args >= MAX_PARMS) 
                {
                        CTX_PRINTF ("Too many function parameters (max %d)\n", 
                                        MAX_PARMS);
                        YYABORT;
                }
                (yyval).args[(yyval).num_args] = (yyvsp[0]).val;
                (yyval).argsize[(yyval).num_args] = (yyvsp[0]).mask; 
                (yyval).num_args++;
            }
#line 2279 "cash.tab.c" /* yacc.c:1646  */
    break;

  case 73:
#line 670 "cash.y" /* yacc.c:1646  */
    {
                CHECK_SCOPE; 
                if ((yyval).num_args >= MAX_PARMS) 
                {
                        CTX_PRINTF ("Too many function parameters (max %d)\n", 
                                        MAX_PARMS);
                        YYABORT;
                }
                (yyval).args[(yyval).num_args] = (yyvsp[0]).val;
                (yyval).argsize[(yyval).num_args] = (yyvsp[0]).mask; 
                (yyval).num_args++;
            }
#line 2296 "cash.tab.c" /* yacc.c:1646  */
    break;

  case 74:
#line 686 "cash.y" /* yacc.c:1646  */
    {
                CHECK_SCOPE; 
                (yyval).num_args = 0; 
                memset ((void*)&(yyval).args, 0, sizeof ((yyval).args)); 
            }
#line 2306 "cash.tab.c" /* yacc.c:1646  */
    break;

  case 76:
#line 696 "cash.y" /* yacc.c:1646  */
    {
                CHECK_SCOPE; 
                (yyval).num_args = 1; 
                memset ((void*)&(yyval).args, 0, sizeof ((yyval).args));
                (yyval).args[0] = (yyvsp[0]).val; 
                (yyval).argsize[0] = (yyvsp[0]).mask; 
            }
#line 2318 "cash.tab.c" /* yacc.c:1646  */
    break;

  case 77:
#line 704 "cash.y" /* yacc.c:1646  */
    {
                CHECK_SCOPE; 
                if ((yyval).num_args >= MAX_PARMS) 
                {
                    CTX_PRINTF ("Too many function parameters (max %d)\n", 
                                    MAX_PARMS);
                    YYABORT;
                }
                (yyval).args[(yyval).num_args] = (yyvsp[0]).val;
                (yyval).argsize[(yyval).num_args] = (yyvsp[0]).mask; 
                (yyval).num_args++;
            }
#line 2335 "cash.tab.c" /* yacc.c:1646  */
    break;

  case 78:
#line 720 "cash.y" /* yacc.c:1646  */
    {
                CHECK_SCOPE; 
                init_proto_args (&(yyval));
            }
#line 2344 "cash.tab.c" /* yacc.c:1646  */
    break;

  case 80:
#line 729 "cash.y" /* yacc.c:1646  */
    {
                CHECK_SCOPE; 
                init_proto_args (&(yyval));
                add_pa_type (&(yyval), &(yyvsp[0]));
            }
#line 2354 "cash.tab.c" /* yacc.c:1646  */
    break;

  case 81:
#line 735 "cash.y" /* yacc.c:1646  */
    {
                CHECK_SCOPE; 
                add_pa_type (&(yyval), &(yyvsp[0]));
            }
#line 2363 "cash.tab.c" /* yacc.c:1646  */
    break;

  case 82:
#line 743 "cash.y" /* yacc.c:1646  */
    {
                char *name = (char*)((uintptr_t)(yyvsp[0]).val);
                CHECK_SCOPE; 
                if (!new_sym (name, (yyvsp[-1]).type, &(yyval), (yyvsp[-1]).mask)) 
                {
                    free (name);
                    YYABORT; 
                }
            }
#line 2377 "cash.tab.c" /* yacc.c:1646  */
    break;

  case 83:
#line 753 "cash.y" /* yacc.c:1646  */
    {
                char *name = (char*)((uintptr_t)(yyvsp[0]).val);
                CHECK_SCOPE; 
                if (!new_libdl_sym (name, (yyvsp[-1]).type, &(yyval), (yyvsp[-1]).mask)) 
                {
                free (name);
                    YYABORT; 
                }
            }
#line 2391 "cash.tab.c" /* yacc.c:1646  */
    break;

  case 84:
#line 763 "cash.y" /* yacc.c:1646  */
    {
                CHECK_SCOPE; 
                if ((yyvsp[-1]).type != (yyvsp[0]).sym->type)
                {
                    CTX_PRINTF ("<%s> : Can not redefine as different type\n", 
                                    (yyvsp[0]).sym->name); 
                    YYABORT; 
                }

                if ((yyvsp[-1]).mask != (yyvsp[0]).sym->mask)
                {
                    CTX_PRINTF ("<%s> : Can not redefine storage width\n", 
                                        (yyvsp[0]).sym->name); 
                    CTX_PRINTF ("<%s> : Use construct: <type> (<symbol>)\n", 
                                        (yyvsp[0]).sym->name); 
                    YYABORT; 
                }

                get_sym (&(yyval), (yyvsp[0]).sym);
            }
#line 2416 "cash.tab.c" /* yacc.c:1646  */
    break;

  case 85:
#line 785 "cash.y" /* yacc.c:1646  */
    {
                CHECK_SCOPE; 
                if ((yyvsp[-3]).type != (yyvsp[-1]).sym->type)
                {
                    CTX_PRINTF ("<%s> : Can not redefine as different type\n", 
                                    (yyvsp[-1]).sym->name); 
                    YYABORT; 
                }
                (yyvsp[-1]).sym->mask = (yyvsp[-3]).mask;
                get_sym (&(yyval), (yyvsp[-1]).sym);
            }
#line 2432 "cash.tab.c" /* yacc.c:1646  */
    break;

  case 86:
#line 798 "cash.y" /* yacc.c:1646  */
    {
                CHECK_SCOPE; 
                if ((yyvsp[-3]).sym->proto)
                    free ((yyvsp[-3]).sym->proto);

                (yyvsp[-3]).sym->proto = (yyvsp[-1]).proto;
                (yyvsp[-3]).sym->proto->rcsize = (yyvsp[-4]).mask;


                (yyval) = (yyvsp[-3]);
                (yyval).type = T_FUNC;
                (yyval).val = *(uintptr_t*)(yyvsp[-3]).sym->ptr;

                if ((yyval).val != 0)
                {
                    /* initialized function can not be redeclared */
                    (yyval).lv = 0;
                }
                else
                {
                    /* function prototype can be assigned to a name */
                    (yyval).lv = (uint64_t)((uintptr_t)(yyval).sym->ptr);
                }
            }
#line 2461 "cash.tab.c" /* yacc.c:1646  */
    break;

  case 87:
#line 823 "cash.y" /* yacc.c:1646  */
    {
                CHECK_SCOPE; 
                SYMTAB_ENTRY *s;

                s = new_sym ((char*)((uintptr_t)(yyvsp[-3]).val), T_FUNC, &(yyval), PTR_MASK); 

                if (s)
                {
                    s->proto = (yyvsp[-1]).proto;
                    s->proto->rcsize = (yyvsp[-4]).mask;
                }
                else
                {
                    (yyval).type = T_ANON;
                }
            }
#line 2482 "cash.tab.c" /* yacc.c:1646  */
    break;

  case 88:
#line 840 "cash.y" /* yacc.c:1646  */
    {
                CHECK_SCOPE; 
                SYMTAB_ENTRY *s;
 
                s = new_libdl_sym ((char*)((uintptr_t)(yyvsp[-3]).val), T_FUNC, &(yyval), PTR_MASK); 
 
                if (s)
                {
                    s->proto = (yyvsp[-1]).proto;
                    s->proto->rcsize = (yyvsp[-4]).mask;
                }
                else
                {
                    (yyval).type = T_ANON;
                }
            }
#line 2503 "cash.tab.c" /* yacc.c:1646  */
    break;

  case 89:
#line 859 "cash.y" /* yacc.c:1646  */
    { (yyval).type = T_INT; (yyval).mask = CHAR_MASK; }
#line 2509 "cash.tab.c" /* yacc.c:1646  */
    break;

  case 90:
#line 860 "cash.y" /* yacc.c:1646  */
    { (yyval).type = T_INT; (yyval).mask = UCHAR_MASK;  }
#line 2515 "cash.tab.c" /* yacc.c:1646  */
    break;

  case 91:
#line 861 "cash.y" /* yacc.c:1646  */
    { (yyval).type = T_INT; (yyval).mask = SHORT_MASK; }
#line 2521 "cash.tab.c" /* yacc.c:1646  */
    break;

  case 92:
#line 862 "cash.y" /* yacc.c:1646  */
    { (yyval).type = T_INT; (yyval).mask = UINT_MASK; }
#line 2527 "cash.tab.c" /* yacc.c:1646  */
    break;

  case 93:
#line 863 "cash.y" /* yacc.c:1646  */
    { (yyval).type = T_INT; (yyval).mask = UINT_MASK; }
#line 2533 "cash.tab.c" /* yacc.c:1646  */
    break;

  case 94:
#line 864 "cash.y" /* yacc.c:1646  */
    { (yyval).type = T_INT; (yyval).mask = ULL_MASK; }
#line 2539 "cash.tab.c" /* yacc.c:1646  */
    break;

  case 95:
#line 865 "cash.y" /* yacc.c:1646  */
    { (yyval).type = T_INT; (yyval).mask = UL_MASK; }
#line 2545 "cash.tab.c" /* yacc.c:1646  */
    break;

  case 96:
#line 866 "cash.y" /* yacc.c:1646  */
    { (yyval).type = T_CHARPTR; (yyval).mask = PTR_MASK; }
#line 2551 "cash.tab.c" /* yacc.c:1646  */
    break;

  case 97:
#line 867 "cash.y" /* yacc.c:1646  */
    { (yyval).type = T_STR; (yyval).mask = PTR_MASK; }
#line 2557 "cash.tab.c" /* yacc.c:1646  */
    break;

  case 98:
#line 868 "cash.y" /* yacc.c:1646  */
    { (yyval).type = T_CHARPTR; (yyval).mask = PTR_MASK; }
#line 2563 "cash.tab.c" /* yacc.c:1646  */
    break;

  case 99:
#line 869 "cash.y" /* yacc.c:1646  */
    { (yyval).type = T_SHORTPTR; (yyval).mask = PTR_MASK; }
#line 2569 "cash.tab.c" /* yacc.c:1646  */
    break;

  case 100:
#line 870 "cash.y" /* yacc.c:1646  */
    { (yyval).type = T_INTPTR; (yyval).mask = PTR_MASK; }
#line 2575 "cash.tab.c" /* yacc.c:1646  */
    break;

  case 101:
#line 871 "cash.y" /* yacc.c:1646  */
    { (yyval).type = T_INT64PTR; (yyval).mask = PTR_MASK; }
#line 2581 "cash.tab.c" /* yacc.c:1646  */
    break;

  case 102:
#line 872 "cash.y" /* yacc.c:1646  */
    { (yyval).type = (PTR_MASK == 0xffffffffULL)?T_INTPTR:T_INT64PTR;
                  (yyval).mask = PTR_MASK; }
#line 2588 "cash.tab.c" /* yacc.c:1646  */
    break;

  case 103:
#line 880 "cash.y" /* yacc.c:1646  */
    {
                CHECK_SCOPE;
                if ((yyvsp[-2]).lv == 0)
                {
                    CTX_PRINTF ("bad lvalue in assignment\n");
                    YYABORT;
                }

                if (scan_ctx->verbose != 2)
                    scan_ctx->verbose = 0;

                switch ((yyvsp[-2]).mask)
                {
                case CHAR_MASK:
                case UCHAR_MASK:
                    *(char*)cash_adrs_remap((yyvsp[-2]).lv) = (yyvsp[0]).val; 
                    (yyval).val = *(char*)cash_adrs_remap((yyvsp[-2]).lv);
                    break;
                case SHORT_MASK:
                    *(short*)cash_adrs_remap((yyvsp[-2]).lv) = (yyvsp[0]).val; 
                    (yyval).val = *(short*)cash_adrs_remap((yyvsp[-2]).lv);
                    break;
                case ULL_MASK:
                    *(uint64_t*)cash_adrs_remap((yyvsp[-2]).lv) = (yyvsp[0]).val; 
                    (yyval).val = *(uint64_t*)cash_adrs_remap((yyvsp[-2]).lv);
                    break;
                default:
                    *(int*)cash_adrs_remap((yyvsp[-2]).lv) = (yyvsp[0]).val; 
                    (yyval).val = *(int*)cash_adrs_remap((yyvsp[-2]).lv);
                    break;
                }
                (yyval).type = T_INT;
                (yyval).sym = NULL;
                (yyval).lv = 0;
            }
#line 2628 "cash.tab.c" /* yacc.c:1646  */
    break;

  case 104:
#line 916 "cash.y" /* yacc.c:1646  */
    {
                CHECK_SCOPE;

                if (!new_sym ((char*)((uintptr_t)(yyvsp[-2]).val), (yyvsp[0]).type, &(yyval), (yyvsp[0]).mask))
                {
                    free ((char*)(uintptr_t)(yyvsp[-2]).val);
                    YYABORT;
                }
                (yyval).val = (yyvsp[0]).val;
 
                switch ((yyval).mask)
                {
                case CHAR_MASK:
                case UCHAR_MASK:
                    *(char*)cash_adrs_remap((yyval).lv) = (yyval).val; 
                    break;
                case SHORT_MASK:
                    *(short*)cash_adrs_remap((yyval).lv) = (yyval).val; 
                    break;
                case ULL_MASK:
                    *(uint64_t*)cash_adrs_remap((yyval).lv) = (yyval).val; 
                    break;
                default:
                    *(int*)cash_adrs_remap((yyval).lv) = (yyval).val; 
                    break;
                }
            }
#line 2660 "cash.tab.c" /* yacc.c:1646  */
    break;

  case 105:
#line 944 "cash.y" /* yacc.c:1646  */
    {
                CHECK_SCOPE;
 
                if (!new_libdl_sym ((char*)((uintptr_t)(yyvsp[-2]).val), (yyvsp[0]).type, &(yyval), (yyvsp[0]).mask))
                {
                    free ((char*)(uintptr_t)(yyvsp[-2]).val);
                    YYABORT;
                }
                (yyval).val = (yyvsp[0]).val;

                switch ((yyval).mask)
                {
                case CHAR_MASK:
                case UCHAR_MASK:
                    *(char*)cash_adrs_remap((yyval).lv) = (yyval).val; 
                    break;
                case SHORT_MASK:
                    *(short*)cash_adrs_remap((yyval).lv) = (yyval).val; 
                    break;
                case ULL_MASK:
                    *(uint64_t*)cash_adrs_remap((yyval).lv) = (yyval).val; 
                    break;
                default:
                    *(int*)cash_adrs_remap((yyval).lv) = (yyval).val; 
                    break;
                }
            }
#line 2692 "cash.tab.c" /* yacc.c:1646  */
    break;

  case 106:
#line 972 "cash.y" /* yacc.c:1646  */
    {
                CHECK_SCOPE;
                CTX_PRINTF("undefined symbol: %s\n", (char*)(uintptr_t)(yyvsp[0]).val);
                free ((char*)(uintptr_t)(yyvsp[0]).val);
                YYABORT;
            }
#line 2703 "cash.tab.c" /* yacc.c:1646  */
    break;


#line 2707 "cash.tab.c" /* yacc.c:1646  */
      default: break;
    }
  /* User semantic actions sometimes alter yychar, and that requires
     that yytoken be updated with the new translation.  We take the
     approach of translating immediately before every use of yytoken.
     One alternative is translating here after every semantic action,
     but that translation would be missed if the semantic action invokes
     YYABORT, YYACCEPT, or YYERROR immediately after altering yychar or
     if it invokes YYBACKUP.  In the case of YYABORT or YYACCEPT, an
     incorrect destructor might then be invoked immediately.  In the
     case of YYERROR or YYBACKUP, subsequent parser actions might lead
     to an incorrect destructor call or verbose syntax error message
     before the lookahead is translated.  */
  YY_SYMBOL_PRINT ("-> $$ =", yyr1[yyn], &yyval, &yyloc);

  YYPOPSTACK (yylen);
  yylen = 0;
  YY_STACK_PRINT (yyss, yyssp);

  *++yyvsp = yyval;

  /* Now 'shift' the result of the reduction.  Determine what state
     that goes to, based on the state we popped back to and the rule
     number reduced by.  */

  yyn = yyr1[yyn];

  yystate = yypgoto[yyn - YYNTOKENS] + *yyssp;
  if (0 <= yystate && yystate <= YYLAST && yycheck[yystate] == *yyssp)
    yystate = yytable[yystate];
  else
    yystate = yydefgoto[yyn - YYNTOKENS];

  goto yynewstate;


/*--------------------------------------.
| yyerrlab -- here on detecting error.  |
`--------------------------------------*/
yyerrlab:
  /* Make sure we have latest lookahead translation.  See comments at
     user semantic actions for why this is necessary.  */
  yytoken = yychar == YYEMPTY ? YYEMPTY : YYTRANSLATE (yychar);

  /* If not already recovering from an error, report this error.  */
  if (!yyerrstatus)
    {
      ++yynerrs;
#if ! YYERROR_VERBOSE
      yyerror (scan_ctx, YY_("syntax error"));
#else
# define YYSYNTAX_ERROR yysyntax_error (&yymsg_alloc, &yymsg, \
                                        yyssp, yytoken)
      {
        char const *yymsgp = YY_("syntax error");
        int yysyntax_error_status;
        yysyntax_error_status = YYSYNTAX_ERROR;
        if (yysyntax_error_status == 0)
          yymsgp = yymsg;
        else if (yysyntax_error_status == 1)
          {
            if (yymsg != yymsgbuf)
              YYSTACK_FREE (yymsg);
            yymsg = (char *) YYSTACK_ALLOC (yymsg_alloc);
            if (!yymsg)
              {
                yymsg = yymsgbuf;
                yymsg_alloc = sizeof yymsgbuf;
                yysyntax_error_status = 2;
              }
            else
              {
                yysyntax_error_status = YYSYNTAX_ERROR;
                yymsgp = yymsg;
              }
          }
        yyerror (scan_ctx, yymsgp);
        if (yysyntax_error_status == 2)
          goto yyexhaustedlab;
      }
# undef YYSYNTAX_ERROR
#endif
    }



  if (yyerrstatus == 3)
    {
      /* If just tried and failed to reuse lookahead token after an
         error, discard it.  */

      if (yychar <= YYEOF)
        {
          /* Return failure if at end of input.  */
          if (yychar == YYEOF)
            YYABORT;
        }
      else
        {
          yydestruct ("Error: discarding",
                      yytoken, &yylval, scan_ctx);
          yychar = YYEMPTY;
        }
    }

  /* Else will try to reuse lookahead token after shifting the error
     token.  */
  goto yyerrlab1;


/*---------------------------------------------------.
| yyerrorlab -- error raised explicitly by YYERROR.  |
`---------------------------------------------------*/
yyerrorlab:

  /* Pacify compilers like GCC when the user code never invokes
     YYERROR and the label yyerrorlab therefore never appears in user
     code.  */
  if (/*CONSTCOND*/ 0)
     goto yyerrorlab;

  /* Do not reclaim the symbols of the rule whose action triggered
     this YYERROR.  */
  YYPOPSTACK (yylen);
  yylen = 0;
  YY_STACK_PRINT (yyss, yyssp);
  yystate = *yyssp;
  goto yyerrlab1;


/*-------------------------------------------------------------.
| yyerrlab1 -- common code for both syntax error and YYERROR.  |
`-------------------------------------------------------------*/
yyerrlab1:
  yyerrstatus = 3;      /* Each real token shifted decrements this.  */

  for (;;)
    {
      yyn = yypact[yystate];
      if (!yypact_value_is_default (yyn))
        {
          yyn += YYTERROR;
          if (0 <= yyn && yyn <= YYLAST && yycheck[yyn] == YYTERROR)
            {
              yyn = yytable[yyn];
              if (0 < yyn)
                break;
            }
        }

      /* Pop the current state because it cannot handle the error token.  */
      if (yyssp == yyss)
        YYABORT;


      yydestruct ("Error: popping",
                  yystos[yystate], yyvsp, scan_ctx);
      YYPOPSTACK (1);
      yystate = *yyssp;
      YY_STACK_PRINT (yyss, yyssp);
    }

  YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN
  *++yyvsp = yylval;
  YY_IGNORE_MAYBE_UNINITIALIZED_END


  /* Shift the error token.  */
  YY_SYMBOL_PRINT ("Shifting", yystos[yyn], yyvsp, yylsp);

  yystate = yyn;
  goto yynewstate;


/*-------------------------------------.
| yyacceptlab -- YYACCEPT comes here.  |
`-------------------------------------*/
yyacceptlab:
  yyresult = 0;
  goto yyreturn;

/*-----------------------------------.
| yyabortlab -- YYABORT comes here.  |
`-----------------------------------*/
yyabortlab:
  yyresult = 1;
  goto yyreturn;

#if !defined yyoverflow || YYERROR_VERBOSE
/*-------------------------------------------------.
| yyexhaustedlab -- memory exhaustion comes here.  |
`-------------------------------------------------*/
yyexhaustedlab:
  yyerror (scan_ctx, YY_("memory exhausted"));
  yyresult = 2;
  /* Fall through.  */
#endif

yyreturn:
  if (yychar != YYEMPTY)
    {
      /* Make sure we have latest lookahead translation.  See comments at
         user semantic actions for why this is necessary.  */
      yytoken = YYTRANSLATE (yychar);
      yydestruct ("Cleanup: discarding lookahead",
                  yytoken, &yylval, scan_ctx);
    }
  /* Do not reclaim the symbols of the rule whose action triggered
     this YYABORT or YYACCEPT.  */
  YYPOPSTACK (yylen);
  YY_STACK_PRINT (yyss, yyssp);
  while (yyssp != yyss)
    {
      yydestruct ("Cleanup: popping",
                  yystos[*yyssp], yyvsp, scan_ctx);
      YYPOPSTACK (1);
    }
#ifndef yyoverflow
  if (yyss != yyssa)
    YYSTACK_FREE (yyss);
#endif
#if YYERROR_VERBOSE
  if (yymsg != yymsgbuf)
    YYSTACK_FREE (yymsg);
#endif
  return yyresult;
}
#line 981 "cash.y" /* yacc.c:1906  */


void funcCall (scan_ctx_t *scan_ctx, cash_fp *func, SYMTAB_ENTRY *sym, VALUE *args, VALUE *res)
{
#ifdef DYNCALL
    int i;
    FCT_PROTO *proto = NULL;
    DCCallVM * callVm = dcNewCallVM (100);
    uint64_t argsize;
    uint64_t arg;

    if (!callVm)
        return;

    dcMode (callVm, DC_CALL_C_DEFAULT);
    dcReset (callVm);

    if (sym)
        proto = sym->proto;

    for (i = 0; i < MAX_PARMS; i++)
    {
        arg = 0;

        if (i < args->num_args)
            arg = args->args[i];

        if (proto && i < proto->numArgs)
        {
            argsize = proto->argsize[i];
        }
        else
        {
            argsize = args->argsize[i];
        }

        switch (argsize)
        {
        case CHAR_MASK:
        case UCHAR_MASK:
            dcArgChar (callVm, (DCchar)arg);
            break;
        case SHORT_MASK:
            dcArgShort (callVm, (DCshort)arg);
            break;
        case ULL_MASK:
            dcArgLongLong (callVm, (DClonglong)arg);
            break;
        default:
            dcArgInt (callVm, (DCint)arg);
            break;
        }
    }

    if (cash_set_argc)
        cash_set_argc (args->num_args);

    if (proto)
    {
        switch (proto->rcsize)
        {
        case CHAR_MASK:
        case UCHAR_MASK:
            res->val = dcCallChar (callVm, (DCpointer)func);
            break;
        case SHORT_MASK:
            res->val = dcCallShort (callVm, (DCpointer)func);
            break;
        case ULL_MASK:
            res->val = dcCallLongLong (callVm, (DCpointer)func);
            break;
        default:
            res->val = dcCallInt (callVm, (DCpointer)func);
            break;
        }
        res->mask = proto->rcsize;
    }
    else
    {
        res->val = dcCallInt (callVm, (DCpointer)func);
        res->mask = UINT_MASK;
    }

    dcFree (callVm);
#else
    cash_fp_pi *fp = (cash_fp_pi*)func;
    cash_fp_2pi *fp2 = (cash_fp_2pi*)func;
    cash_fp_3pi *fp3 = (cash_fp_3pi*)func;
    cash_fp_4pi *fp4 = (cash_fp_4pi*)func;
    cash_fp_5pi *fp5 = (cash_fp_5pi*)func;

    if (cash_set_argc)
        cash_set_argc (args->num_args);

    int num_ptr;
    for (num_ptr = 0; num_ptr <= 5; num_ptr++)
    {
        if (args->argsize[num_ptr] != PTR_MASK)
        break;
    }

    switch (num_ptr)
    {
    case 1:
        res->val = fp ((void*)args->args[0],
            (int)args->args[1],
            (int)args->args[2],
            (int)args->args[3],
            (int)args->args[4],
            (int)args->args[5],
            (int)args->args[6],
            (int)args->args[7],
            (int)args->args[8],
            (int)args->args[9],
            (int)args->args[10],
            (int)args->args[11],
            (int)args->args[12],
            (int)args->args[13],
            (int)args->args[14],
            (int)args->args[15],
            (int)args->args[16],
            (int)args->args[17],
            (int)args->args[18],
            (int)args->args[19]
            );
        break;
    case 2:
        res->val = fp2((void*)args->args[0],
            (void*)args->args[1],
            (int)args->args[2],
            (int)args->args[3],
            (int)args->args[4],
            (int)args->args[5],
            (int)args->args[6],
            (int)args->args[7],
            (int)args->args[8],
            (int)args->args[9],
            (int)args->args[10],
            (int)args->args[11],
            (int)args->args[12],
            (int)args->args[13],
            (int)args->args[14],
            (int)args->args[15],
            (int)args->args[16],
            (int)args->args[17],
            (int)args->args[18],
            (int)args->args[19]
            );
        break;
    case 3:
      res->val = fp3((void*)args->args[0],
            (void*)args->args[1],
            (void*)args->args[2],
            (int)args->args[3],
            (int)args->args[4],
            (int)args->args[5],
            (int)args->args[6],
            (int)args->args[7],
            (int)args->args[8],
            (int)args->args[9],
            (int)args->args[10],
            (int)args->args[11],
            (int)args->args[12],
            (int)args->args[13],
            (int)args->args[14],
            (int)args->args[15],
            (int)args->args[16],
            (int)args->args[17],
            (int)args->args[18],
            (int)args->args[19]
            );
        break;
    case 4:
      res->val = fp4((void*)args->args[0],
            (void*)args->args[1],
            (void*)args->args[2],
            (void*)args->args[3],
            (int)args->args[4],
            (int)args->args[5],
            (int)args->args[6],
            (int)args->args[7],
            (int)args->args[8],
            (int)args->args[9],
            (int)args->args[10],
            (int)args->args[11],
            (int)args->args[12],
            (int)args->args[13],
            (int)args->args[14],
            (int)args->args[15],
            (int)args->args[16],
            (int)args->args[17],
            (int)args->args[18],
            (int)args->args[19]
            );
        break;
    case 5:
        res->val = fp5((void*)args->args[0],
            (void*)args->args[1],
            (void*)args->args[2],
            (void*)args->args[3],
            (void*)args->args[4],
            (int)args->args[5],
            (int)args->args[6],
            (int)args->args[7],
            (int)args->args[8],
            (int)args->args[9],
            (int)args->args[10],
            (int)args->args[11],
            (int)args->args[12],
            (int)args->args[13],
            (int)args->args[14],
            (int)args->args[15],
            (int)args->args[16],
            (int)args->args[17],
            (int)args->args[18],
            (int)args->args[19]
            );
        break;
    default:
        res->val = func((int)args->args[0],
            (int)args->args[1],
            (int)args->args[2],
            (int)args->args[3],
            (int)args->args[4],
            (int)args->args[5],
            (int)args->args[6],
            (int)args->args[7],
            (int)args->args[8],
            (int)args->args[9],
            (int)args->args[10],
            (int)args->args[11],
            (int)args->args[12],
            (int)args->args[13],
            (int)args->args[14],
            (int)args->args[15],
            (int)args->args[16],
            (int)args->args[17],
            (int)args->args[18],
            (int)args->args[19]
            );
        break;
    }

    res->mask = UINT_MASK;
#endif
    res->type = T_INT;
    res->sym = NULL;
    res->lv = 0;
        
    /* if call has forked, don't let child process resume parsing */
    if (getpid() != scan_ctx->pid)
    {
        exit(0);
    }
}


static void get_sym (VALUE *val, SYMTAB_ENTRY *sym)
{
    bzero (val, sizeof(*val));
    
    switch (sym->mask)
    {
        case CHAR_MASK:
        case UCHAR_MASK:
            val->val =  (uint64_t)(*(char*)cash_adrs_remap((intptr_t)sym->ptr));
            break;
        case SHORT_MASK:
              val->val =  (uint64_t)(*(short*)cash_adrs_remap((intptr_t)sym->ptr));
              break;
        case ULL_MASK:
              val->val =  (uint64_t)(*(uint64_t*)cash_adrs_remap((intptr_t)sym->ptr));
              break;
        default:
              val->val =  (uint64_t)(*(int*)cash_adrs_remap((intptr_t)sym->ptr));
              break;
    }
    val->type = sym->type;
    val->mask = sym->mask;
    val->sym = sym;
    val->lv = (uint64_t)((uintptr_t)sym->ptr);


}

static SYMTAB_ENTRY *new_sym (char *name, scantype type, VALUE *val, uint64_t mask)
{
    SYMTAB_ENTRY    *entry;

    if (mask == 0)
        mask = UINT_MASK;

    entry = cash_symtab_add (name, type, 0, NULL, mask);

    if (entry == NULL)
        return NULL;

    val->type = type;
    val->val = 0;
    val->mask = mask;
    val->sym = entry;
    val->lv = (intptr_t)entry->ptr;

    return entry;
}
 
static SYMTAB_ENTRY *new_libdl_sym (char *name, scantype type, VALUE *val, uint64_t mask)
{
#ifndef USE_LIBDL
    return NULL;
#else
    SYMTAB_ENTRY    *entry;
    intptr_t adrs;
 
    if (mask == 0)
        mask = UINT_MASK;
 
    adrs = (intptr_t)dlsym (NULL, name);
    if (!adrs)
        return NULL;
 
    entry = cash_symtab_add (name, type, adrs, NULL, mask);
 
    if (entry == NULL)
        return NULL;
 
    val->type = type;
    val->val = adrs;
    val->mask = mask;
    val->sym = entry;
    val->lv = (intptr_t)entry->ptr;
 
    return entry;
#endif
}
 
static int assign_op (scan_ctx_t *scan_ctx, VALUE *vl, VALUE *vr, int op)
{
    if (vl->lv == 0)
    {
        CTX_PRINTF ("bad lvalue\n");
        return 0;
    }

    switch (vl->mask)
    {
    case CHAR_MASK:
        switch (op)
        {
        case ADDA:  *(signed char*)cash_adrs_remap(vl->lv) += vr->val; break;
        case SUBA:  *(signed char*)cash_adrs_remap(vl->lv) -= vr->val; break;
        case MULA:  *(signed char*)cash_adrs_remap(vl->lv) *= vr->val; break;
        case DIVA:  *(signed char*)cash_adrs_remap(vl->lv) /= vr->val; break;
        case ORA:   *(signed char*)cash_adrs_remap(vl->lv) |= vr->val; break;
        case ANDA:  *(signed char*)cash_adrs_remap(vl->lv) &= vr->val; break;
        case XORA:  *(signed char*)cash_adrs_remap(vl->lv) ^= vr->val; break;
        case MODA:  *(signed char*)cash_adrs_remap(vl->lv) %= vr->val; break;
        case SHLA:  *(signed char*)cash_adrs_remap(vl->lv) <<= vr->val; break;
        case SHRA:  *(signed char*)cash_adrs_remap(vl->lv) >>= vr->val; break;
        case AI:    *(signed char*)cash_adrs_remap(vl->lv) += 1; break;
        case AD:    *(signed char*)cash_adrs_remap(vl->lv) -= 1; break;
        default:    return 0;
        }
        break;
    case UCHAR_MASK:
        switch (op)
        {
        case ADDA:  *(unsigned char*)cash_adrs_remap(vl->lv) += vr->val; break;
        case SUBA:  *(unsigned char*)cash_adrs_remap(vl->lv) -= vr->val; break;
        case MULA:  *(unsigned char*)cash_adrs_remap(vl->lv) *= vr->val; break;
        case DIVA:  *(unsigned char*)cash_adrs_remap(vl->lv) /= vr->val; break;
        case ORA:   *(unsigned char*)cash_adrs_remap(vl->lv) |= vr->val; break;
        case ANDA:  *(unsigned char*)cash_adrs_remap(vl->lv) &= vr->val; break;
        case XORA:  *(unsigned char*)cash_adrs_remap(vl->lv) ^= vr->val; break;
        case MODA:  *(unsigned char*)cash_adrs_remap(vl->lv) %= vr->val; break;
        case SHLA:  *(unsigned char*)cash_adrs_remap(vl->lv) <<= vr->val; break;
        case SHRA:  *(unsigned char*)cash_adrs_remap(vl->lv) >>= vr->val; break;
        case AI:    *(unsigned char*)cash_adrs_remap(vl->lv) += 1; break;
        case AD:    *(unsigned char*)cash_adrs_remap(vl->lv) -= 1; break;
        default:    return 0;
        }
        break;
    case SHORT_MASK:
        switch (op)
        {
        case ADDA:  *(unsigned short*)cash_adrs_remap(vl->lv) += vr->val; break;
        case SUBA:  *(unsigned short*)cash_adrs_remap(vl->lv) -= vr->val; break;
        case MULA:  *(unsigned short*)cash_adrs_remap(vl->lv) *= vr->val; break;
        case DIVA:  *(unsigned short*)cash_adrs_remap(vl->lv) /= vr->val; break;
        case ORA:   *(unsigned short*)cash_adrs_remap(vl->lv) |= vr->val; break;
        case ANDA:  *(unsigned short*)cash_adrs_remap(vl->lv) &= vr->val; break;
        case XORA:  *(unsigned short*)cash_adrs_remap(vl->lv) ^= vr->val; break;
        case MODA:  *(unsigned short*)cash_adrs_remap(vl->lv) %= vr->val; break;
        case SHLA:  *(unsigned short*)cash_adrs_remap(vl->lv) <<= vr->val; break;
        case SHRA:  *(unsigned short*)cash_adrs_remap(vl->lv) >>= vr->val; break;
        case AI:    *(unsigned short*)cash_adrs_remap(vl->lv) += 1; break;
        case AD:    *(unsigned short*)cash_adrs_remap(vl->lv) -= 1; break;
        default:    return 0;
        }
        break;
    case ULL_MASK:
        switch (op)
        {
        case ADDA:  *(uint64_t*)cash_adrs_remap(vl->lv) += vr->val; break;
        case SUBA:  *(uint64_t*)cash_adrs_remap(vl->lv) -= vr->val; break;
        case MULA:  *(uint64_t*)cash_adrs_remap(vl->lv) *= vr->val; break;
        case DIVA:  *(uint64_t*)cash_adrs_remap(vl->lv) /= vr->val; break;
        case ORA:   *(uint64_t*)cash_adrs_remap(vl->lv) |= vr->val; break;
        case ANDA:  *(uint64_t*)cash_adrs_remap(vl->lv) &= vr->val; break;
        case XORA:  *(uint64_t*)cash_adrs_remap(vl->lv) ^= vr->val; break;
        case MODA:  *(uint64_t*)cash_adrs_remap(vl->lv) %= vr->val; break;
        case SHLA:  *(uint64_t*)cash_adrs_remap(vl->lv) <<= vr->val; break;
        case SHRA:  *(uint64_t*)cash_adrs_remap(vl->lv) >>= vr->val; break;
        case AI:    *(uint64_t*)cash_adrs_remap(vl->lv) += 1; break;
        case AD:    *(uint64_t*)cash_adrs_remap(vl->lv) -= 1; break;
        default:    return 0;
        }
        break;
    default:
        switch (op)
        {
        case ADDA:  *(unsigned int*)cash_adrs_remap(vl->lv) += vr->val; break;
        case SUBA:  *(unsigned int*)cash_adrs_remap(vl->lv) -= vr->val; break;
        case MULA:  *(unsigned int*)cash_adrs_remap(vl->lv) *= vr->val; break;
        case DIVA:  *(unsigned int*)cash_adrs_remap(vl->lv) /= vr->val; break;
        case ORA:   *(unsigned int*)cash_adrs_remap(vl->lv) |= vr->val; break;
        case ANDA:  *(unsigned int*)cash_adrs_remap(vl->lv) &= vr->val; break;
        case XORA:  *(unsigned int*)cash_adrs_remap(vl->lv) ^= vr->val; break;
        case MODA:  *(unsigned int*)cash_adrs_remap(vl->lv) %= vr->val; break;
        case SHLA:  *(unsigned int*)cash_adrs_remap(vl->lv) <<= vr->val; break;
        case SHRA:  *(unsigned int*)cash_adrs_remap(vl->lv) >>= vr->val; break;
        case AI:    *(unsigned int*)cash_adrs_remap(vl->lv) += 1; break;
        case AD:    *(unsigned int*)cash_adrs_remap(vl->lv) -= 1; break;
        default:    return 0;
        }
        break;
    }

    vl->val = *(uint64_t*)cash_adrs_remap(vl->lv);

    return 1;
}

void yyerror( scan_ctx_t *scan_ctx, const char *msg, ... )
{
 
    if (scan_ctx->anon_sym != NULL)
    {
        CTX_PRINTF ("undefined symbol: %s\n", scan_ctx->anon_sym);
        cash_sym_delete (scan_ctx->anon_sym);
        scan_ctx->anon_sym = NULL;
        return;
    }

    CTX_PRINTF ("%s\n", msg);

    return;
}


int yywrap (void)
{
    return 1;
}

static void init_proto_args (VALUE *pa)
{
#ifndef DYNCALL
    static int dcWarn = 0;

    if (!dcWarn)
    {
        CTX_PRINTF ("WARNING: Function prototypes not supported, requires dynlib\n");
        dcWarn = 1;
    }
    pa->proto = NULL;
    return;
#else
    pa->proto = malloc (sizeof(*pa->proto));
    pa->proto->numArgs = 0;
#endif
}

static void add_pa_type (VALUE *pa, VALUE *t)
{
    if (!pa->proto)
        return;

    if (pa->proto->numArgs >= MAX_PARMS)
       return;

    pa->proto->argsize[pa->proto->numArgs++] = t->mask;
}

static int pushScope (scan_ctx_t *scan_ctx, int activate)
{
    int wasActive = scan_ctx->scope[scan_ctx->scope_id].active;
    
    if (scan_ctx->scope_id >= MAX_SCOPE-1)
    {
        yyerror (scan_ctx, "Nesting too high (max. %d)", MAX_SCOPE);
        return 1;
    }
    
    scan_ctx->scope_id++;
    
    scan_ctx->scope[scan_ctx->scope_id].active = wasActive && activate;

    return 0;
}

static int popScope (scan_ctx_t *scan_ctx)
{
    scan_ctx->scope_id--;
    if (scan_ctx->scope_id < 0)
    {
        yyerror (scan_ctx, "Unbalanced Scope");
        return 1;
    }   
    return 0;
}

