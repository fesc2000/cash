/* A Bison parser, made by GNU Bison 3.0.4.  */

/* Bison interface for Yacc-like parsers in C

   Copyright (C) 1984, 1989-1990, 2000-2015 Free Software Foundation, Inc.

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.  */

/* As a special exception, you may create a larger work that contains
   part or all of the Bison parser skeleton and distribute that work
   under terms of your choice, so long as that work isn't itself a
   parser generator using the skeleton or a modified version thereof
   as a parser skeleton.  Alternatively, if you modify or redistribute
   the parser skeleton itself, you may (at your option) remove this
   special exception, which will cause the skeleton and the resulting
   Bison output files to be licensed under the GNU General Public
   License without this special exception.

   This special exception was added by the Free Software Foundation in
   version 2.2 of Bison.  */

#ifndef YY_YY_CASH_TAB_H_INCLUDED
# define YY_YY_CASH_TAB_H_INCLUDED
/* Debug traces.  */
#ifndef YYDEBUG
# define YYDEBUG 0
#endif
#if YYDEBUG
extern int yydebug;
#endif

/* Token type.  */
#ifndef YYTOKENTYPE
# define YYTOKENTYPE
  enum yytokentype
  {
    STRING = 258,
    NUMBER = 259,
    CHAR = 260,
    ANON_ID = 261,
    LIBDL_ID = 262,
    FUNC_ID = 263,
    VAR_ID = 264,
    AND = 265,
    RSHIFT = 266,
    LSHIFT = 267,
    KW_VOID = 268,
    KW_CHAR = 269,
    KW_UINT8 = 270,
    KW_UINT16 = 271,
    KW_UINT32 = 272,
    KW_UINT64 = 273,
    KW_LONG = 274,
    MULA = 275,
    DIVA = 276,
    MODA = 277,
    ADDA = 278,
    SUBA = 279,
    SHLA = 280,
    SHRA = 281,
    ANDA = 282,
    ORA = 283,
    XORA = 284,
    OR = 285,
    GE = 286,
    LE = 287,
    EQ = 288,
    NE = 289,
    AI = 290,
    AD = 291,
    KW_SIZEOF = 292,
    DOTDOT = 293,
    ITER_COND = 294,
    ITER_START = 295,
    ITER_END = 296,
    UNARY = 297
  };
#endif

/* Value type.  */
#if ! defined YYSTYPE && ! defined YYSTYPE_IS_DECLARED
typedef int YYSTYPE;
# define YYSTYPE_IS_TRIVIAL 1
# define YYSTYPE_IS_DECLARED 1
#endif



int yyparse (scan_ctx_t *scan_ctx);

#endif /* !YY_YY_CASH_TAB_H_INCLUDED  */
