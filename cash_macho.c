/* -*- Mode: C; indent-tabs-mode: nil; c-basic-offset: 4 -*- */

#include <stdio.h>
#include <string.h>
#include <stdint.h>
#include <stdbool.h>
#include <mach-o/loader.h>
#include <mach-o/nlist.h>
#include <sys/mman.h>

#include "cash.h"

int
cash_load_symtab_from_file (char *file, int use_libdl, void *handle)
{
    char *content;
    size_t length;
    size_t offset;
    uint32_t i, n_commands;
    uint32_t section_index = 0;
    uint32_t text_section_index = 0;
    uint32_t data_section_index = 0;
    uint32_t bss_section_index = 0;
    uint32_t common_section_index = 0;

    content = mapfile (file, &length);

    if (!content)
    {
        fprintf (stderr, "Failed to read symbols from %s\n", file);
	      return -1;
    }

    {
        struct mach_header_64 *header;

        header = (struct mach_header_64 *)content;
        if (header->magic != MH_MAGIC_64){
            printf("not Mach-O 64\n");
            return -1;
        }

        offset = sizeof(*header);
        n_commands = header->ncmds;
    }

    for (i = 0; i < n_commands; i++) {
        struct load_command *load;

        load = (struct load_command *)(content + offset);
        switch (load->cmd) {
        case LC_SEGMENT_64:
        {
            struct segment_command_64 *segment;
            struct section_64 *section;
            int j;

            segment = (struct segment_command_64 *)(content + offset);
            if (strcmp(segment->segname, "__TEXT") && strcmp(segment->segname, "__DATA")) {
                section_index += segment->nsects;
                break;
            }

            section = (struct section_64 *)(content + offset + sizeof(*segment));
            for (j = 0; j < segment->nsects; j++, section++) {
//printf (" %s\n", section->sectname);
                section_index++;
                if (!strcmp(section->sectname, "__data"))
                    data_section_index = section_index;
                if (!strcmp(section->sectname, "__text"))
                    text_section_index = section_index;
                if (!strcmp(section->sectname, "__bss"))
                    bss_section_index = section_index;
                if (!strcmp(section->sectname, "__common"))
                    common_section_index = section_index;
            }
            break;
        }
        case LC_SYMTAB:
        {
            struct symtab_command *table;
            struct nlist_64 *symbol;
            char *string_table;
            int j;

            table = (struct symtab_command *)(content + offset);
            symbol = (struct nlist_64 *)(content + table->symoff);
            string_table = content + table->stroff;
            for (j = 0; j < table->nsyms; j++, symbol++) {
                bool defined_in_section = false;

                if ((symbol->n_type & N_TYPE) == N_SECT)
                    defined_in_section = true;

                if (defined_in_section &&
                    symbol->n_sect == text_section_index &&
                    symbol->n_type & N_EXT) {
                    char *name;
                    int32_t string_offset;

                    string_offset = symbol->n_un.n_strx;
                    name = string_table + string_offset + 1;
                    if (!cash_is_helpstr(name))
                      dl_func (name, cash_dl_handle(handle));
                }
                if (defined_in_section &&
                    symbol->n_sect == data_section_index) {
                    char *name;
                    int32_t string_offset;

                    string_offset = symbol->n_un.n_strx;
                    name = string_table + string_offset + 1;

                    if (!cash_is_helpstr(name))
                      dl_int (name, cash_dl_handle(handle));
                }
                if (defined_in_section &&
                    symbol->n_sect == bss_section_index) {
                    char *name;
                    int32_t string_offset;

                    string_offset = symbol->n_un.n_strx;
                    name = string_table + string_offset + 1;

                    if (!cash_is_helpstr(name))
                      dl_int (name, cash_dl_handle(handle));
                }
                if (defined_in_section &&
                    symbol->n_sect == common_section_index) {
                    char *name;
                    int32_t string_offset;

                    string_offset = symbol->n_un.n_strx;
                    name = string_table + string_offset + 1;

                    if (!cash_is_helpstr(name))
                      dl_int (name, cash_dl_handle(handle));
                }
            }
            break;
        }
        default:
            break;
        }
        offset += load->cmdsize;
    }

    if (content)
	munmap (content, length);

    return 0;
}
