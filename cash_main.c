/*
Copyright 2002,2020 Felix Schmidt

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

#include <sys/types.h>
#include <stdio.h>
#include <stdlib.h>
#include <getopt.h>
#include <fcntl.h>
#include <unistd.h>
#include <string.h>
#include <signal.h>
#include <setjmp.h>
#include <sys/time.h>
#include <sys/types.h>
#if (HAVE_BACKTRACE!=0)
#include <execinfo.h>
#endif
#ifdef USE_LIBDL
#include <dlfcn.h>
#endif

#include <dynload.h>

#ifdef HAVE_READLINE
#include <readline/readline.h>
#include <readline/history.h>
#endif

#include "cash.h"

typedef unsigned int (*fp) (void);


#if (HAVE_BACKTRACE!=0)
#define BACKTRACE                                       \
       {                                                \
           int j, nptrs;                                \
           void *buffer[1000];                          \
            char **strings;                             \
           nptrs = backtrace(buffer, 1000);             \
           strings = backtrace_symbols(buffer, nptrs);  \
           if (strings == NULL) {                       \
               perror("backtrace_symbols");             \
           } else {                                     \
           printf ("Backtrace:\n");                     \
           for (j = 0; j < nptrs; j++)                  \
               printf("|  %s\n", strings[j]);           \
           free(strings);                               \
            }                                           \
       }
#else
#define BACKTRACE
#endif


#include "cash.h"

extern FILE *yyin;

#define EOS 0

static sigjmp_buf excenv;
static sigjmp_buf probe_excenv;
static struct sigaction segv;
int __mem_probe;
static int do_memprobe = 0;
static pid_t main_pid = 0;

static void segv_handler(int sig, siginfo_t *si, void *arg)
{
    if (getpid() != main_pid)
    {
      exit(1);
    }

    if (do_memprobe)
    {
        siglongjmp (probe_excenv, 1);
    }

    if (si->si_signo == SIGSEGV)
    {
        printf ("***** Segmentation Violation accessing address %p (physical %p)\n",
                (void*)si->si_addr,
                (void*)cash_virt_to_io ((intptr_t)si->si_addr)
                );
    }
    else if (si->si_signo == SIGBUS)
    {
        printf ("***** BUS ERROR accessing address %p (physical %p)\n",
                (void*)si->si_addr,
                (void*)cash_virt_to_io ((intptr_t)si->si_addr)
                );
    }
    else if (si->si_signo == SIGINT)
    {
        printf ("***** aborted by SIGINT/CTRL-C\n",
                (void*)si->si_addr,
                (void*)cash_virt_to_io ((intptr_t)si->si_addr)
                );
    }
    else
    {
        printf ("Received signal %d\n", si->si_signo);
        return;
    }

    BACKTRACE


    siglongjmp (excenv, 1);

}

void cash_backtrace(void)
{
    BACKTRACE
}

int simple_strtoul (char *c, void *p, int base)
{
    return (int)strtoul (c, p, base);
}

uint64_t simple_strtoull (char *c, void *p, int base)
{
    return strtoull (c, p, base);
}

#define MAX_LINE    1024


char* cash_getexename(char* buf, size_t size)
{
#ifdef __APPLE__
    uint32_t length = (uint32_t)size;
    if (0 != _NSGetExecutablePath(buf, &length))
    {
        buf = NULL;
    }
#else
    char linkname[64]; /* /proc/<pid>/exe */
    int ret;

    if (snprintf(linkname, sizeof(linkname), "/proc/%i/exe", main_pid) < 0)
    {
        return NULL;
    }


    /* Now read the symbolic link */
    ret = readlink(linkname, buf, size);

    /* In case of an error, leave the handling up to the caller */
    if (ret == -1)
        return NULL;

    /* Report insufficient buffer size */
    if (ret >= size)
    {
        return NULL;
    }

    /* Ensure proper NUL termination */
    buf[ret] = 0;
#endif
    return buf;
}

#ifdef HAVE_READLINE
static char *history_fname = NULL;

static void save_history (char *line)
{
    if (!history_fname)
    {
        history_fname = calloc (strlen(getenv("HOME")) + 20, 1);
        sprintf (history_fname, "%s/.cash-history", getenv("HOME"));
    }

    FILE *sv = fopen (history_fname, "a");

    if (!sv)
        return;

    fprintf (sv, "%s\n", line);

    fclose (sv);
}

static void load_history ()
{
    FILE *sv;
    char line[250];
    int i;
    int num = 0;

    if (!history_fname)
    {
        history_fname = calloc (strlen(getenv("HOME")) + 20, 1);
        sprintf (history_fname, "%s/.cash-history", getenv("HOME"));
    }

    sv = fopen (history_fname, "r");

    if (!sv)
        return;

    i = 0;
    while (!feof(sv))
    {
        int c = fgetc(sv);

        if (c == EOF)
            break;
        
        if (c == '\n')
        {
            line[i] = 0;

            if (strlen(line) > 0)
            {
                add_history(strdup(line));
                num++;
            }
            i = 0;
            continue;
        }

        if ((c >= 0x20) && (c < 0x7f))
        {
            if (i < sizeof(line) - 3)
                line[i++] = (char)(c & 0xff);
        }
        else
        {
            i = 0;
        }
    }
    fclose (sv);
}

int cash_run_tty (VALUE *result)
{
    char *line, *prev_line;
    int rc = 1;
    char *prompt = "CASH> ";

    prompt = cash_prompt();

    rl_completion_entry_function = symtab_complete;

    prev_line = NULL;

    load_history();
    
    while (1)
    {
        line = readline (prompt);

        if (line == NULL)
            return rc;
        
        if (-1 == sigaction (SIGSEGV, &segv, NULL))
        {
            perror ("sigaction");
        }
        if (-1 == sigaction (SIGBUS, &segv, NULL))
        {
            perror ("sigaction");
        }
        if (-1 == sigaction (SIGINT, &segv, NULL))
        {
            perror ("sigaction");
        }

        if (sigsetjmp (excenv, 1) == 0)
        {
            rc = cash_run_expr_s (line, 1, result);
        }
        else if (getpid() == main_pid)
        {
            printf ("** Skipping rest of expression\n");
        }

        signal (SIGSEGV, SIG_DFL);
        signal (SIGBUS, SIG_DFL);
        signal (SIGINT, SIG_DFL);

        if (getpid() != main_pid)
        {
            /* expression has forked, just terminate here */
            exit(0);
        }

        /* add line to history unless it's empty or the same as the
         * previous one
         */
        if (((prev_line != NULL) && !strcmp (prev_line, line)) ||
            (strlen(line) == 0) ||
            !strcmp (line, "\n"))
        {
            free (line);
        }
        else
        {
            add_history (line);
            save_history (line);
            prev_line = line;
        }
    }
}
#endif

int cash_mem_probe (char *adrs)
{
    int rc;

    do_memprobe = 1;

    if (sigsetjmp (probe_excenv, 1) == 0)
    {
        __mem_probe = *adrs;
        rc = 1;
    }
    else
    {
        rc = 0;
    }

    do_memprobe = 0;

    return rc;
}


int cash_run_file (int fd, VALUE *result)
{
    int off = 0;
    int rc = 1;
    int l;
    char    line[MAX_LINE];
    unsigned char c;

#ifdef HAVE_READLINE
    if (isatty (fd))
        return cash_run_tty (result);
#endif

    while (1)
    {
        l = read (fd, &c, 1);

        if (l < 0)
        {
            perror ("read");
            return rc;
        }

        line[off++] = c;

        if ((c < 0x20) || (c >= 0x80) || (l == 0))
        {
            line[off-1] = 0;

            if (strlen(line) > 0)
            {
                if (-1 == sigaction (SIGSEGV, &segv, NULL))
                {
                    perror ("sigaction");
                }
                if (-1 == sigaction (SIGBUS, &segv, NULL))
                {
                    perror ("sigaction");
                }


                if (sigsetjmp (excenv, 1) == 0)
                {
                    rc = cash_run_expr_s (line, 1, result);
                }
                else if (getpid() == main_pid)
                {
                    printf ("** Skipping rest of expression\n");
                }

                if (getpid() != main_pid)
                {
                    /* expression has forked and returned, exit here */
                    exit(0);
                }
            }
            off = 0;
        }

        if (l == 0)
            return rc;
    }
}

int cash_script (char *fname)
{
    int fd, rc;
    
    if (!fname)
        return -1;
    
    fd = open (fname, O_RDONLY);
    
    if (fd == -1)
    {
        perror (fname);
        return -1;
    }
    
    rc = cash_run_file (fd, NULL);
    
    close (fd);
    
    return rc;
}

void dfl_segv_handler()
{
    memset ((void*)&segv, 0, sizeof(segv));
    segv.sa_handler = NULL;
    segv.sa_sigaction = segv_handler;
    segv.sa_flags = SA_SIGINFO;
    bzero((char*)&segv.sa_mask, sizeof(segv.sa_mask));

    if (-1 == sigaction (SIGSEGV, &segv, NULL))
    {
        perror ("sigaction");
    }
    if (-1 == sigaction (SIGBUS, &segv, NULL))
    {
        perror ("sigaction");
    }
}

int cash_segv_catch (void)
{
    return (sigsetjmp (excenv, 1) == 0);
}

void cash_proto_init(void)
{
#if defined(EXT_PROTO) && !defined(CASH_NOEXT)
    EXT_PROTO
#endif

    DECL_PROTO (printf,  PA64, PA64);
    DECL_PROTO (malloc,  PA64, PA64);
    DECL_PROTO (open,    PA32, PA64, PA32);
    DECL_PROTO (close,   PA32, PA32);
    DECL_PROTO (read,    PA32, PA64, PA32);
    DECL_PROTO (write,   PA32, PA64, PA32);
    DECL_PROTO (memcmp,  PA64, PA64, PA64);
    DECL_PROTO (memcpy,  PA64, PA64, PA64);
    DECL_PROTO (memset,  PA64, PA64, PA32, PA64);
    DECL_PROTO (cash_md, PA32, PA64, PA32, PA32);
    DECL_PROTO (help,    PA32, PA64);
    DECL_PROTO (dlLoadLibrary,    PA64, PA64);
   
#ifdef USE_LIBDL
    DECL_PROTO (dlopen,  PA64, PA64);
    DECL_PROTO (dlsym,   PA64, PA64);
    DECL_PROTO (dl_int,  PA64, PA64);
    DECL_PROTO (dl_func, PA64, PA64);
#endif

#ifdef USE_LIBELF
    DECL_PROTO (cash_load_symtab_from_file, PA32, PA64, PA32, PA64);
#endif

#if defined(EXT_INIT) && !defined(CASH_NOEXT)
    EXT_INIT
#endif
}

void cash_init(char *fname, char *libname)
{
    SYMTAB_ENTRY *toolSymbols;
    fp  tsFct;
    static int cash_initialized = 0;

    if (cash_initialized)
        return;

    dfl_segv_handler();

    cash_do_io_map();
    cash_expr_initialize();
    cash_proto_init();
    cash_net_init();


#ifdef USE_LIBELF
    cash_symtab_add ("cash_load_symtab_from_file", T_FUNC, 
        (uint64_t)(uintptr_t)cash_load_symtab_from_file, NULL, 0);

    {
        char exename[10000];

        if (cash_getexename (exename, sizeof(exename)))
        {
            cash_dl_set_dflfile (exename);
            cash_load_symtab_from_file (exename, 1, NULL);
        }
        else if (fname)
        {
            cash_dl_set_dflfile (fname);
            cash_load_symtab_from_file (fname, 1, NULL);
        }


        if (libname)
            cash_load_symtab_from_file (libname, 1, dlopen(libname, RTLD_LAZY));
    }
#endif

    toolSymbols = cash_symtab_find ("toolSymbols", T_FUNC);

    if (toolSymbols)
    {
        tsFct = (fp)((uintptr_t)cash_symval_get(toolSymbols));
        tsFct ();
    }

}

int 
cash_main (int argc, char **argv, char *initScript, VALUE *rp)
{
    int fd;
    int i;
    VALUE result;
    main_pid = getpid();

    cash_init (argv[0], NULL);

    if (initScript)
    {
        fd = open (initScript, O_RDONLY);

        if (fd == -1)
        {
            perror (argv[0]);
            exit (1);
        }

        cash_run_file (fd, &result);

        close (fd);
    }

    for (i = 1; (i < argc) && (getpid() == main_pid); i++)
    {
        if (!argv[i])
            break;
            
        if (!strcmp (argv[i], "-h"))
        {
            printf ("Usage: %s [-h] [-e <expr>] [-E expr] [<file1>] [<file2>] ...\n"
                    "  <file> is either\n"
                    "   a file name\n"
                    "   - to use stdin/out\n"
                    "   @<port>[<net-args>] to open a socket at <port>, with <net-args>\n"
                    "     :tty      To redirect stdin/out to the socket after connection\n"
                    "     :ttyline  To redirect stdin/out while evaluating an expression\n"
                    "     :spawn    To spawn the network handling in a separate thread\n"
                    "     By default, stdin/out is left on the calling terminal, only the\n"
                    "     result of the expression is sent to the socket\n\n"
                    "     To connect to the socket, use telnet or socat (socat - tcp:host:port)\n"
                    "     Example: cash @10000:tty\n"
                    "              cash @10000:ttyline:spawn -\n"
                    , argv[0]);
            exit (0);
        }
        else if (!strcmp (argv[i], "-e") && ((i+1) < argc))
        {
            i++;
            if (sigsetjmp (excenv, 1) == 0)
            {
                cash_run_expr_s (argv[i], 1, &result);
            }
            else if (getpid() == main_pid)
            {
                printf ("** Skipping rest of expression\n");
            }
        }
        else if (!strcmp (argv[i], "-E") && ((i+1) < argc))
        {
            i++;
            if (sigsetjmp (excenv, 1) == 0)
            {
                cash_run_expr_s (argv[i], 1, &result);
            }
            else if (getpid() == main_pid)
            {
                printf ("** Skipping rest of expression\n");
            }
        }
        else if (!strcmp (argv[i], "-"))
        {
            cash_run_file (STDIN_FILENO, &result);
        }
        else if (argv[i][0] == '@')
        {
            cash_netserver_args_t *args = calloc (sizeof(cash_netserver_args_t), 1);
            int sp = 0;

            args->port = atoi (&argv[i][1]);

            if (args->port != 0)
            {
                args->mode = ns_mode_line;

                if (strstr (&argv[i][1], ":redir"))
                {
                    args->mode = ns_mode_redir;
                }
                else if (strstr (&argv[i][1], ":ttyline"))
                {
                    args->mode = ns_mode_line_redir;
                }
                else if (strstr (&argv[i][1], ":tty"))
                {
                    args->mode = ns_mode_tty;
                }

                if (strstr (&argv[i][1], ":spawn"))
                    sp = 1;

                if (sp)
                {
                    cash_spawn ((void*)cash_netserver, (void*)args, "netserver");
                }
                else
                {
                    cash_netserver (args);
                }
            }
        }
        else
        {
            fd = open (argv[i], O_RDONLY);

            if (fd == -1)
            {
                perror (argv[i]);
                exit (1);
            }

            cash_run_file (fd, &result);

            close (fd);
        }
    }

    if (rp)
        *rp = result;

    return result.val;;
}

uint64_t cash_time64(void)
{
    struct timeval tv;
            
    gettimeofday (&tv, NULL);
                    
    return ((uint64_t)tv.tv_sec * 1000000ULL + (uint64_t)tv.tv_usec);
}
