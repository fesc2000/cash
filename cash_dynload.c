
#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#include "cash.h"
#include "dynload.h"

static int (*appMain)(int, char **);
static int app_argc = 0;
static char **app_argv = NULL;


void cash_dynload(int argc, char**argv)
{
  assert(argc > 0);

  const char* appl = argv[0];

  DLSyms* syms = dlSymsInit(appl);

  if (!syms)
  {
    fprintf(stderr, "dlSymsInit failed\n");
  }

  for (int i = 0; i < dlSymsCount(syms); i++)
  {
    const char *name = dlSymsName(syms, i);

    if (!name)
    {
      break;
    }

    printf ("%d %s\n", i, name);
  }

  dlSymsCleanup(syms);


  DLLib * app_handle = dlLoadLibrary (appl);

  if (!app_handle)
  {
    fprintf(stderr, "dlLoadLibrary(%s) failed\n", appl);
    exit(1);
  }

  appMain = dlFindSymbol(app_handle, "main");

  if (NULL == appMain)
  {
    fprintf(stderr, "dlFindSymbol(%s) failed\n", "main");
    exit(1);
  };

  app_argc = argc;
  app_argv = argv;
}

int dl_exec()
{
  return appMain(app_argc, app_argv);
}
