%{
#include <stdlib.h>
#include <stdio.h>
#include "cash.h"
#ifdef USE_LIBDL
#include <dlfcn.h>
#endif

void yyerror( scan_ctx_t *scan_ctx, const char *fmt, ... );
void cash_show_val (VALUE *val);

static int assign_op (scan_ctx_t*, VALUE *vl, VALUE *vr, int op);
static SYMTAB_ENTRY *new_sym (char *name, scantype type, VALUE *val, uint64_t mask);
static SYMTAB_ENTRY *new_libdl_sym (char *name, scantype type, VALUE *val, uint64_t mask);
static void funcCall (scan_ctx_t*, cash_fp *func, SYMTAB_ENTRY *sym, VALUE *args, VALUE *res);
static void init_proto_args (VALUE *pa);
static void add_pa_type (VALUE *pa, VALUE *t);
static void get_sym (VALUE *val, SYMTAB_ENTRY *sym);
static int pushScope (scan_ctx_t*, int activate);
static int popScope (scan_ctx_t*);

#define SCOPE_ACTIVE       (scan_ctx->scope[scan_ctx->scope_id].active)
#define CHECK_SCOPE        if (!SCOPE_ACTIVE) break;
#define SCOPE_START(cond)  if (pushScope(scan_ctx, cond)) YYABORT;
#define SCOPE_END          if (popScope(scan_ctx)) YYABORT;

cash_fp *cash_set_argc = NULL;


%}

%lex-param    { VALUE *yylval, scan_ctx_t *scan_ctx }
%parse-param  { scan_ctx_t *scan_ctx }

%token STRING
%token NUMBER
%token CHAR
%token ANON_ID
%token LIBDL_ID
%token FUNC_ID
%token VAR_ID
%token AND
%token RSHIFT
%token LSHIFT

%token KW_VOID
%token KW_CHAR
%token KW_UINT8
%token KW_UINT16
%token KW_UINT32
%token KW_UINT64
%token KW_LONG
%token MULA
%token DIVA
%token MODA
%token ADDA
%token SUBA
%token SHLA
%token SHRA
%token ANDA
%token ORA
%token XORA
%token OR
%token GE
%token LE
%token EQ
%token NE
%token AI
%token AD
%token KW_SIZEOF
%token DOTDOT
%token ITER_COND
%token ITER_START
%token ITER_END

%left MULA DIVA MODA ADDA SUBA SHLA SHRA ANDA ORA XORA
%right '?' ':' '='
%left OR
%left AND
%left '|'
%left '^'
%left '&'
%left EQ 
%left NE
%left GE 
%left LE 
%left '>' '<'
%left LSHIFT 
%left RSHIFT
%left '+' '-'
%left '*' '/' '%'
%precedence '('
%precedence UNARY

%define api.pure
%define parse.error verbose

%%

Statements:
  Statement
| Statements ';' Statement


Statement:
  ExprList
        {
            if (scan_ctx->verbose && scan_ctx->pid == getpid())
            {
                printf ("Result: "); cash_show_val (&$1); printf("\n");
            }
            scan_ctx->result = $1; 
        }
| %empty
;

ExprList:
  Expr
| ExprList ',' Expr
;

Expr:
  SimpleExpr
| FUNC_ID
            {
                CHECK_SCOPE; 

                $$.type = T_FUNC;
                $$.val = cash_symval_get ($1.sym);
                $$.mask = PTR_MASK;
                $$.sym = $1.sym;

                if ($$.val != 0)
                {
                    /* initialized function can not be redeclared */
                    $$.lv = 0;
                }
                else
                {
                    /* function prototype can be assigned to a name */
                    $$.lv = (uint64_t)((intptr_t)$$.sym->ptr);
                }
            }
;

ExpOrType:
  Expr
| Typedecl
;

SimpleExpr:
  '(' SimpleExpr ')'
      {
        $$ = $2;
      }
| KW_SIZEOF '(' ExpOrType ')'
            {
                CHECK_SCOPE; 

                bzero ((char*)&$$, sizeof($$));
                $$.type = T_INT;
                $$.mask = UINT_MASK;

                switch ($3.mask)
                {
                    case CHAR_MASK:
                    case UCHAR_MASK:
                        $$.val = 1; 
                        break;
                    case SHORT_MASK:
                        $$.val = 2; 
                        break;
                    case UINT_MASK:
                        $$.val = 4; 
                        break;
                    case ULL_MASK:
                        $$.val = 8; 
                        break;
                }
            }
| NUMBER
            {
                $$.type = T_INT;
                $$.lv = 0;
                $$.val = $1.val; 
                $$.mask = $1.mask;
            }
| CHAR
            {
                $$.type = T_INT; $$.lv = 0;
                $$.val = $1.val; 
                $$.mask = $1.mask;
            }
| '-' Expr %prec UNARY
            {
                CHECK_SCOPE; 
                $$ = $2; $$.val = (-$$.val) & $$.mask;
                $$.sym = NULL; $$.lv = 0; 
            }
| '+' Expr %prec UNARY
            {
                CHECK_SCOPE; 
                $$ = $2; 
            }
| '(' Typedecl ')' Expr %prec UNARY
            {
                CHECK_SCOPE; 
                $$ = $4; 
                $$.mask = $2.mask; 
                $$.val &= $$.mask;
                $$.type = $2.type;
                $$.lv = 0;
            }
| '*' Expr %prec UNARY
            {
                CHECK_SCOPE; 
                $$.type = T_INT;
                $$.sym = NULL;
                $$.lv = $2.val;

                switch ($2.type) {
                    case T_STR:
                            $$.mask = CHAR_MASK;
                            $$.val = *(unsigned char*)cash_adrs_remap($2.val);
                            break;
                    case T_STRPTR:
                            $$.type = T_STR;
                            $$.mask = PTR_MASK;
                            $$.val = *(unsigned char*)cash_adrs_remap($2.val);
                            break;
                    case T_CHARPTR:
                            $$.mask = UCHAR_MASK;
                            $$.val = *(unsigned char*)cash_adrs_remap($2.val);
                            break;
                    case T_SHORTPTR:
                            $$.mask = SHORT_MASK;
                            $$.val = *(unsigned short*)cash_adrs_remap($2.val);
                            break;
                    case T_INT:
                    case T_INTPTR:
                            $$.mask = UINT_MASK;
                            $$.val = *(unsigned int*)cash_adrs_remap($2.val);
                            break;
                    case T_INT64PTR:
                            $$.mask = ULL_MASK;
                            $$.val = *(uint64_t*)cash_adrs_remap($2.val);
                            break;
                    case T_FCTPTR:
                            $$.type = T_FUNC;
                            $$.mask = FUNC_MASK;
                            $$.val = *(long*)cash_adrs_remap($2.val);
                            break;
                    default:
                            CTX_PRINTF ("can not dereference non-pointer type");
                            YYABORT;
                }
            }
| '&' Expr  %prec UNARY
            { 
                CHECK_SCOPE; 
                scantype t;
                uint64_t v;

                if ($2.sym == NULL)
                {
#ifndef ALLOW_CONSTPTR
                    CTX_PRINTF ("can not determine address of constant\n");
                    YYABORT;
#else
                    /* for dereferencing a constant, need to add a temporary
                     * storage
                     */
                    v = (intptr_t)cash_get_constp (scan_ctx, $2.val, $2.mask);
                    
                    if (v == 0)
                    {
                        CTX_PRINTF ("too many constant pointers (max %d)\n", MAX_CONSTP);
                        YYABORT;
                    }
                    
                    t = $2.type;
#endif
                }
                else
                {
                    t = $2.type;
                    v = (intptr_t)$2.sym->ptr;
                }

                switch (t)
                {
                    case T_FUNC:
                        $$.type = T_FCTPTR;
                        break;
                    case T_STR:
                        $$.type = T_STRPTR;
                        break;
                    default:
                        switch ($2.mask)
                        {
                            case CHAR_MASK:
                                $$.type = T_STR; break;
                            case UCHAR_MASK:
                                $$.type = T_CHARPTR; break;
                            case SHORT_MASK:
                                $$.type = T_SHORTPTR; break;
                            case ULL_MASK:
                                $$.type = T_INT64PTR; break;
                            default:
                                $$.type = T_INTPTR; break;
                        }
                }
                $$.mask = PTR_MASK;
                $$.sym = NULL;
                $$.val = v;
                $$.lv = 0;
            }
| Expr '[' Expr ']'
            {
                CHECK_SCOPE; 
                $$.type = T_INT;
                $$.sym = NULL;
                switch ($1.type) {
                    case T_STR:
                        $$.mask = CHAR_MASK;
                        $$.lv = $1.val + $3.val * sizeof(char);
                        $$.val = *(unsigned char*)cash_adrs_remap($$.lv);
                        break;
                    case T_CHARPTR:
                        $$.mask = UCHAR_MASK;
                        $$.lv = $1.val + $3.val * sizeof(char);
                        $$.val = *(unsigned char*)cash_adrs_remap($$.lv);
                        break;
                    case T_SHORTPTR:
                        $$.mask = SHORT_MASK;
                        $$.lv = $1.val + $3.val * sizeof(short);
                        $$.val = *(unsigned short*)cash_adrs_remap($$.lv);
                        break;
                    case T_INTPTR:
                        $$.mask = UINT_MASK;
                        $$.lv = $1.val + $3.val * sizeof(int);
                        $$.val = *(unsigned int*)cash_adrs_remap($$.lv);
                        break;
                    case T_INT64PTR:
                        $$.mask = ULL_MASK;
                        $$.lv = $1.val + $3.val * sizeof(uint64_t);
                        $$.val = *(uint64_t*)cash_adrs_remap($$.lv);
                        break;
                    default:
                        CTX_PRINTF ("can not dereference non-pointer type");
                        YYABORT;
                }
            }
| STRING
            {
                CHECK_SCOPE; 
                $$.val = $1.val;

                if ($$.val == 0)
                    YYABORT;

                $$.type = T_STR;
                $$.mask = PTR_MASK;
                $$.sym = NULL;
                $$.lv = 0;
            }
| VAR_ID
            {
                CHECK_SCOPE; 
                switch ($1.sym->mask)
                {
                    case CHAR_MASK:
                    case UCHAR_MASK:
                        $$.val =  (uint64_t)(*(char*)cash_adrs_remap((intptr_t)$1.sym->ptr));
                        break;
                    case SHORT_MASK:
                        $$.val =  (uint64_t)(*(short*)cash_adrs_remap((intptr_t)$1.sym->ptr));
                        break;
                    case ULL_MASK:
                        $$.val =  (uint64_t)(*(uint64_t*)cash_adrs_remap((intptr_t)$1.sym->ptr));
                        break;
                    default:
                        $$.val =  (uint64_t)(*(int*)cash_adrs_remap((intptr_t)$1.sym->ptr));
                        break;
                }
                $$.type = $1.sym->type;
                $$.mask = $1.sym->mask;
                $$.sym = $1.sym;
                $$.lv = (uint64_t)((uintptr_t)$1.sym->ptr);
            }
| Expr '*' Expr
            { 
                CHECK_SCOPE; 
                $$.val = ($1.val * $3.val) & $1.mask; $$.mask = $1.mask; $$.sym = 0; $$.lv = 0; 
            }
| Expr '/' Expr
            { 
                CHECK_SCOPE; 
                $$.val = ($1.val / $3.val) & $1.mask; $$.mask = $1.mask; $$.sym = 0; $$.lv = 0; 
            }
| Expr '+' Expr
            { 
                CHECK_SCOPE; 
                $$.val = ($1.val + $3.val) & $1.mask; $$.mask = $1.mask; $$.sym = 0; $$.lv = 0; 
            }
| Expr '-' Expr
            { 
                CHECK_SCOPE; 
                $$.val = ($1.val - $3.val) & $1.mask; $$.mask = $1.mask; $$.sym = 0; $$.lv = 0; 
            }
| Expr '|' Expr
            { 
                CHECK_SCOPE; 
                $$.val = ($1.val | $3.val) & $1.mask; $$.mask = $1.mask; $$.sym = 0; $$.lv = 0; 
            }
| Expr '&' Expr
            {
                CHECK_SCOPE; 
                $$.val = ($1.val & $3.val) & $1.mask; $$.mask = $1.mask; $$.sym = 0; $$.lv = 0; 
            }
| Expr '^' Expr
            {
                CHECK_SCOPE; 
                $$.val = ($1.val ^ $3.val) & $1.mask; $$.mask = $1.mask; $$.sym = 0; $$.lv = 0; 
            }
| Expr '%' Expr
            {
                CHECK_SCOPE; 
                $$.val = ($1.val % $3.val) & $1.mask; $$.mask = $1.mask; $$.sym = 0; $$.lv = 0; 
            }

| Expr ADDA Expr
            {
                CHECK_SCOPE; 
                if (!assign_op (scan_ctx, &$1, &$3, ADDA)) YYABORT; $$ = $1; 
            }
| Expr SUBA Expr
            {
                CHECK_SCOPE; 
                if (!assign_op (scan_ctx, &$1, &$3, SUBA)) YYABORT; $$ = $1; 
            }
| Expr MULA Expr
            {
                CHECK_SCOPE; 
                if (!assign_op (scan_ctx, &$1, &$3, MULA)) YYABORT; $$ = $1; 
            }
| Expr DIVA Expr
            {
                CHECK_SCOPE; 
                if (!assign_op (scan_ctx, &$1, &$3, DIVA)) YYABORT; $$ = $1; 
            }
| Expr ORA Expr
            {
                CHECK_SCOPE; 
                if (!assign_op (scan_ctx, &$1, &$3, ORA)) YYABORT; $$ = $1; 
            }
| Expr ANDA Expr
            {
                CHECK_SCOPE; 
                if (!assign_op (scan_ctx, &$1, &$3, ANDA)) YYABORT; $$ = $1; 
            }
| Expr XORA Expr
            {
                CHECK_SCOPE; 
                if (!assign_op (scan_ctx, &$1, &$3, XORA)) YYABORT; $$ = $1; 
            }
| Expr MODA Expr
            {
                CHECK_SCOPE; 
                if (!assign_op (scan_ctx, &$1, &$3, MODA)) YYABORT; $$ = $1; 
            }
| Expr SHLA Expr
            {
                CHECK_SCOPE; 
                if (!assign_op (scan_ctx, &$1, &$3, SHLA)) YYABORT; $$ = $1; 
            }
| Expr SHRA Expr
            {
                CHECK_SCOPE; 
                if (!assign_op (scan_ctx, &$1, &$3, SHRA)) YYABORT; $$ = $1; 
            }

| Expr AI %prec UNARY
            {
                CHECK_SCOPE; 
                $$=$1; if (!assign_op (scan_ctx, &$1, NULL, AI)) YYABORT; 
            }
| Expr AD %prec UNARY
            {
                CHECK_SCOPE; 
                $$=$1; if (!assign_op (scan_ctx, &$1, NULL, AD)) YYABORT; 
            }

| AI Expr %prec UNARY
            {
                CHECK_SCOPE; 
                if (!assign_op (scan_ctx, &$2, NULL, AI)) YYABORT; $$ = $2; 
            }
| AD Expr %prec UNARY
            {
                CHECK_SCOPE; 
                if (!assign_op (scan_ctx, &$2, NULL, AD)) YYABORT; $$ = $2; 
            }

| Expr AND Expr
            {
                CHECK_SCOPE; 
                $$.val = ($1.val && $3.val); $$.mask = $1.mask; $$.type = T_INT; $$.sym = 0; $$.lv = 0; 
            }
| Expr OR Expr
            {
                CHECK_SCOPE; 
                $$.val = ($1.val || $3.val); $$.mask = $1.mask; $$.type = T_INT; $$.sym = 0; $$.lv = 0; 
            }
| Expr EQ Expr
            {
                CHECK_SCOPE; 
                $$.val = ($1.val == $3.val); $$.mask = $1.mask; $$.type = T_INT; $$.sym = 0; $$.lv = 0; 
            }
| Expr NE Expr
            {
                CHECK_SCOPE; 
                $$.val = ($1.val != $3.val); $$.mask = $1.mask; $$.type = T_INT; $$.sym = 0; $$.lv = 0; 
            }
| Expr '>' Expr
            {
                CHECK_SCOPE; 
                $$.val = ($1.val > $3.val); $$.mask = $1.mask; $$.type = T_INT; $$.sym = 0; $$.lv = 0; 
            }
| Expr '<' Expr
            {
                CHECK_SCOPE; 
                $$.val = ($1.val < $3.val); $$.mask = $1.mask; $$.type = T_INT; $$.sym = 0; $$.lv = 0; 
            }
| Expr GE Expr
            {
                CHECK_SCOPE; 
                $$.val = ($1.val >= $3.val); $$.mask = $1.mask; $$.type = T_INT; $$.sym = 0; $$.lv = 0; 
            }
| Expr LE Expr
            {
                CHECK_SCOPE; 
                $$.val = ($1.val <= $3.val); $$.mask = $1.mask; $$.type = T_INT; $$.sym = 0; $$.lv = 0; 
            }

| Expr RSHIFT Expr
            {
                CHECK_SCOPE; 
                $$.val = ($1.val >> $3.val) & $1.mask; $$.mask = $1.mask; $$.sym = 0; $$.lv = 0; 
            }
| Expr LSHIFT Expr
            {
                CHECK_SCOPE; 
                $$.val = ($1.val << $3.val) & $1.mask; $$.mask = $1.mask; $$.sym = 0; $$.lv = 0;
            }
| Expr '?' 
            {SCOPE_START($1.val != 0)} Expr {SCOPE_END; SCOPE_START($1.val == 0);} ':' 
            Expr 
            {
                SCOPE_END; 
                CHECK_SCOPE; 
                $$.val = $1.val ? $4.val : $7.val;
                $$.mask = $1.val ? $4.mask : $7.mask;
                $$.val &= $$.mask; 
                $$.sym = 0;
                $$.lv = 0; 
            }
| '!' Expr  %prec UNARY
            {
                CHECK_SCOPE; 
                $$ = $2; $$.val = (!$$.val) & $$.mask; $$.sym = 0; $$.lv = 0; 
            }
| '~' Expr  %prec UNARY
            {
                CHECK_SCOPE; 
                $$ = $2; $$.val = (~$$.val) & $$.mask; $$.sym = 0; $$.lv = 0; 
            }
| Expr '(' Parameters ')' 
            { 
                CHECK_SCOPE; 
                funcCall (scan_ctx, (cash_fp*)((uintptr_t)$1.val), NULL, &$3, &$$);
            }
| FUNC_ID '(' Parameters ')' 
            { 
                CHECK_SCOPE;
                funcCall (scan_ctx, (cash_fp*)((uintptr_t)cash_symval_get ($1.sym)), $1.sym, &$3, &$$);
            }
| LIBDL_ID '(' Parameters ')' 
            {
                CHECK_SCOPE;
                SYMTAB_ENTRY        *e;
                
                e = dl_func_get ((char*)(uintptr_t)$1.val, NULL);
 
                if (e == NULL)
                {
                    CTX_PRINTF("undefined symbol : %s\n", (char*)(uintptr_t)$1.val);
                    free ((char*)(uintptr_t)$1.val);
                    YYABORT;
                }
                else
                {
                    funcCall (scan_ctx, (cash_fp*)((uintptr_t)cash_symval_get (e)), e, &$3, &$$);
                }
            }
| ITER_START SimpleExpr ITER_COND SimpleExpr DOTDOT ITER_END
            {
                CHECK_SCOPE; 
                int flag = $2.val ? 0 : ITER_TERM_LOCAL;
                cash_do_iterator ((scan_ctx_t*)scan_ctx, &$4, NULL, &$$, flag);
            }
| ITER_START SimpleExpr ITER_COND SimpleExpr DOTDOT SimpleExpr ITER_END
            {
                CHECK_SCOPE; 
                int flag = $2.val ? 0 : ITER_TERM_LOCAL;
                cash_do_iterator ((scan_ctx_t*)scan_ctx, &$4, &$6, &$$, flag);
            }
| ITER_START SimpleExpr DOTDOT SimpleExpr ITER_END
            {
                CHECK_SCOPE; 
                cash_do_iterator ((scan_ctx_t*)scan_ctx, &$2, &$4, &$$, 0);
            }
| ITER_START SimpleExpr ITER_COND IteratorList ITER_END
            {
                CHECK_SCOPE; 
                int flag = $2.val ? 0 : ITER_TERM_LOCAL;
                flag |= ITER_VALUESET;
                cash_do_iterator ((scan_ctx_t*)scan_ctx, &$4, NULL, &$$, flag );
            }
| ITER_START IteratorList ITER_END
            {
                CHECK_SCOPE; 
                cash_do_iterator ((scan_ctx_t*)scan_ctx, &$2, NULL, &$$, ITER_VALUESET );
            }
| Declaration
| Assignment
;

IteratorList:
            Expr
            {
                CHECK_SCOPE; 
                $$.num_args = 1; 
                memset ((void*)&$$.args, 0, sizeof ($$.args));
                $$.args[0] = $1.val; 
                $$.argsize[0] = $1.mask; 
            }
|         IteratorList ',' Expr
            {
                static int warn = 0;
                CHECK_SCOPE; 
                
                if (!warn && scan_ctx->iterator_warn)
                {
                    CTX_PRINTF ("cash: use of comma for iterator is deprecated, use colon instead!\n");
                    warn = 1;
                }
                
                if ($$.num_args >= MAX_PARMS) 
                {
                        CTX_PRINTF ("Too many function parameters (max %d)\n", 
                                        MAX_PARMS);
                        YYABORT;
                }
                $$.args[$$.num_args] = $3.val;
                $$.argsize[$$.num_args] = $3.mask; 
                $$.num_args++;
            }
|         IteratorList ':' Expr
            {
                CHECK_SCOPE; 
                if ($$.num_args >= MAX_PARMS) 
                {
                        CTX_PRINTF ("Too many function parameters (max %d)\n", 
                                        MAX_PARMS);
                        YYABORT;
                }
                $$.args[$$.num_args] = $3.val;
                $$.argsize[$$.num_args] = $3.mask; 
                $$.num_args++;
            }
;

Parameters:
  %empty
            {
                CHECK_SCOPE; 
                $$.num_args = 0; 
                memset ((void*)&$$.args, 0, sizeof ($$.args)); 
            }
| ParameterList
;

ParameterList:
            Expr
            {
                CHECK_SCOPE; 
                $$.num_args = 1; 
                memset ((void*)&$$.args, 0, sizeof ($$.args));
                $$.args[0] = $1.val; 
                $$.argsize[0] = $1.mask; 
            }
|         ParameterList ',' Expr
            {
                CHECK_SCOPE; 
                if ($$.num_args >= MAX_PARMS) 
                {
                    CTX_PRINTF ("Too many function parameters (max %d)\n", 
                                    MAX_PARMS);
                    YYABORT;
                }
                $$.args[$$.num_args] = $3.val;
                $$.argsize[$$.num_args] = $3.mask; 
                $$.num_args++;
            }
;

ProtoArgs:
            %empty
            {
                CHECK_SCOPE; 
                init_proto_args (&$$);
            }
|        ProtoArgList
;

ProtoArgList:
        Typedecl
            {
                CHECK_SCOPE; 
                init_proto_args (&$$);
                add_pa_type (&$$, &$1);
            }
| ProtoArgList ',' Typedecl
            {
                CHECK_SCOPE; 
                add_pa_type (&$$, &$3);
            }
;

Declaration:
        Typedecl ANON_ID
            {
                char *name = (char*)((uintptr_t)$2.val);
                CHECK_SCOPE; 
                if (!new_sym (name, $1.type, &$$, $1.mask)) 
                {
                    free (name);
                    YYABORT; 
                }
            }
| Typedecl LIBDL_ID
            {
                char *name = (char*)((uintptr_t)$2.val);
                CHECK_SCOPE; 
                if (!new_libdl_sym (name, $1.type, &$$, $1.mask)) 
                {
                free (name);
                    YYABORT; 
                }
            }
| Typedecl VAR_ID
            {
                CHECK_SCOPE; 
                if ($1.type != $2.sym->type)
                {
                    CTX_PRINTF ("<%s> : Can not redefine as different type\n", 
                                    $2.sym->name); 
                    YYABORT; 
                }

                if ($1.mask != $2.sym->mask)
                {
                    CTX_PRINTF ("<%s> : Can not redefine storage width\n", 
                                        $2.sym->name); 
                    CTX_PRINTF ("<%s> : Use construct: <type> (<symbol>)\n", 
                                        $2.sym->name); 
                    YYABORT; 
                }

                get_sym (&$$, $2.sym);
            }

| Typedecl '(' VAR_ID ')'
            {
                CHECK_SCOPE; 
                if ($1.type != $3.sym->type)
                {
                    CTX_PRINTF ("<%s> : Can not redefine as different type\n", 
                                    $3.sym->name); 
                    YYABORT; 
                }
                $3.sym->mask = $1.mask;
                get_sym (&$$, $3.sym);
            }

| Typedecl  FUNC_ID  '(' ProtoArgs ')'
            {
                CHECK_SCOPE; 
                if ($2.sym->proto)
                    free ($2.sym->proto);

                $2.sym->proto = $4.proto;
                $2.sym->proto->rcsize = $1.mask;


                $$ = $2;
                $$.type = T_FUNC;
                $$.val = *(uintptr_t*)$2.sym->ptr;

                if ($$.val != 0)
                {
                    /* initialized function can not be redeclared */
                    $$.lv = 0;
                }
                else
                {
                    /* function prototype can be assigned to a name */
                    $$.lv = (uint64_t)((uintptr_t)$$.sym->ptr);
                }
            }
| Typedecl  ANON_ID '(' ProtoArgs ')'
            {
                CHECK_SCOPE; 
                SYMTAB_ENTRY *s;

                s = new_sym ((char*)((uintptr_t)$2.val), T_FUNC, &$$, PTR_MASK); 

                if (s)
                {
                    s->proto = $4.proto;
                    s->proto->rcsize = $1.mask;
                }
                else
                {
                    $$.type = T_ANON;
                }
            }
| Typedecl  LIBDL_ID '(' ProtoArgs ')'
            {
                CHECK_SCOPE; 
                SYMTAB_ENTRY *s;
 
                s = new_libdl_sym ((char*)((uintptr_t)$2.val), T_FUNC, &$$, PTR_MASK); 
 
                if (s)
                {
                    s->proto = $4.proto;
                    s->proto->rcsize = $1.mask;
                }
                else
                {
                    $$.type = T_ANON;
                }
            }
;

Typedecl:
  KW_CHAR       { $$.type = T_INT; $$.mask = CHAR_MASK; }
| KW_UINT8      { $$.type = T_INT; $$.mask = UCHAR_MASK;  }
| KW_UINT16     { $$.type = T_INT; $$.mask = SHORT_MASK; }
| KW_VOID       { $$.type = T_INT; $$.mask = UINT_MASK; }
| KW_UINT32     { $$.type = T_INT; $$.mask = UINT_MASK; }
| KW_UINT64     { $$.type = T_INT; $$.mask = ULL_MASK; }
| KW_LONG       { $$.type = T_INT; $$.mask = UL_MASK; }
| KW_VOID '*'   { $$.type = T_CHARPTR; $$.mask = PTR_MASK; }
| KW_CHAR '*'   { $$.type = T_STR; $$.mask = PTR_MASK; }
| KW_UINT8 '*'  { $$.type = T_CHARPTR; $$.mask = PTR_MASK; }
| KW_UINT16 '*' { $$.type = T_SHORTPTR; $$.mask = PTR_MASK; }
| KW_UINT32 '*' { $$.type = T_INTPTR; $$.mask = PTR_MASK; }
| KW_UINT64 '*' { $$.type = T_INT64PTR; $$.mask = PTR_MASK; }
| Typedecl '*'  { $$.type = (PTR_MASK == 0xffffffffULL)?T_INTPTR:T_INT64PTR;
                  $$.mask = PTR_MASK; }
;



Assignment:
            Expr '=' Expr
            {
                CHECK_SCOPE;
                if ($1.lv == 0)
                {
                    CTX_PRINTF ("bad lvalue in assignment\n");
                    YYABORT;
                }

                if (scan_ctx->verbose != 2)
                    scan_ctx->verbose = 0;

                switch ($1.mask)
                {
                case CHAR_MASK:
                case UCHAR_MASK:
                    *(char*)cash_adrs_remap($1.lv) = $3.val; 
                    $$.val = *(char*)cash_adrs_remap($1.lv);
                    break;
                case SHORT_MASK:
                    *(short*)cash_adrs_remap($1.lv) = $3.val; 
                    $$.val = *(short*)cash_adrs_remap($1.lv);
                    break;
                case ULL_MASK:
                    *(uint64_t*)cash_adrs_remap($1.lv) = $3.val; 
                    $$.val = *(uint64_t*)cash_adrs_remap($1.lv);
                    break;
                default:
                    *(int*)cash_adrs_remap($1.lv) = $3.val; 
                    $$.val = *(int*)cash_adrs_remap($1.lv);
                    break;
                }
                $$.type = T_INT;
                $$.sym = NULL;
                $$.lv = 0;
            }
| ANON_ID '=' Expr
            {
                CHECK_SCOPE;

                if (!new_sym ((char*)((uintptr_t)$1.val), $3.type, &$$, $3.mask))
                {
                    free ((char*)(uintptr_t)$1.val);
                    YYABORT;
                }
                $$.val = $3.val;
 
                switch ($$.mask)
                {
                case CHAR_MASK:
                case UCHAR_MASK:
                    *(char*)cash_adrs_remap($$.lv) = $$.val; 
                    break;
                case SHORT_MASK:
                    *(short*)cash_adrs_remap($$.lv) = $$.val; 
                    break;
                case ULL_MASK:
                    *(uint64_t*)cash_adrs_remap($$.lv) = $$.val; 
                    break;
                default:
                    *(int*)cash_adrs_remap($$.lv) = $$.val; 
                    break;
                }
            }
| LIBDL_ID '=' Expr
            {
                CHECK_SCOPE;
 
                if (!new_libdl_sym ((char*)((uintptr_t)$1.val), $3.type, &$$, $3.mask))
                {
                    free ((char*)(uintptr_t)$1.val);
                    YYABORT;
                }
                $$.val = $3.val;

                switch ($$.mask)
                {
                case CHAR_MASK:
                case UCHAR_MASK:
                    *(char*)cash_adrs_remap($$.lv) = $$.val; 
                    break;
                case SHORT_MASK:
                    *(short*)cash_adrs_remap($$.lv) = $$.val; 
                    break;
                case ULL_MASK:
                    *(uint64_t*)cash_adrs_remap($$.lv) = $$.val; 
                    break;
                default:
                    *(int*)cash_adrs_remap($$.lv) = $$.val; 
                    break;
                }
            }
| ANON_ID
            {
                CHECK_SCOPE;
                CTX_PRINTF("undefined symbol: %s\n", (char*)(uintptr_t)$1.val);
                free ((char*)(uintptr_t)$1.val);
                YYABORT;
            }
;


%%

void funcCall (scan_ctx_t *scan_ctx, cash_fp *func, SYMTAB_ENTRY *sym, VALUE *args, VALUE *res)
{
#ifdef DYNCALL
    int i;
    FCT_PROTO *proto = NULL;
    DCCallVM * callVm = dcNewCallVM (100);
    uint64_t argsize;
    uint64_t arg;

    if (!callVm)
        return;

    dcMode (callVm, DC_CALL_C_DEFAULT);
    dcReset (callVm);

    if (sym)
        proto = sym->proto;

    for (i = 0; i < MAX_PARMS; i++)
    {
        arg = 0;

        if (i < args->num_args)
            arg = args->args[i];

        if (proto && i < proto->numArgs)
        {
            argsize = proto->argsize[i];
        }
        else
        {
            argsize = args->argsize[i];
        }

        switch (argsize)
        {
        case CHAR_MASK:
        case UCHAR_MASK:
            dcArgChar (callVm, (DCchar)arg);
            break;
        case SHORT_MASK:
            dcArgShort (callVm, (DCshort)arg);
            break;
        case ULL_MASK:
            dcArgLongLong (callVm, (DClonglong)arg);
            break;
        default:
            dcArgInt (callVm, (DCint)arg);
            break;
        }
    }

    if (cash_set_argc)
        cash_set_argc (args->num_args);

    if (proto)
    {
        switch (proto->rcsize)
        {
        case CHAR_MASK:
        case UCHAR_MASK:
            res->val = dcCallChar (callVm, (DCpointer)func);
            break;
        case SHORT_MASK:
            res->val = dcCallShort (callVm, (DCpointer)func);
            break;
        case ULL_MASK:
            res->val = dcCallLongLong (callVm, (DCpointer)func);
            break;
        default:
            res->val = dcCallInt (callVm, (DCpointer)func);
            break;
        }
        res->mask = proto->rcsize;
    }
    else
    {
        res->val = dcCallInt (callVm, (DCpointer)func);
        res->mask = UINT_MASK;
    }

    dcFree (callVm);
#else
    cash_fp_pi *fp = (cash_fp_pi*)func;
    cash_fp_2pi *fp2 = (cash_fp_2pi*)func;
    cash_fp_3pi *fp3 = (cash_fp_3pi*)func;
    cash_fp_4pi *fp4 = (cash_fp_4pi*)func;
    cash_fp_5pi *fp5 = (cash_fp_5pi*)func;

    if (cash_set_argc)
        cash_set_argc (args->num_args);

    int num_ptr;
    for (num_ptr = 0; num_ptr <= 5; num_ptr++)
    {
        if (args->argsize[num_ptr] != PTR_MASK)
        break;
    }

    switch (num_ptr)
    {
    case 1:
        res->val = fp ((void*)args->args[0],
            (int)args->args[1],
            (int)args->args[2],
            (int)args->args[3],
            (int)args->args[4],
            (int)args->args[5],
            (int)args->args[6],
            (int)args->args[7],
            (int)args->args[8],
            (int)args->args[9],
            (int)args->args[10],
            (int)args->args[11],
            (int)args->args[12],
            (int)args->args[13],
            (int)args->args[14],
            (int)args->args[15],
            (int)args->args[16],
            (int)args->args[17],
            (int)args->args[18],
            (int)args->args[19]
            );
        break;
    case 2:
        res->val = fp2((void*)args->args[0],
            (void*)args->args[1],
            (int)args->args[2],
            (int)args->args[3],
            (int)args->args[4],
            (int)args->args[5],
            (int)args->args[6],
            (int)args->args[7],
            (int)args->args[8],
            (int)args->args[9],
            (int)args->args[10],
            (int)args->args[11],
            (int)args->args[12],
            (int)args->args[13],
            (int)args->args[14],
            (int)args->args[15],
            (int)args->args[16],
            (int)args->args[17],
            (int)args->args[18],
            (int)args->args[19]
            );
        break;
    case 3:
      res->val = fp3((void*)args->args[0],
            (void*)args->args[1],
            (void*)args->args[2],
            (int)args->args[3],
            (int)args->args[4],
            (int)args->args[5],
            (int)args->args[6],
            (int)args->args[7],
            (int)args->args[8],
            (int)args->args[9],
            (int)args->args[10],
            (int)args->args[11],
            (int)args->args[12],
            (int)args->args[13],
            (int)args->args[14],
            (int)args->args[15],
            (int)args->args[16],
            (int)args->args[17],
            (int)args->args[18],
            (int)args->args[19]
            );
        break;
    case 4:
      res->val = fp4((void*)args->args[0],
            (void*)args->args[1],
            (void*)args->args[2],
            (void*)args->args[3],
            (int)args->args[4],
            (int)args->args[5],
            (int)args->args[6],
            (int)args->args[7],
            (int)args->args[8],
            (int)args->args[9],
            (int)args->args[10],
            (int)args->args[11],
            (int)args->args[12],
            (int)args->args[13],
            (int)args->args[14],
            (int)args->args[15],
            (int)args->args[16],
            (int)args->args[17],
            (int)args->args[18],
            (int)args->args[19]
            );
        break;
    case 5:
        res->val = fp5((void*)args->args[0],
            (void*)args->args[1],
            (void*)args->args[2],
            (void*)args->args[3],
            (void*)args->args[4],
            (int)args->args[5],
            (int)args->args[6],
            (int)args->args[7],
            (int)args->args[8],
            (int)args->args[9],
            (int)args->args[10],
            (int)args->args[11],
            (int)args->args[12],
            (int)args->args[13],
            (int)args->args[14],
            (int)args->args[15],
            (int)args->args[16],
            (int)args->args[17],
            (int)args->args[18],
            (int)args->args[19]
            );
        break;
    default:
        res->val = func((int)args->args[0],
            (int)args->args[1],
            (int)args->args[2],
            (int)args->args[3],
            (int)args->args[4],
            (int)args->args[5],
            (int)args->args[6],
            (int)args->args[7],
            (int)args->args[8],
            (int)args->args[9],
            (int)args->args[10],
            (int)args->args[11],
            (int)args->args[12],
            (int)args->args[13],
            (int)args->args[14],
            (int)args->args[15],
            (int)args->args[16],
            (int)args->args[17],
            (int)args->args[18],
            (int)args->args[19]
            );
        break;
    }

    res->mask = UINT_MASK;
#endif
    res->type = T_INT;
    res->sym = NULL;
    res->lv = 0;
        
    /* if call has forked, don't let child process resume parsing */
    if (getpid() != scan_ctx->pid)
    {
        exit(0);
    }
}


static void get_sym (VALUE *val, SYMTAB_ENTRY *sym)
{
    bzero (val, sizeof(*val));
    
    switch (sym->mask)
    {
        case CHAR_MASK:
        case UCHAR_MASK:
            val->val =  (uint64_t)(*(char*)cash_adrs_remap((intptr_t)sym->ptr));
            break;
        case SHORT_MASK:
              val->val =  (uint64_t)(*(short*)cash_adrs_remap((intptr_t)sym->ptr));
              break;
        case ULL_MASK:
              val->val =  (uint64_t)(*(uint64_t*)cash_adrs_remap((intptr_t)sym->ptr));
              break;
        default:
              val->val =  (uint64_t)(*(int*)cash_adrs_remap((intptr_t)sym->ptr));
              break;
    }
    val->type = sym->type;
    val->mask = sym->mask;
    val->sym = sym;
    val->lv = (uint64_t)((uintptr_t)sym->ptr);


}

static SYMTAB_ENTRY *new_sym (char *name, scantype type, VALUE *val, uint64_t mask)
{
    SYMTAB_ENTRY    *entry;

    if (mask == 0)
        mask = UINT_MASK;

    entry = cash_symtab_add (name, type, 0, NULL, mask);

    if (entry == NULL)
        return NULL;

    val->type = type;
    val->val = 0;
    val->mask = mask;
    val->sym = entry;
    val->lv = (intptr_t)entry->ptr;

    return entry;
}
 
static SYMTAB_ENTRY *new_libdl_sym (char *name, scantype type, VALUE *val, uint64_t mask)
{
#ifndef USE_LIBDL
    return NULL;
#else
    SYMTAB_ENTRY    *entry;
    intptr_t adrs;
 
    if (mask == 0)
        mask = UINT_MASK;
 
    adrs = (intptr_t)dlsym (NULL, name);
    if (!adrs)
        return NULL;
 
    entry = cash_symtab_add (name, type, adrs, NULL, mask);
 
    if (entry == NULL)
        return NULL;
 
    val->type = type;
    val->val = adrs;
    val->mask = mask;
    val->sym = entry;
    val->lv = (intptr_t)entry->ptr;
 
    return entry;
#endif
}
 
static int assign_op (scan_ctx_t *scan_ctx, VALUE *vl, VALUE *vr, int op)
{
    if (vl->lv == 0)
    {
        CTX_PRINTF ("bad lvalue\n");
        return 0;
    }

    switch (vl->mask)
    {
    case CHAR_MASK:
        switch (op)
        {
        case ADDA:  *(signed char*)cash_adrs_remap(vl->lv) += vr->val; break;
        case SUBA:  *(signed char*)cash_adrs_remap(vl->lv) -= vr->val; break;
        case MULA:  *(signed char*)cash_adrs_remap(vl->lv) *= vr->val; break;
        case DIVA:  *(signed char*)cash_adrs_remap(vl->lv) /= vr->val; break;
        case ORA:   *(signed char*)cash_adrs_remap(vl->lv) |= vr->val; break;
        case ANDA:  *(signed char*)cash_adrs_remap(vl->lv) &= vr->val; break;
        case XORA:  *(signed char*)cash_adrs_remap(vl->lv) ^= vr->val; break;
        case MODA:  *(signed char*)cash_adrs_remap(vl->lv) %= vr->val; break;
        case SHLA:  *(signed char*)cash_adrs_remap(vl->lv) <<= vr->val; break;
        case SHRA:  *(signed char*)cash_adrs_remap(vl->lv) >>= vr->val; break;
        case AI:    *(signed char*)cash_adrs_remap(vl->lv) += 1; break;
        case AD:    *(signed char*)cash_adrs_remap(vl->lv) -= 1; break;
        default:    return 0;
        }
        break;
    case UCHAR_MASK:
        switch (op)
        {
        case ADDA:  *(unsigned char*)cash_adrs_remap(vl->lv) += vr->val; break;
        case SUBA:  *(unsigned char*)cash_adrs_remap(vl->lv) -= vr->val; break;
        case MULA:  *(unsigned char*)cash_adrs_remap(vl->lv) *= vr->val; break;
        case DIVA:  *(unsigned char*)cash_adrs_remap(vl->lv) /= vr->val; break;
        case ORA:   *(unsigned char*)cash_adrs_remap(vl->lv) |= vr->val; break;
        case ANDA:  *(unsigned char*)cash_adrs_remap(vl->lv) &= vr->val; break;
        case XORA:  *(unsigned char*)cash_adrs_remap(vl->lv) ^= vr->val; break;
        case MODA:  *(unsigned char*)cash_adrs_remap(vl->lv) %= vr->val; break;
        case SHLA:  *(unsigned char*)cash_adrs_remap(vl->lv) <<= vr->val; break;
        case SHRA:  *(unsigned char*)cash_adrs_remap(vl->lv) >>= vr->val; break;
        case AI:    *(unsigned char*)cash_adrs_remap(vl->lv) += 1; break;
        case AD:    *(unsigned char*)cash_adrs_remap(vl->lv) -= 1; break;
        default:    return 0;
        }
        break;
    case SHORT_MASK:
        switch (op)
        {
        case ADDA:  *(unsigned short*)cash_adrs_remap(vl->lv) += vr->val; break;
        case SUBA:  *(unsigned short*)cash_adrs_remap(vl->lv) -= vr->val; break;
        case MULA:  *(unsigned short*)cash_adrs_remap(vl->lv) *= vr->val; break;
        case DIVA:  *(unsigned short*)cash_adrs_remap(vl->lv) /= vr->val; break;
        case ORA:   *(unsigned short*)cash_adrs_remap(vl->lv) |= vr->val; break;
        case ANDA:  *(unsigned short*)cash_adrs_remap(vl->lv) &= vr->val; break;
        case XORA:  *(unsigned short*)cash_adrs_remap(vl->lv) ^= vr->val; break;
        case MODA:  *(unsigned short*)cash_adrs_remap(vl->lv) %= vr->val; break;
        case SHLA:  *(unsigned short*)cash_adrs_remap(vl->lv) <<= vr->val; break;
        case SHRA:  *(unsigned short*)cash_adrs_remap(vl->lv) >>= vr->val; break;
        case AI:    *(unsigned short*)cash_adrs_remap(vl->lv) += 1; break;
        case AD:    *(unsigned short*)cash_adrs_remap(vl->lv) -= 1; break;
        default:    return 0;
        }
        break;
    case ULL_MASK:
        switch (op)
        {
        case ADDA:  *(uint64_t*)cash_adrs_remap(vl->lv) += vr->val; break;
        case SUBA:  *(uint64_t*)cash_adrs_remap(vl->lv) -= vr->val; break;
        case MULA:  *(uint64_t*)cash_adrs_remap(vl->lv) *= vr->val; break;
        case DIVA:  *(uint64_t*)cash_adrs_remap(vl->lv) /= vr->val; break;
        case ORA:   *(uint64_t*)cash_adrs_remap(vl->lv) |= vr->val; break;
        case ANDA:  *(uint64_t*)cash_adrs_remap(vl->lv) &= vr->val; break;
        case XORA:  *(uint64_t*)cash_adrs_remap(vl->lv) ^= vr->val; break;
        case MODA:  *(uint64_t*)cash_adrs_remap(vl->lv) %= vr->val; break;
        case SHLA:  *(uint64_t*)cash_adrs_remap(vl->lv) <<= vr->val; break;
        case SHRA:  *(uint64_t*)cash_adrs_remap(vl->lv) >>= vr->val; break;
        case AI:    *(uint64_t*)cash_adrs_remap(vl->lv) += 1; break;
        case AD:    *(uint64_t*)cash_adrs_remap(vl->lv) -= 1; break;
        default:    return 0;
        }
        break;
    default:
        switch (op)
        {
        case ADDA:  *(unsigned int*)cash_adrs_remap(vl->lv) += vr->val; break;
        case SUBA:  *(unsigned int*)cash_adrs_remap(vl->lv) -= vr->val; break;
        case MULA:  *(unsigned int*)cash_adrs_remap(vl->lv) *= vr->val; break;
        case DIVA:  *(unsigned int*)cash_adrs_remap(vl->lv) /= vr->val; break;
        case ORA:   *(unsigned int*)cash_adrs_remap(vl->lv) |= vr->val; break;
        case ANDA:  *(unsigned int*)cash_adrs_remap(vl->lv) &= vr->val; break;
        case XORA:  *(unsigned int*)cash_adrs_remap(vl->lv) ^= vr->val; break;
        case MODA:  *(unsigned int*)cash_adrs_remap(vl->lv) %= vr->val; break;
        case SHLA:  *(unsigned int*)cash_adrs_remap(vl->lv) <<= vr->val; break;
        case SHRA:  *(unsigned int*)cash_adrs_remap(vl->lv) >>= vr->val; break;
        case AI:    *(unsigned int*)cash_adrs_remap(vl->lv) += 1; break;
        case AD:    *(unsigned int*)cash_adrs_remap(vl->lv) -= 1; break;
        default:    return 0;
        }
        break;
    }

    vl->val = *(uint64_t*)cash_adrs_remap(vl->lv);

    return 1;
}

void yyerror( scan_ctx_t *scan_ctx, const char *msg, ... )
{
 
    if (scan_ctx->anon_sym != NULL)
    {
        CTX_PRINTF ("undefined symbol: %s\n", scan_ctx->anon_sym);
        cash_sym_delete (scan_ctx->anon_sym);
        scan_ctx->anon_sym = NULL;
        return;
    }

    CTX_PRINTF ("%s\n", msg);

    return;
}


int yywrap (void)
{
    return 1;
}

static void init_proto_args (VALUE *pa)
{
#ifndef DYNCALL
    static int dcWarn = 0;

    if (!dcWarn)
    {
        CTX_PRINTF ("WARNING: Function prototypes not supported, requires dynlib\n");
        dcWarn = 1;
    }
    pa->proto = NULL;
    return;
#else
    pa->proto = malloc (sizeof(*pa->proto));
    pa->proto->numArgs = 0;
#endif
}

static void add_pa_type (VALUE *pa, VALUE *t)
{
    if (!pa->proto)
        return;

    if (pa->proto->numArgs >= MAX_PARMS)
       return;

    pa->proto->argsize[pa->proto->numArgs++] = t->mask;
}

static int pushScope (scan_ctx_t *scan_ctx, int activate)
{
    int wasActive = scan_ctx->scope[scan_ctx->scope_id].active;
    
    if (scan_ctx->scope_id >= MAX_SCOPE-1)
    {
        yyerror (scan_ctx, "Nesting too high (max. %d)", MAX_SCOPE);
        return 1;
    }
    
    scan_ctx->scope_id++;
    
    scan_ctx->scope[scan_ctx->scope_id].active = wasActive && activate;

    return 0;
}

static int popScope (scan_ctx_t *scan_ctx)
{
    scan_ctx->scope_id--;
    if (scan_ctx->scope_id < 0)
    {
        yyerror (scan_ctx, "Unbalanced Scope");
        return 1;
    }   
    return 0;
}

