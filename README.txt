
This is cash, a C parser shall and application wrapper.
There are several options how to use it:

1) As standalone application ("cash")
2) As standalone application containing an application
3) As library to be linked into any kind of application
4) As LD_PRELOAD shared library which adds cash support to any (?)
   application.

The core of cash is a simple C expression parser and a dynamic
symbol table. Basic features:
- Call functions
- Read/write/declare variables
- Evaluate expression is C syntax
- Simple (non-C like) loop statements

On top of this, a command line interface is provided which allows
to enter expressions. The command line interface can be operated
from the shell, or by establishing TCP/Unix sockets.
The latter allows to provide a sideband debug/scripting interface to
applications.

Additional features include
- Thread management
- Symbol table loading via libdl and libelf

Usage
=====
cash -
	Start standalone cash with interactive shell

cash -h	
	Get further options

cash_wrap my-application
	Start cash and let it spawn my-application immedeately.
	Access to the cash shell is available via an AF_UNIX socket in
	/tmp/cash_PID/0
	To access it the socat tool is recommended:
	socat UNIX:/tmp/cash_12345/0 readline

cash_wrap -i my-application
	Start cash shell interactively. The actual application can be 
	started by calling "cash_app_start", or "cash_app_start&" to
	run it as separate thread (the shell will stay available).


Build Options
=============
To build, call make

The following environment variables can be set

CROSS_COMPILE
	Cross compiler prefix

DYNCALL_LIB
	Location where the dyncall library source has been
	extracted (see http://dyncall.org).
	Required for function prototype support.
	The configure script should have been run on the build host,
	the resulting configuration should also work when cross-compiling.
	Tested with version 0.8, which is located/configured below dyncall-0.8

LIBREADLINE
	Set to 0 to not use libreadline/libhistory for tty prompt.
      
LIBDL	
	Set to 0 to not use libdl.

OBJDIR
	Existing directory where objectes, libraries and binaries shall be put into

PREBUILT_YACC_SRC
	If set to 1, the build-process uses pre-built y.tab.c/h files instead of re-generating
	them using bison. This might be useful for the case that an up-to-date bison version
	(3.x) is not available.


Implementation Notes
====================

bison
-----
The yacc source has been developed with bison 3.0.2. Earlier version might fail.
The sources generated with bison 3.0.2 are located in the "generated" direcrory. If there
are problems, try copying them from there and retry buiding.
